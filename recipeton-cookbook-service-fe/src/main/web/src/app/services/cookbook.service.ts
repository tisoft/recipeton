/*-
 * #%L
 * recipeton-cookbook-service-fe
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';

import {AppConfig} from '../app.config';
import {Cookbook} from '../models/cookbook';
import {CookbookContents} from '../models/cookbookContents';
import {CookbookPublishRequest} from '../models/cookbookPublishRequest';
import {RecipeDefinition} from '../models/recipeDefinition';

@Injectable({
  providedIn: 'root'
})
export class CookbookService {
  private endpoint: string;

  constructor(private http: HttpClient) {
    this.endpoint = AppConfig.endpoints.cookbookApi;
  }

  getThumbnailUrl(cookbookId: string, recipeUid: string): Observable<Cookbook> {
    // @ts-ignore
    return `${this.endpoint}/${cookbookId}/content/${recipeUid}/thumbnail`;
  }

  getImageUrl(cookbookId: string, recipeUid: string): Observable<Cookbook> {
    // @ts-ignore
    return `${this.endpoint}/${cookbookId}/content/${recipeUid}/image`;
  }

  getRecipeDefinition(cookbookId: string, recipeUid: string): Observable<RecipeDefinition> {
    // @ts-ignore
    return this.http.get(`${this.endpoint}/${cookbookId}/content/${recipeUid}/recipeDefinition`);
  }


  get(cookbookId: string): Observable<Cookbook> {
    // @ts-ignore
    return this.http.get(`${this.endpoint}/${cookbookId}`);
  }

  getAll(): Observable<Cookbook[]> {
    return this.http.get<Cookbook[]>(this.endpoint);
  }

  getContents(cookbookId: string): Observable<CookbookContents> {
    // @ts-ignore
    return this.http.get(`${this.endpoint}/${cookbookId}/content`);
  }

  postPublishRequest(cookbookPublishRequest: CookbookPublishRequest): Observable<any> {
    return this.http.post(`${this.endpoint}/${cookbookPublishRequest.id}/publish`, cookbookPublishRequest);
  }

  put(cookbook: Cookbook): Observable<any> {
    return this.http.put(`${this.endpoint}/${cookbook.id}`, cookbook);
  }

  static toTerms(text: string) {
    return text
      .normalize("NFD")
      .replace(/[/(/)\u0300-\u036f]/g, "")
      .replace("d'", "de ")
      .replace("l'", " ")
      .replace(/[-_]/g, " ")
      .toLowerCase().split(" ")
      .filter(s => s.length >= 3)
      .filter((value, index, self) => {
        return self.indexOf(value) == index
      })
      .sort();
  }

}
