/*-
 * #%L
 * recipeton-cookbook-service-fe
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {MatSnackBar} from '@angular/material/snack-bar';
import {ActivatedRoute, Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {AppConfig} from '../../app.config';
import {RecipeDefinition} from '../../models/recipeDefinition';
import {AuthService, User} from '../../services/auth.service';
import {CookbookService} from '../../services/cookbook.service';
import {Banner} from '../banner/banner.component';
import {LoginMenuitemComponent} from '../login/login-menuitem.component';

@Component({
  selector: 'app-recipe',
  templateUrl: './recipe.component.html',
  styleUrls: ['./recipe.component.scss'],
})
export class RecipeComponent implements OnInit, AfterViewInit {
  recipeDefinition?: RecipeDefinition;

  user: User;

  busy: boolean;

  private cookbookId: string;
  private recipeUid: string;

  @ViewChild('banner', {static: true}) banner: Banner;
  @ViewChild('login', {static: true}) login: LoginMenuitemComponent;

  constructor(private cookbookService: CookbookService,
              private authService: AuthService,
              private router: Router,
              private activatedRoute: ActivatedRoute,
              private matSnackBar: MatSnackBar,
              private translateService: TranslateService) {
    this.cookbookId = this.activatedRoute.snapshot.paramMap.get('cookbookId');
    this.recipeUid = this.activatedRoute.snapshot.paramMap.get('recipeUid');
  }

  doPublish(cookbookId: string) {
    this.busy = true
    this.cookbookService.postPublishRequest({id: cookbookId})
      .subscribe(
        d => {
          console.log(d);
          this.reload();

          this.translateService.get('Cookbook published').subscribe(t => {
            this.matSnackBar.open(t, 'OK', {duration: AppConfig.snackBarDuration});
          });
        },
        e => {
          this.busy = false;
          this.onError(e);
        });
  }

  getImageUrl() {
    return this.cookbookService.getImageUrl(this.cookbookId, this.recipeUid);
  }

  async fetchRecipe() {
    this.busy = true;
    this.cookbookService.getRecipeDefinition(this.cookbookId, this.recipeUid)
      .subscribe(
        rd => {
          this.recipeDefinition = rd;
          this.onDataReady();
        },
        e => {
          this.busy = false;
          this.onError(e);
        });
  }

  ngOnInit(): void {
    this.busy = true;
  }

  reload(): void {
    this.fetchRecipe();
  }

  onPublishClicked() {
    if (this.recipeDefinition === undefined) {
      return;
    }

    this.doPublish(this.cookbookId);
  }

  ngAfterViewInit() {
    this.fetchRecipe();
  }

  onBackClicked() {
    this.router.navigate([`${AppConfig.routes.recipeton}`]);
  }

  formatInstruction(text: string) {
    return text
      .replace("", "&#x1F373;") // bowl
      .replace("", "&#8634")
      .replace("", "&#x1F33E;")
      .replace("", "&#x1f944;")
      .replace("<nobr>", "<span style='font-weight: bold; white-space: nowrap;'>")
      .replace("</nobr>", "</span>");
  }

  onError(e: any) {
    console.log("onError", e);
    if (e['status'] == 403 || e['status'] == 401) {
      this.banner.push("Invalid credentials.", ["LOGIN", "IGNORE"])
        .subscribe(o => {
          if (o == 0) {
            this.login.doLogin();
          }
        });

      return;
    }
    this.banner.push(e.message ? e.message : e, ["OK"])
      .subscribe(o => {
        // Just dismiss
      });
  }

  private onDataReady() {
    console.log("onDataReady");
    this.busy = false;
  }
}
