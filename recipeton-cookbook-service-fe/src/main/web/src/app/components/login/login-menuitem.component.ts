/*-
 * #%L
 * recipeton-cookbook-service-fe
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import {AfterViewInit, Component, EventEmitter, Output} from '@angular/core';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {Router} from '@angular/router';
import {Observable} from 'rxjs';
import {AppConfig} from '../../app.config';
import {AuthService, User} from '../../services/auth.service';
import {LoginDialogComponent} from './login-dialog.component';

@Component({
  selector: 'app-login-menuitem',
  templateUrl: './login-menuitem.component.html',
})
export class LoginMenuitemComponent implements AfterViewInit {

  user: User;
  @Output() onLoginError = new EventEmitter<string>();

  constructor(private authService: AuthService,
              private router: Router,
              private dialog: MatDialog) {
  }

  doLogin() {
    this.openDialog()
      .subscribe(
        u => {
          console.log("login result ", u);
          if (u) {
            this.user = u;
          }
        },
        e => {
          console.log("login error ", e);
          this.onLoginError.emit(e);
        }
      );
  }

  ngAfterViewInit(): void {
    this.fetchUser();
  }

  onLoginClicked(): void {
    this.doLogin();
  }

  onLogoutClicked(): void {
    console.log("logout clicked");
    this.authService.logout()
      .subscribe(() => {
          console.log("logout success");
          this.router.navigateByUrl(AppConfig.routes.recipeton);
          this.fetchUser()
        },
        e => {
          console.log("logout error", e);
          this.router.navigateByUrl(AppConfig.routes.recipeton);
          this.fetchUser()
        });
  }

  private async fetchUser() {
    this.authService.getAuthenticatedUser().subscribe(u => {
      Promise.resolve().then(() => {
        this.user = u;
      });
    });
  }

  private openDialog(): Observable<User> {
    return new Observable<User>(o => {
      const dialogConfig = new MatDialogConfig();
      dialogConfig.data = {};
      const dialogRef = this.dialog.open(LoginDialogComponent, dialogConfig);
      dialogRef.afterClosed().subscribe(dr => {
          if (dr) {
            this.authService.login(dr.username, dr.password).subscribe(u => {
              console.log("r1", u);
                o.next(u);
              },
              e => {
                console.log("r2", e);
                o.error(e);
              });
          } else {
            console.log("r3");
            o.next(undefined);
          }
        },
        e => {
          console.log("r4", e);
          o.error(e);
        });
    });
  }

}
