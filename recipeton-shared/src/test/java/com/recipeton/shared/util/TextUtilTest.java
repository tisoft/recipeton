package com.recipeton.shared.util;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.junit.jupiter.api.Test;

import java.util.regex.Pattern;

import static com.recipeton.shared.util.TextUtil.*;
import static java.util.regex.Pattern.compile;
import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

class TextUtilTest {

    @Test
    void testNormalizeAsciiGivenStringWithNonAsciiCharactersThenReplacesOnlyFirstCharacterWithAsciiEquivalent() {
        assertThat(normalizeAsciiFirstChar("àèìòùÀÈÌÒÙáéíóúýÁÉÍÓÚÝâêîôûÂÊÎÔÛãñõÃÑÕ")).isEqualTo("aèìòùÀÈÌÒÙáéíóúýÁÉÍÓÚÝâêîôûÂÊÎÔÛãñõÃÑÕ");
    }

    @Test
    void testNormalizeAsciiGivenStringWithNonAsciiCharactersThenReplacesWithAsciiEquivalent() {
        assertThat(normalizeAscii("àèìòùÀÈÌÒÙáéíóúýÁÉÍÓÚÝâêîôûÂÊÎÔÛãñõÃÑÕ")).isEqualTo("aeiouAEIOUaeiouyAEIOUYaeiouAEIOUanoANO");
    }

    @Test
    void testNormalizeAsciiGivenStringWithNumbersThenNumbersArePreserved() {
        assertThat(normalizeAsciiFirstChar("0123456789")).isEqualTo("0123456789");
    }

    @Test
    void testRemoveAllGivenTextAnAndMatchingTemplateThenReturnsTextWithoutMatchedTextParts() {
        Pattern p = compile("<br>|<br />|<strong>|</strong>");
        assertThat(removeAll("a<br><br>b", p)).isEqualTo("ab");
        assertThat(removeAll("<br>a<br>b<br>", p)).isEqualTo("ab");
        assertThat(removeAll("<strong>a<br><br /><br>b</strong>", p)).isEqualTo("ab");
    }
}
