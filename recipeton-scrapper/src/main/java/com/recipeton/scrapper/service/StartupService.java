package com.recipeton.scrapper.service;

/*-
 * #%L
 * recipeton-scrapper
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.scrapper.config.ApplicationConfiguration;
import com.recipeton.scrapper.service.ckd.CkdScrapperService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

import static com.recipeton.scrapper.config.ApplicationConfiguration.OPERATION_EXPORT;
import static com.recipeton.scrapper.config.ApplicationConfiguration.OPERATION_REPROCESS;

@Slf4j
@RequiredArgsConstructor
@Service
public class StartupService {

    private final CkdScrapperService ckdScrapperService;
    private final ExportService exportService;
    private final ApplicationConfiguration applicationConfiguration;

    public void doStartup() throws IOException, ExecutionException, InterruptedException {
        if (OPERATION_EXPORT.equals(applicationConfiguration.getOperation())) {
            log.info("Start export");
            exportService.doExport();
            return;
        }
        if (OPERATION_REPROCESS.equals(applicationConfiguration.getOperation())) {
            log.info("Start reprocess");
            ckdScrapperService.reprocess();
            return;
        }
        log.info("Start scrap");
        ckdScrapperService.process();
    }
}
