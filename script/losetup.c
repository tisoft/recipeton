/*
 * crypto-losetup.c
 * Simplified version of losetup designed specifically for use with cryptoloop
 * Usage: crypto-losetup [-c cipher] [-r] <loopdev> <file> <key>
 *            -c <cipher>  set cipher (default aes-cbc)
 *            -r           open file read-only
 */
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <linux/loop.h>
#include <sys/ioctl.h>
#include <getopt.h>

static bool parse_hex_nibble(const char c, unsigned char *result)
{
    if ((c >= '0') && (c <= '9'))
        *result = c - '0';
    else if ((c >= 'a') && (c <= 'f'))
        *result = c - 'a' + 10;
    else if ((c >= 'A') && (c <= 'F'))
        *result = c - 'A' + 10;
    else
        return false;
    return true;
}

static bool parse_keystring(const char *hex_string, unsigned char *key, unsigned int key_len)
{
    unsigned char hi, lo;
    unsigned int i;

    for (i = 0; i < key_len; i++)
    {
        if (!parse_hex_nibble(hex_string[2*i], &hi)
                || !parse_hex_nibble(hex_string[2*i+1], &lo))
            return false;

        key[i] = (hi<<4)|lo;
    }
    return true;
}

static bool setup_loopdev(const char *loopdev, const char *file, bool readonly, const char *cipher, unsigned char *key, unsigned int keylen)
{
    struct loop_info64 li;
    int loopfd, fd;
    bool ret = false;

    if (keylen > sizeof(li.lo_encrypt_key))
    {
        fprintf(stderr, "key too long (max %lu bytes)\n", sizeof(li.lo_encrypt_key));
        goto out_noclose2;
    }

    fd = open(file, readonly ? O_RDONLY : O_RDWR);
    if (fd == -1)
    {
        perror(file);
        goto out_noclose2;
    }

    loopfd = open(loopdev, O_RDWR);
    if (loopfd == -1)
    {
        perror(loopdev);
        goto out_noclose1;
    }

    if (ioctl(loopfd, LOOP_SET_FD, fd) == -1)
    {
        perror("LOOP_SET_FD");
        goto out;
    }

    memset(&li, 0, sizeof(li));
    strncpy((char *)li.lo_file_name, file, sizeof(li.lo_file_name));
    li.lo_encrypt_type = LO_CRYPT_CRYPTOAPI;
    strncpy((char *)li.lo_crypt_name, cipher, sizeof(li.lo_crypt_name));
    li.lo_encrypt_key_size = keylen;
    memcpy(li.lo_encrypt_key, key, keylen);
    if (ioctl(loopfd, LOOP_SET_STATUS64, &li) == -1)
    {
        perror("LOOP_SET_STATUS64");
        ioctl(loopfd, LOOP_CLR_FD);
        goto out;
    }

    ret = true;
out:
    close(loopfd);
out_noclose1:
    close(fd);
out_noclose2:
    return ret;
}

int main(int argc, char *argv[])
{
    const char *loopdev, *file;
    const char *cipher = "aes-cbc";
    const char *keystring = NULL;
    unsigned char *key;
    unsigned int keylen;
    bool readonly = false;
    int c, ret;

    while ((c = getopt(argc, argv, "c:rh?")) != -1)
    {
        switch (c)
        {
            case 'c':
                cipher = optarg;
                break;
            case 'r':
                readonly = true;
                break;
            default:
                goto usage;
        }
    }

    if (argc-optind != 3)
        goto usage;

    loopdev = argv[optind];
    file = argv[optind+1];
    keystring = argv[optind+2];

    keylen = strlen(keystring)/2;
    key = malloc(keylen);
    if (!key)
    {
        fprintf(stderr, "malloc failed\n");
        return 1;
    }
    if (!parse_keystring(keystring, key, keylen))
    {
        fprintf(stderr, "failed to parse key\n");
        free(key);
        return 1;
    }

    ret = setup_loopdev(loopdev, file, readonly, cipher, key, keylen) ? 0 : 1;
    free(key);
    return ret;

usage:
    fprintf(stderr, "usage: %s [-c cipher] [-r] <loopdev> <file> <key>\n", argv[0]);
    return 1;
}
