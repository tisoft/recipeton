#!/bin/sh -e
serial_file="`dirname $1`/serial"
if test -f "$serial_file"; then
  serial_number=`head -1 $serial_file`
else
  serial_number="0 0;exit 0"
fi

printf "Loading module gmasstorage. file=$1 serial=$serial_number"

/sbin/modprobe -v g_mass_storage file=$1 stall=0 idVendor=0x090c idProduct=0x1000 iManufacturer="General" iProduct="USB Flash Disk" iSerialNumber="$serial_number" bcdDevice=0x1100 cdrom=y ro=y
printf "gmasstorage module loaded"

exit 0
