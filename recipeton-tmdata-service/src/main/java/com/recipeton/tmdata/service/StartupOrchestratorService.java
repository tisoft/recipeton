package com.recipeton.tmdata.service;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.tmdata.config.ApplicationConfiguration;
import com.recipeton.tmdata.shared.domain.maintenance.MaintenanceHashType;
import com.recipeton.tmdata.shared.domain.misc.RangeType;
import com.recipeton.tmdata.shared.domain.misc.TmVersion;
import com.recipeton.tmdata.shared.domain.project.ProjectLinkType;
import com.recipeton.tmdata.shared.domain.project.ProjectType;
import com.recipeton.tmdata.shared.domain.project.lang.ProjectLangAttribute;
import com.recipeton.tmdata.shared.domain.recipe.lang.RecipeCategoryLangAttribute;
import com.recipeton.tmdata.shared.service.DataInitializer;
import com.recipeton.tmdata.shared.service.cookstick.CookstickService;
import com.recipeton.tmdata.shared.service.ingredient.IngredientService;
import com.recipeton.tmdata.shared.service.ingredient.ShoppingCategoryService;
import com.recipeton.tmdata.shared.service.maintenance.MaintenanceHashTypeRepository;
import com.recipeton.tmdata.shared.service.material.MaterialService;
import com.recipeton.tmdata.shared.service.misc.DataExtService;
import com.recipeton.tmdata.shared.service.misc.LocaleService;
import com.recipeton.tmdata.shared.service.misc.RangeTypeRepository;
import com.recipeton.tmdata.shared.service.misc.TmVersionRepository;
import com.recipeton.tmdata.shared.service.project.ProjectLinkTypeRepository;
import com.recipeton.tmdata.shared.service.project.ProjectTypeRepository;
import com.recipeton.tmdata.shared.service.project.lang.ProjectLangAttributeRepository;
import com.recipeton.tmdata.shared.service.recipe.GuidedStepDataInitializeService;
import com.recipeton.tmdata.shared.service.recipe.RecipeDataInitializeService;
import com.recipeton.tmdata.shared.service.recipe.RecipeImportBulkService;
import com.recipeton.tmdata.shared.service.recipe.TmControlService;
import com.recipeton.tmdata.shared.service.recipe.lang.RecipeCategoryLangAttributeRepository;
import com.recipeton.tmdata.shared.service.unit.UnitService;
import com.recipeton.tmdata.shared.service.utensil.UtensilService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Slf4j
@Service
@AllArgsConstructor
public class StartupOrchestratorService {

    private final CookstickService cookstickService;
    private final DataExtService dataExtService;
    private final GuidedStepDataInitializeService guidedStepDataInitializeService;
    private final IngredientService ingredientService;
    private final LocaleService localeService;
    private final MaterialService materialService;
    private final RecipeDataInitializeService recipeDataInitializeService;
    private final RecipeImportBulkService recipeImportBulkService;
    private final ShoppingCategoryService shoppingCategoryService;
    private final TmControlService tmControlService;
    private final UnitService unitService;
    private final UtensilService utensilService;
    private final RecipeCategoryLangAttributeRepository recipeCategoryLangAttributeRepository;
    private final MaintenanceHashTypeRepository maintenanceHashTypeRepository;
    private final ProjectLangAttributeRepository projectLangAttributeRepository;
    private final ProjectLinkTypeRepository projectLinkTypeRepository;
    private final ProjectTypeRepository projectTypeRepository;
    private final RangeTypeRepository rangeTypeRepository;
    private final TmVersionRepository tmVersionRepository;
    private final ApplicationConfiguration applicationConfiguration;

    public void doStartup() throws IOException {
        if (applicationConfiguration.isDataDefaultAutocreateDisabled()) {
            return;
        }

        localeService.doDataInitialize();
        dataExtService.doDataInitialize();

        DataInitializer.getPersisted(rangeTypeRepository, RangeType.DEFAULT);

        DataInitializer.getPersisted(tmVersionRepository, TmVersion.DEFAULT);

        materialService.doDataInitialize();

        cookstickService.doDataInitialize();

        ingredientService.doDataInitialize();

        DataInitializer.getPersisted(maintenanceHashTypeRepository, MaintenanceHashType.DEFAULT);

        DataInitializer.getPersisted(projectLangAttributeRepository, ProjectLangAttribute.DEFAULT);
        DataInitializer.getPersisted(projectLinkTypeRepository, ProjectLinkType.DEFAULT);
        DataInitializer.getPersisted(projectTypeRepository, ProjectType.DEFAULT);

        unitService.doDataInitialize();

        utensilService.doDataInitialize();

        tmControlService.doDataInitialize();
        guidedStepDataInitializeService.doDataInitialize();

        shoppingCategoryService.doDataInitialize();

        DataInitializer.getPersisted(recipeCategoryLangAttributeRepository, RecipeCategoryLangAttribute.DEFAULT);

        recipeDataInitializeService.doDataInitialize();

        recipeImportBulkService.doDataInitialize();
        if (applicationConfiguration.isImportOnStart()) {
            log.info("First import starting. output={}", applicationConfiguration.getBookRootPath());
            recipeImportBulkService.doImport();
            if (applicationConfiguration.isCloseAfterImport()) {
                log.info("Import completed succesfully. Closing application as requested. output={}", applicationConfiguration.getBookRootPath());
                return;
            }
        }
        if (applicationConfiguration.isServiceMode()) {
            recipeImportBulkService.startDirectoryWatch();
        }
    }
}
