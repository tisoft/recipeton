package com.recipeton.tmdata.service.recipe;

/*-
 * #%L
 * recipeton-tmdata-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.tmdata.config.ApplicationConfiguration;
import com.recipeton.tmdata.shared.domain.misc.Locale;
import com.recipeton.tmdata.shared.domain.recipe.GuidedCreatedIngredientStepTextAttribute;
import com.recipeton.tmdata.shared.domain.recipe.GuidedFreetextType;
import com.recipeton.tmdata.shared.domain.recipe.GuidedIngredientStepTextAttribute;
import com.recipeton.tmdata.shared.domain.recipe.GuidedIngredientStepWeighingAttribute;
import com.recipeton.tmdata.shared.domain.recipe.GuidedStepMaterialObjectType;
import com.recipeton.tmdata.shared.domain.recipe.GuidedStepType;
import com.recipeton.tmdata.shared.domain.recipe.GuidedUtensilActionType;
import com.recipeton.tmdata.shared.domain.recipe.lang.GuidedTmSettingStepLangAttribute;
import com.recipeton.tmdata.shared.service.DataInitializer;
import com.recipeton.tmdata.shared.service.JsonResourceService;
import com.recipeton.tmdata.shared.service.material.MaterialService;
import com.recipeton.tmdata.shared.service.misc.LocaleRepository;
import com.recipeton.tmdata.shared.service.recipe.GuidedCreatedIngredientStepService;
import com.recipeton.tmdata.shared.service.recipe.GuidedCreatedIngredientStepTextAttributeRepository;
import com.recipeton.tmdata.shared.service.recipe.GuidedCreatedIngredientStepTextRepository;
import com.recipeton.tmdata.shared.service.recipe.GuidedFreetextTypeRepository;
import com.recipeton.tmdata.shared.service.recipe.GuidedIngredientStepService;
import com.recipeton.tmdata.shared.service.recipe.GuidedIngredientStepTextAttributeRepository;
import com.recipeton.tmdata.shared.service.recipe.GuidedIngredientStepTextRepository;
import com.recipeton.tmdata.shared.service.recipe.GuidedIngredientStepWeighingAttributeRepository;
import com.recipeton.tmdata.shared.service.recipe.GuidedStepDataInitializeService;
import com.recipeton.tmdata.shared.service.recipe.GuidedStepMaterialObjectRepository;
import com.recipeton.tmdata.shared.service.recipe.GuidedStepMaterialObjectTypeRepository;
import com.recipeton.tmdata.shared.service.recipe.GuidedStepTypeRepository;
import com.recipeton.tmdata.shared.service.recipe.GuidedUtensilActionTypeRepository;
import com.recipeton.tmdata.shared.service.recipe.lang.GuidedTmSettingStepLangAttributeRepository;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
@Service
@AllArgsConstructor
public class GuidedStepDataInitializeServiceImpl implements GuidedStepDataInitializeService {

    private final ApplicationConfiguration applicationConfiguration;
    private final GuidedCreatedIngredientStepTextAttributeRepository guidedCreatedIngredientStepTextAttributeRepository;
    private final GuidedCreatedIngredientStepTextRepository guidedCreatedIngredientStepTextRepository;
    private final GuidedFreetextTypeRepository guidedFreetextTypeRepository;
    private final GuidedIngredientStepTextAttributeRepository guidedIngredientStepTextAttributeRepository;
    private final GuidedIngredientStepTextRepository guidedIngredientStepTextRepository;
    private final GuidedIngredientStepWeighingAttributeRepository guidedIngredientStepWeighingAttributeRepository;
    private final GuidedStepMaterialObjectTypeRepository guidedStepMaterialObjectTypeRepository;
    private final GuidedIngredientStepService guidedIngredientStepService;
    private final GuidedCreatedIngredientStepService guidedCreatedIngredientStepService;
    private final GuidedStepTypeRepository guidedStepTypeRepository;
    private final GuidedTmSettingStepLangAttributeRepository guidedTmSettingStepLangAttributeRepository;
    private final GuidedUtensilActionTypeRepository guidedUtensilActionTypeRepository;
    private final JsonResourceService jsonResourceService;
    private final LocaleRepository localeRepository;
    private final GuidedStepMaterialObjectRepository guidedStepMaterialObjectRepository;
    private final MaterialService materialService;


    @Override
    @Transactional
    public void doDataInitialize() throws IOException {
        DataInitializer.getPersisted(guidedStepTypeRepository, GuidedStepType.DEFAULT);
        DataInitializer.getPersisted(guidedStepMaterialObjectTypeRepository, GuidedStepMaterialObjectType.DEFAULT);

        DataInitializer.getPersisted(guidedFreetextTypeRepository, GuidedFreetextType.DEFAULT);
        DataInitializer.getPersisted(guidedTmSettingStepLangAttributeRepository, GuidedTmSettingStepLangAttribute.DEFAULT);

        DataInitializer.getPersisted(guidedCreatedIngredientStepTextAttributeRepository, GuidedCreatedIngredientStepTextAttribute.DEFAULT);
        if (guidedCreatedIngredientStepTextRepository.count() == 0) {
            List<GuidedCreatedIngredientStepTextLangForm> forms = jsonResourceService.readCollection(applicationConfiguration.getDataDefaultGuidedCreatedIngredientStepTextLangs(), GuidedCreatedIngredientStepTextLangForm.class);

            Stream.of(GuidedCreatedIngredientStepTextAttribute.Enum.values())
                    .forEach(a -> {
                        Map<Locale, String> localization = forms.stream()
                                .filter(f -> f.getGuidedCreatedIngredientStepTextAttributeId() == a.getId())
                                .collect(Collectors.toMap(f -> localeRepository.getOne(f.getLocaleId()), GuidedCreatedIngredientStepTextLangForm::getText));
                        guidedCreatedIngredientStepTextRepository.save(guidedCreatedIngredientStepService.createGuidedCreatedIngredientStepText(localization, a, true));
                    });
        }


        DataInitializer.getPersisted(guidedIngredientStepWeighingAttributeRepository, GuidedIngredientStepWeighingAttribute.DEFAULT);
        DataInitializer.getPersisted(guidedIngredientStepTextAttributeRepository, GuidedIngredientStepTextAttribute.DEFAULT);
        if (guidedIngredientStepTextRepository.count() == 0) {
            List<GuidedIngredientStepTextLangForm> forms = jsonResourceService.readCollection(applicationConfiguration.getDataDefaultGuidedIngredientStepTextLangs(), GuidedIngredientStepTextLangForm.class);

            Stream.of(GuidedIngredientStepTextAttribute.Enum.values())
                    .forEach(a -> {
                        Map<Locale, String> localization = forms.stream()
                                .filter(f -> f.getGuidedIngredientStepTextAttributeId() == a.getId())
                                .collect(Collectors.toMap(f -> localeRepository.getOne(f.getLocaleId()), GuidedIngredientStepTextLangForm::getText));
                        guidedIngredientStepTextRepository.save(guidedIngredientStepService.createGuidedIngredientStepText(localization, a, true));
                    });
        }


        DataInitializer.getPersisted(guidedUtensilActionTypeRepository, GuidedUtensilActionType.DEFAULT);
        if (guidedStepMaterialObjectRepository.count() == 0) {
            for (GuidedUtensilActionType.Enum type : GuidedUtensilActionType.Enum.values()) {
                materialService.getPersisted(type);
            }
        }
    }

    @Data
    private static class GuidedIngredientStepTextLangForm {
        private long guidedIngredientStepTextAttributeId;
        private long localeId;
        private String text;
    }


    @Data
    private static class GuidedCreatedIngredientStepTextLangForm {
        private long guidedCreatedIngredientStepTextAttributeId;
        private long localeId;
        private String text;
    }


}
