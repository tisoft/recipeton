---
-- #%L
-- recipeton-tmdata-service
-- %%
-- Copyright (C) 2021 tincomisc
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---
CREATE TABLE recipeSignatureType (
    id INTEGER NOT NULL PRIMARY KEY,
    type CLOB NOT NULL
);
CREATE TABLE recipeSyncStateType (
    id INTEGER NOT NULL PRIMARY KEY,
    type CLOB NOT NULL
);
CREATE TABLE recipeSyncState (
    recipeSyncStateType_id INTEGER NOT NULL PRIMARY KEY REFERENCES recipeSyncStateType ON DELETE CASCADE ON UPDATE RESTRICT,
    value CLOB NOT NULL
);
CREATE TABLE maintenanceHashType (
    id INTEGER NOT NULL PRIMARY KEY,
    type CLOB NOT NULL
);
CREATE TABLE maintenanceCloudSyncHash (
    id INTEGER NOT NULL PRIMARY KEY,
    hash CLOB NOT NULL,
    maintenanceHashType_id INTEGER NOT NULL REFERENCES maintenanceHashType ON DELETE CASCADE ON UPDATE RESTRICT
);
CREATE TABLE maintenanceMediaHash (
    maintenanceCloudSyncHash_id INTEGER NOT NULL PRIMARY KEY REFERENCES maintenanceCloudSyncHash ON DELETE CASCADE ON UPDATE CASCADE,
    fileURI CLOB NOT NULL
);
CREATE TABLE recipeRecentlySyncedType (
    id INTEGER NOT NULL PRIMARY KEY,
    type CLOB NOT NULL
);
CREATE TABLE recipeRecentlySynced (
    id INTEGER NOT NULL PRIMARY KEY,
    recipe_id INTEGER NOT NULL,
    timestamp INTEGER NOT NULL,
    recipeRecentlySyncedType_id INTEGER NOT NULL REFERENCES recipeRecentlySyncedType ON DELETE CASCADE ON UPDATE RESTRICT
);
CREATE TABLE tmControlLangAttribute (
    id INTEGER NOT NULL PRIMARY KEY,
    type CLOB NOT NULL
);
CREATE TABLE tmControlProgramSoftBlendingSpeedRangeType (
    id INTEGER NOT NULL PRIMARY KEY,
    type CLOB NOT NULL
);
CREATE TABLE guidedCreatedIngredientStepTextAttribute (
    id INTEGER NOT NULL PRIMARY KEY,
    value CLOB NOT NULL
);
CREATE TABLE guidedIngredientStepTextAttribute (
    id INTEGER NOT NULL PRIMARY KEY,
    value CLOB NOT NULL
);
CREATE TABLE ingredientPreparationLangAttribute (
    id INTEGER NOT NULL PRIMARY KEY,
    type VARCHAR NOT NULL
);
CREATE TABLE cookstickExtLangAttribute (
    id INTEGER NOT NULL PRIMARY KEY,
    type CLOB NOT NULL
);
CREATE TABLE cookstickMaterialObjectType (
    id INTEGER NOT NULL PRIMARY KEY,
    type CLOB NOT NULL
);
CREATE TABLE guidedFreetextType (
    id INTEGER NOT NULL PRIMARY KEY,
    value CLOB NOT NULL
);
CREATE TABLE guidedUtensilActionText (
    id INTEGER NOT NULL PRIMARY KEY,
    isPredefined BOOLEAN DEFAULT 0 NOT NULL
);
CREATE TABLE guidedUtensilActionType (
    id INTEGER NOT NULL PRIMARY KEY,
    value CLOB NOT NULL
);
CREATE TABLE guidedCreatedIngredientStepText (
    id INTEGER NOT NULL PRIMARY KEY,
    guidedCreatedIngredientStepTextAttribute_id INTEGER NOT NULL REFERENCES guidedCreatedIngredientStepTextAttribute ON DELETE CASCADE ON UPDATE RESTRICT,
    isPredefined BOOLEAN NOT NULL
);
CREATE TABLE guidedCreatedIngredientNotation (
    id INTEGER NOT NULL PRIMARY KEY
);
CREATE TABLE guidedIngredientStepText (
    id INTEGER NOT NULL PRIMARY KEY,
    guidedIngredientStepTextAttribute_id INTEGER NOT NULL REFERENCES guidedIngredientStepTextAttribute ON DELETE CASCADE ON UPDATE RESTRICT,
    isPredefined BOOLEAN NOT NULL
);
CREATE TABLE guidedIngredientStepWeighingAttribute (
    id INTEGER NOT NULL PRIMARY KEY,
    value CLOB NOT NULL
);
CREATE TABLE guidedTmSettingStepLangAttribute (
    id INTEGER NOT NULL PRIMARY KEY,
    value CLOB NOT NULL
);
CREATE TABLE guidedStepType (
    id INTEGER NOT NULL PRIMARY KEY,
    value CLOB NOT NULL
);
CREATE TABLE guidedStepMaterialObjectType (
    id INTEGER NOT NULL PRIMARY KEY,
    value CLOB NOT NULL
);
CREATE TABLE materialOrphans (
    id INTEGER NOT NULL PRIMARY KEY,
    value CLOB NOT NULL
);
CREATE TABLE recipePathType (
    id INTEGER NOT NULL PRIMARY KEY,
    type VARCHAR NOT NULL
);
CREATE TABLE tmControlTurboType (
    id INTEGER NOT NULL PRIMARY KEY,
    value VARCHAR NOT NULL
);
CREATE TABLE tmControlTimeType (
    id INTEGER NOT NULL PRIMARY KEY,
    value VARCHAR NOT NULL
);
CREATE TABLE projectCategory (
    id INTEGER NOT NULL PRIMARY KEY,
    uid VARCHAR NOT NULL,
    position INTEGER NOT NULL,
    lft INTEGER NOT NULL,
    rgt INTEGER NOT NULL
);
CREATE TABLE projectLinkType (
    id INTEGER NOT NULL PRIMARY KEY,
    type VARCHAR NOT NULL
);
CREATE TABLE projectLangAttribute (
    id INTEGER NOT NULL PRIMARY KEY,
    type VARCHAR NOT NULL
);
CREATE TABLE projectType (
    id INTEGER NOT NULL PRIMARY KEY,
    type VARCHAR NOT NULL
);
CREATE TABLE project (
    id INTEGER NOT NULL PRIMARY KEY,
    uid VARCHAR NOT NULL,
    description VARCHAR NOT NULL,
    projectType_id INTEGER NOT NULL REFERENCES projectType ON DELETE CASCADE ON UPDATE RESTRICT,
    nofRecipesPlanned INTEGER NOT NULL,
    isInternal BOOLEAN NOT NULL,
    isInternational BOOLEAN NOT NULL,
    isDeleted BOOLEAN NOT NULL,
    createdts INTEGER NOT NULL,
    lastchangedts INTEGER NOT NULL
);
CREATE TABLE maintenanceRecipeCollectionHash (
    maintenanceCloudSyncHash_id INTEGER NOT NULL PRIMARY KEY REFERENCES maintenanceCloudSyncHash ON DELETE CASCADE ON UPDATE CASCADE,
    project_id INTEGER NOT NULL REFERENCES project ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE TABLE project_projectCategory (
    project_id INTEGER NOT NULL REFERENCES project ON DELETE CASCADE ON UPDATE CASCADE,
    projectCategory_id INTEGER NOT NULL REFERENCES projectCategory ON DELETE CASCADE ON UPDATE CASCADE,
    PRIMARY KEY (project_id, projectCategory_id)
);
CREATE TABLE projectLink (
    targetProject_id INTEGER NOT NULL REFERENCES project ON DELETE CASCADE ON UPDATE CASCADE,
    sourceProject_id INTEGER NOT NULL REFERENCES project ON DELETE CASCADE ON UPDATE CASCADE,
    projectLinkType_id INTEGER NOT NULL REFERENCES projectLinkType ON DELETE CASCADE ON UPDATE RESTRICT,
    PRIMARY KEY (targetProject_id, sourceProject_id, projectLinkType_id)
);
CREATE TABLE projectEntry (
    id INTEGER NOT NULL PRIMARY KEY,
    project_id INTEGER NOT NULL REFERENCES project ON DELETE CASCADE ON UPDATE CASCADE,
    lft INTEGER NOT NULL,
    rgt INTEGER NOT NULL
);
CREATE TABLE projectEntryTitle (
    id INTEGER NOT NULL PRIMARY KEY REFERENCES projectEntry ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE TABLE dataExtType (
    id INTEGER NOT NULL PRIMARY KEY,
    type VARCHAR NOT NULL
);
CREATE TABLE dataExt (
    dataExtType_id INTEGER NOT NULL PRIMARY KEY REFERENCES dataExtType ON DELETE CASCADE ON UPDATE RESTRICT,
    value VARCHAR NOT NULL
);
CREATE TABLE recipeGroup (
    id INTEGER NOT NULL PRIMARY KEY
);
CREATE TABLE materialValueType (
    id INTEGER NOT NULL PRIMARY KEY,
    type VARCHAR NOT NULL
);
CREATE TABLE materialObjectLangAttribute (
    id INTEGER NOT NULL PRIMARY KEY,
    type VARCHAR NOT NULL
);
CREATE TABLE materialObjectFlag (
    id INTEGER NOT NULL PRIMARY KEY,
    flag VARCHAR NOT NULL
);
CREATE TABLE materialStorageLocationType (
    id INTEGER NOT NULL PRIMARY KEY,
    type VARCHAR NOT NULL
);
CREATE TABLE materialStorageLocation (
    id INTEGER NOT NULL PRIMARY KEY,
    materialStorageLocationType_id INTEGER NOT NULL REFERENCES materialStorageLocationType ON DELETE CASCADE ON UPDATE RESTRICT,
    source VARCHAR NOT NULL
);
CREATE TABLE materialType (
    id INTEGER NOT NULL PRIMARY KEY,
    type VARCHAR NOT NULL
);
CREATE TABLE materialType_materialValueType (
    materialType_id INTEGER NOT NULL REFERENCES materialType ON DELETE CASCADE ON UPDATE RESTRICT,
    materialValueType_id INTEGER NOT NULL REFERENCES materialValueType ON DELETE CASCADE ON UPDATE RESTRICT,
    PRIMARY KEY (materialType_id, materialValueType_id)
);
CREATE TABLE material (
    id INTEGER NOT NULL PRIMARY KEY,
    materialType_id INTEGER NOT NULL REFERENCES materialType ON DELETE CASCADE ON UPDATE RESTRICT,
    mdbId INTEGER
);
CREATE TABLE materialObject (
    id INTEGER NOT NULL PRIMARY KEY,
    material_id INTEGER NOT NULL REFERENCES material ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE TABLE projectEntry_materialObject (
    projectEntry_id INTEGER NOT NULL REFERENCES projectEntry ON DELETE CASCADE ON UPDATE CASCADE,
    materialObject_id INTEGER NOT NULL REFERENCES materialObject ON DELETE CASCADE ON UPDATE CASCADE,
    position INTEGER NOT NULL,
    isPrimary BOOLEAN NOT NULL,
    PRIMARY KEY (projectEntry_id, materialObject_id)
);
CREATE TABLE project_materialObject (
    project_id INTEGER NOT NULL REFERENCES project ON DELETE CASCADE ON UPDATE CASCADE,
    materialObject_id INTEGER NOT NULL REFERENCES materialObject ON DELETE CASCADE ON UPDATE CASCADE,
    position INTEGER NOT NULL,
    isPrimary BOOLEAN NOT NULL,
    PRIMARY KEY (project_id, materialObject_id)
);
CREATE TABLE materialObject_materialObjectFlag (
    materialObject_id INTEGER NOT NULL REFERENCES materialObject ON DELETE CASCADE ON UPDATE CASCADE,
    materialObjectFlag_id INTEGER NOT NULL REFERENCES materialObjectFlag ON DELETE CASCADE ON UPDATE RESTRICT,
    PRIMARY KEY (materialObject_id, materialObjectFlag_id)
);
CREATE TABLE materialValue (
    materialObject_id INTEGER NOT NULL REFERENCES materialObject ON DELETE CASCADE ON UPDATE CASCADE,
    materialType_id INTEGER NOT NULL,
    materialValueType_id INTEGER NOT NULL,
    value VARCHAR NOT NULL,
    FOREIGN KEY (materialValueType_id, materialType_id) REFERENCES materialType_materialValueType (materialValueType_id, materialType_id) ON DELETE CASCADE ON UPDATE CASCADE,
    PRIMARY KEY (materialObject_id, materialType_id, materialValueType_id)
);
CREATE TABLE material_materialStorageLocation (
    material_id INTEGER NOT NULL REFERENCES material ON DELETE CASCADE ON UPDATE CASCADE,
    materialStorageLocation_id INTEGER NOT NULL REFERENCES materialStorageLocation ON DELETE CASCADE ON UPDATE CASCADE,
    PRIMARY KEY (material_id, materialStorageLocation_id)
);
CREATE TABLE materialFlag (
    id INTEGER NOT NULL PRIMARY KEY,
    flag VARCHAR NOT NULL
);
CREATE TABLE material_materialFlag (
    material_id INTEGER NOT NULL REFERENCES material ON DELETE CASCADE ON UPDATE CASCADE,
    materialFlag_id INTEGER NOT NULL REFERENCES materialFlag ON DELETE CASCADE ON UPDATE RESTRICT,
    PRIMARY KEY (material_id, materialFlag_id)
);
CREATE TABLE tmControlProgramType (
    id INTEGER NOT NULL PRIMARY KEY,
    program VARCHAR NOT NULL
);
CREATE TABLE tmControlSpeedType (
    id INTEGER NOT NULL PRIMARY KEY,
    value FLOAT NOT NULL,
    description VARCHAR NOT NULL
);
CREATE TABLE tmControlRotationDirectionType (
    id INTEGER NOT NULL PRIMARY KEY,
    value VARCHAR NOT NULL
);
CREATE TABLE tmControlTemperatureType (
    id INTEGER NOT NULL PRIMARY KEY,
    value INTEGER NOT NULL,
    description VARCHAR NOT NULL
);
CREATE TABLE tmControl (
    id INTEGER NOT NULL PRIMARY KEY,
    tmControlTemperatureType_id INTEGER NOT NULL REFERENCES tmControlTemperatureType ON DELETE CASCADE ON UPDATE RESTRICT,
    position INTEGER NOT NULL
);
CREATE TABLE tmControlTime (
    tmControl_id INTEGER NOT NULL PRIMARY KEY REFERENCES tmControl ON DELETE CASCADE ON UPDATE CASCADE,
    tmControlTimeType_id INTEGER NOT NULL REFERENCES tmControlTimeType ON DELETE CASCADE ON UPDATE RESTRICT
);
CREATE TABLE tmControlProgram (
    id INTEGER NOT NULL PRIMARY KEY REFERENCES tmControl ON DELETE CASCADE ON UPDATE CASCADE,
    tmControlProgramType_id INTEGER NOT NULL REFERENCES tmControlProgramType ON DELETE CASCADE ON UPDATE RESTRICT
);
CREATE TABLE tmControlProgramSoftBlending (
    tmControl_id INTEGER NOT NULL PRIMARY KEY REFERENCES tmControlProgram ON DELETE CASCADE ON UPDATE CASCADE,
    duration INTEGER NOT NULL
);
CREATE TABLE tmControlProgramSoftBlendingSpeedRange (
    tmControlProgramSoftBlending_id INTEGER NOT NULL REFERENCES tmControlProgramSoftBlending ON DELETE CASCADE ON UPDATE CASCADE,
    tmControlProgramSoftBlendingSpeedRangeType_id INTEGER NOT NULL REFERENCES tmControlProgramSoftBlendingSpeedRangeType ON DELETE CASCADE ON UPDATE RESTRICT,
    tmControlSpeedType_id INTEGER NOT NULL REFERENCES tmControlSpeedType ON DELETE CASCADE ON UPDATE RESTRICT,
    PRIMARY KEY (tmControlProgramSoftBlending_id, tmControlProgramSoftBlendingSpeedRangeType_id)
);
CREATE TABLE tmControlProgramTurbo (
    tmControl_id INTEGER NOT NULL PRIMARY KEY REFERENCES tmControlProgram ON DELETE CASCADE ON UPDATE CASCADE,
    tmControlTurboType_id INTEGER NOT NULL REFERENCES tmControlTurboType ON DELETE CASCADE ON UPDATE RESTRICT,
    impulseCount INTEGER NOT NULL
);
CREATE TABLE tmControlSpeed (
    id INTEGER NOT NULL PRIMARY KEY REFERENCES tmControl ON DELETE CASCADE ON UPDATE CASCADE,
    tmControlRotationDirectionType_id INTEGER NOT NULL REFERENCES tmControlRotationDirectionType ON DELETE CASCADE ON UPDATE RESTRICT
);
CREATE TABLE rangeType (
    id INTEGER NOT NULL PRIMARY KEY,
    type VARCHAR NOT NULL
);
CREATE TABLE tmControlTimeRange (
    tmControl_id INTEGER NOT NULL REFERENCES tmControlTime ON DELETE CASCADE ON UPDATE CASCADE,
    rangeType_id INTEGER NOT NULL REFERENCES rangeType ON DELETE CASCADE ON UPDATE RESTRICT,
    time INTEGER NOT NULL,
    PRIMARY KEY (tmControl_id, rangeType_id)
);
CREATE TABLE tmControlSpeedRange (
    tmControlSpeed_id INTEGER NOT NULL REFERENCES tmControlSpeed ON DELETE CASCADE ON UPDATE CASCADE,
    rangeType_id INTEGER NOT NULL REFERENCES rangeType ON DELETE CASCADE ON UPDATE RESTRICT,
    tmControlSpeedType_id INTEGER NOT NULL REFERENCES tmControlSpeedType ON DELETE CASCADE ON UPDATE RESTRICT,
    PRIMARY KEY (tmControlSpeed_id, rangeType_id)
);
CREATE TABLE recipeLinkType (
    id INTEGER NOT NULL PRIMARY KEY,
    type VARCHAR NOT NULL
);
CREATE TABLE unitType (
    id INTEGER NOT NULL PRIMARY KEY,
    type VARCHAR NOT NULL
);
CREATE TABLE recipeNutritionalValueType (
    id INTEGER NOT NULL PRIMARY KEY,
    type VARCHAR NOT NULL
);
CREATE TABLE tmVersion (
    id INTEGER NOT NULL PRIMARY KEY,
    version VARCHAR NOT NULL
);
CREATE TABLE recipeTimeType (
    id INTEGER NOT NULL PRIMARY KEY,
    type VARCHAR NOT NULL
);
CREATE TABLE unitNotationPriority (
    id INTEGER NOT NULL PRIMARY KEY,
    text VARCHAR NOT NULL
);
CREATE TABLE utensilType (
    id INTEGER NOT NULL PRIMARY KEY,
    utensilText VARCHAR NOT NULL
);
CREATE TABLE shoppingCategoryLangAttribute (
    id INTEGER NOT NULL PRIMARY KEY,
    name VARCHAR NOT NULL
);
CREATE TABLE unitNotationLangAttribute (
    id INTEGER NOT NULL PRIMARY KEY,
    name VARCHAR NOT NULL
);
CREATE TABLE utensilLangAttribute (
    id INTEGER NOT NULL PRIMARY KEY,
    name VARCHAR NOT NULL
);
CREATE TABLE shoppingCategory (
    id INTEGER NOT NULL PRIMARY KEY,
    position INTEGER NOT NULL
);
CREATE TABLE ingredientFoodCategory (
    id INTEGER NOT NULL PRIMARY KEY,
    lft INTEGER NOT NULL,
    rgt INTEGER NOT NULL
);
CREATE TABLE ingredientNotationLangAttribute (
    id INTEGER NOT NULL PRIMARY KEY,
    type VARCHAR NOT NULL
);
CREATE TABLE recipeCategoryLangAttribute (
    id INTEGER NOT NULL PRIMARY KEY,
    type VARCHAR NOT NULL
);
CREATE TABLE ingredientNotationPriority (
    id INTEGER NOT NULL PRIMARY KEY,
    text VARCHAR NOT NULL
);
CREATE TABLE ingredient (
    id INTEGER NOT NULL PRIMARY KEY,
    uid VARCHAR NOT NULL,
    shoppingCategory_id INTEGER NOT NULL REFERENCES shoppingCategory ON DELETE CASCADE ON UPDATE CASCADE,
    version DECIMAL(8,2) NOT NULL,
    isDeleted BOOLEAN NOT NULL,
    createdts INTEGER NOT NULL
);
CREATE TABLE ingredientPreparation (
    id INTEGER NOT NULL PRIMARY KEY,
    ingredient_id INTEGER NOT NULL REFERENCES ingredient ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE TABLE ingredient_materialObject (
    ingredient_id INTEGER NOT NULL REFERENCES ingredient ON DELETE CASCADE ON UPDATE CASCADE,
    materialObject_id INTEGER NOT NULL REFERENCES materialObject ON DELETE CASCADE ON UPDATE CASCADE,
    position INTEGER NOT NULL,
    isPrimary BOOLEAN DEFAULT 0 NOT NULL,
    PRIMARY KEY (ingredient_id, materialObject_id)
);
CREATE TABLE ingredient_ingredientFoodCategory (
    ingredient_id INTEGER NOT NULL REFERENCES ingredient ON DELETE CASCADE ON UPDATE CASCADE,
    ingredientFoodCategory_id INTEGER NOT NULL REFERENCES ingredientFoodCategory ON DELETE CASCADE ON UPDATE CASCADE,
    PRIMARY KEY (ingredient_id, ingredientFoodCategory_id)
);
CREATE TABLE recipeCategory (
    id INTEGER NOT NULL PRIMARY KEY,
    uid VARCHAR NOT NULL,
    position INTEGER NOT NULL,
    isPrimary BOOLEAN NOT NULL,
    lft INTEGER NOT NULL,
    rgt INTEGER NOT NULL
);
CREATE TABLE recipeCategory_materialObject (
    recipeCategory_id INTEGER NOT NULL REFERENCES recipeCategory ON DELETE CASCADE ON UPDATE CASCADE,
    materialObject_id INTEGER NOT NULL REFERENCES materialObject ON DELETE CASCADE ON UPDATE CASCADE,
    position INTEGER NOT NULL,
    isPrimary BOOLEAN DEFAULT 0 NOT NULL,
    PRIMARY KEY (recipeCategory_id, materialObject_id)
);
CREATE TABLE ingredientLangAttribute (
    id INTEGER NOT NULL PRIMARY KEY,
    type VARCHAR NOT NULL
);
CREATE TABLE ingredientFlag (
    id INTEGER NOT NULL PRIMARY KEY,
    flagName VARCHAR NOT NULL
);
CREATE TABLE recipeNutritionLangAttribute (
    id INTEGER NOT NULL PRIMARY KEY,
    type VARCHAR NOT NULL
);
CREATE TABLE ingredientNotation (
    id INTEGER NOT NULL PRIMARY KEY,
    ingredient_id INTEGER NOT NULL REFERENCES ingredient ON DELETE CASCADE ON UPDATE CASCADE,
    ingredientNotationPriority_id INTEGER NOT NULL REFERENCES ingredientNotationPriority ON DELETE CASCADE ON UPDATE RESTRICT
);
CREATE TABLE utensil (
    id INTEGER NOT NULL PRIMARY KEY,
    uid VARCHAR NOT NULL,
    utensilType_id INTEGER NOT NULL REFERENCES utensilType ON DELETE CASCADE ON UPDATE RESTRICT,
    version DECIMAL(8,2) NOT NULL
);
CREATE TABLE utensil_materialObject (
    utensil_id INTEGER NOT NULL REFERENCES utensil ON DELETE CASCADE ON UPDATE CASCADE,
    materialObject_id INTEGER NOT NULL REFERENCES materialObject ON DELETE CASCADE ON UPDATE CASCADE,
    position INTEGER NOT NULL,
    isPrimary BOOLEAN DEFAULT 0 NOT NULL,
    PRIMARY KEY (utensil_id, materialObject_id)
);
CREATE TABLE unit (
    id INTEGER NOT NULL PRIMARY KEY,
    position INTEGER NOT NULL,
    version INTEGER NOT NULL
);
CREATE TABLE unit_unitType (
    unit_id INTEGER NOT NULL REFERENCES unit ON DELETE CASCADE ON UPDATE CASCADE,
    unitType_id INTEGER NOT NULL REFERENCES unitType ON DELETE CASCADE ON UPDATE RESTRICT,
    PRIMARY KEY (unit_id, unitType_id)
);
CREATE TABLE unitNotation (
    id INTEGER NOT NULL PRIMARY KEY,
    unit_id INTEGER NOT NULL REFERENCES unit ON DELETE CASCADE ON UPDATE CASCADE,
    unitNotationPriority_id INTEGER NOT NULL REFERENCES unitNotationPriority ON DELETE CASCADE ON UPDATE RESTRICT
);
CREATE TABLE recipeLangAttribute (
    id INTEGER NOT NULL PRIMARY KEY,
    type VARCHAR(50) NOT NULL
);
CREATE TABLE recipeStepLangAttribute (
    id INTEGER NOT NULL PRIMARY KEY,
    type VARCHAR NOT NULL
);
CREATE TABLE language (
    id INTEGER NOT NULL PRIMARY KEY,
    code VARCHAR(2) NOT NULL
);
CREATE TABLE country (
    id INTEGER NOT NULL PRIMARY KEY,
    code VARCHAR(2) NOT NULL
);
CREATE TABLE address (
    id INTEGER NOT NULL PRIMARY KEY,
    addressField CLOB NOT NULL,
    street VARCHAR NOT NULL,
    houseNo VARCHAR NOT NULL,
    plz INTEGER NOT NULL,
    city VARCHAR NOT NULL,
    country_id INTEGER NOT NULL REFERENCES country ON DELETE CASCADE ON UPDATE RESTRICT
);
CREATE TABLE company (
    id INTEGER NOT NULL PRIMARY KEY,
    name VARCHAR NOT NULL,
    address_id INTEGER NOT NULL REFERENCES address ON DELETE CASCADE ON UPDATE CASCADE,
    phone VARCHAR NOT NULL,
    website VARCHAR NOT NULL
);
CREATE TABLE project_company (
    project_id INTEGER NOT NULL REFERENCES project ON DELETE CASCADE ON UPDATE CASCADE,
    company_id INTEGER NOT NULL REFERENCES company ON DELETE CASCADE ON UPDATE CASCADE,
    PRIMARY KEY (project_id, company_id)
);
CREATE TABLE material_company (
    material_id INTEGER NOT NULL REFERENCES material ON DELETE CASCADE ON UPDATE CASCADE,
    company_id INTEGER NOT NULL REFERENCES company ON DELETE CASCADE ON UPDATE CASCADE,
    PRIMARY KEY (material_id, company_id)
);
CREATE TABLE locale (
    id INTEGER NOT NULL PRIMARY KEY,
    country_id INTEGER NOT NULL REFERENCES country ON DELETE CASCADE ON UPDATE RESTRICT,
    language_id INTEGER NOT NULL REFERENCES language ON DELETE CASCADE ON UPDATE RESTRICT,
    lft INTEGER NOT NULL,
    rgt INTEGER NOT NULL
);
CREATE TABLE tmControlLang (
    tmControl_id INTEGER NOT NULL REFERENCES tmControl ON DELETE CASCADE ON UPDATE CASCADE,
    tmControlLangAttribute_id INTEGER NOT NULL REFERENCES tmControlLangAttribute ON DELETE CASCADE ON UPDATE RESTRICT,
    locale_id INTEGER NOT NULL REFERENCES locale ON DELETE CASCADE ON UPDATE RESTRICT,
    text CLOB NOT NULL,
    sortKey BLOB NOT NULL,
    PRIMARY KEY (tmControl_id, tmControlLangAttribute_id, locale_id)
);
CREATE TABLE ingredientPreparationLang (
    ingredientPreparation_id INTEGER NOT NULL REFERENCES ingredientPreparation ON DELETE CASCADE ON UPDATE CASCADE,
    ingredientPreparationLangAttribute_id INTEGER NOT NULL REFERENCES ingredientPreparationLangAttribute ON DELETE CASCADE ON UPDATE RESTRICT,
    locale_id INTEGER NOT NULL REFERENCES locale ON DELETE CASCADE ON UPDATE RESTRICT,
    text VARCHAR NOT NULL,
    sortKey BLOB NOT NULL,
    PRIMARY KEY (ingredientPreparation_id, ingredientPreparationLangAttribute_id, locale_id)
);
CREATE TABLE cookstickMaterialObject (
    id INTEGER NOT NULL PRIMARY KEY,
    locale_id INTEGER NOT NULL REFERENCES locale ON DELETE CASCADE ON UPDATE RESTRICT,
    cookstickMaterialObjectType_id INTEGER NOT NULL REFERENCES cookstickMaterialObjectType ON DELETE CASCADE ON UPDATE RESTRICT,
    materialObject_id INTEGER NOT NULL REFERENCES materialObject ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE TABLE cookstickExtLang (
    cookstickExtLangAttribute_id INTEGER NOT NULL REFERENCES cookstickExtLangAttribute ON DELETE CASCADE ON UPDATE RESTRICT,
    locale_id INTEGER NOT NULL REFERENCES locale ON DELETE CASCADE ON UPDATE RESTRICT,
    text VARCHAR NOT NULL,
    sortKey BLOB NOT NULL,
    PRIMARY KEY (cookstickExtLangAttribute_id, locale_id)
);
CREATE TABLE guidedUtensilActionTextLang (
    guidedUtensilActionText_id INTEGER NOT NULL REFERENCES guidedUtensilActionText ON DELETE CASCADE ON UPDATE CASCADE,
    locale_id INTEGER NOT NULL REFERENCES locale ON DELETE CASCADE ON UPDATE RESTRICT,
    text VARCHAR NOT NULL,
    sortKey BLOB NOT NULL,
    PRIMARY KEY (guidedUtensilActionText_id, locale_id)
);
CREATE TABLE guidedCreatedIngredientStepTextLang (
    guidedCreatedIngredientStepText_id INTEGER NOT NULL REFERENCES guidedCreatedIngredientStepText ON DELETE CASCADE ON UPDATE CASCADE,
    locale_id INTEGER NOT NULL REFERENCES locale ON DELETE CASCADE ON UPDATE RESTRICT,
    text VARCHAR NOT NULL,
    sortKey BLOB NOT NULL,
    PRIMARY KEY (guidedCreatedIngredientStepText_id, locale_id)
);
CREATE TABLE guidedCreatedIngredientNotationLang (
    guidedCreatedIngredientNotation_id INTEGER NOT NULL REFERENCES guidedCreatedIngredientNotation ON DELETE CASCADE ON UPDATE CASCADE,
    locale_id INTEGER NOT NULL REFERENCES locale ON DELETE CASCADE ON UPDATE RESTRICT,
    text VARCHAR NOT NULL,
    sortKey BLOB NOT NULL,
    PRIMARY KEY (guidedCreatedIngredientNotation_id, locale_id)
);
CREATE TABLE guidedIngredientStepTextLang (
    guidedIngredientStepText_id INTEGER NOT NULL REFERENCES guidedIngredientStepText ON DELETE CASCADE ON UPDATE CASCADE,
    locale_id INTEGER NOT NULL REFERENCES locale ON DELETE CASCADE ON UPDATE RESTRICT,
    text VARCHAR NOT NULL,
    sortKey BLOB NOT NULL,
    PRIMARY KEY (guidedIngredientStepText_id, locale_id)
);
CREATE TABLE companyLocale (
    id INTEGER NOT NULL PRIMARY KEY,
    company_id INTEGER NOT NULL REFERENCES company ON DELETE CASCADE ON UPDATE CASCADE,
    locale_id INTEGER NOT NULL REFERENCES locale ON DELETE CASCADE ON UPDATE RESTRICT,
    priority INTEGER NOT NULL
);
CREATE TABLE projectCategoryLang (
    projectCategory_id INTEGER NOT NULL REFERENCES projectCategory ON DELETE CASCADE ON UPDATE CASCADE,
    locale_id INTEGER NOT NULL REFERENCES locale ON DELETE CASCADE ON UPDATE RESTRICT,
    text VARCHAR NOT NULL,
    sortKey BLOB NOT NULL,
    PRIMARY KEY (projectCategory_id, locale_id)
);
CREATE TABLE projectEntryTitleLang (
    projectEntryTitle_id INTEGER NOT NULL REFERENCES projectEntryTitle ON DELETE CASCADE ON UPDATE CASCADE,
    locale_id INTEGER NOT NULL REFERENCES locale ON DELETE CASCADE ON UPDATE RESTRICT,
    title VARCHAR NOT NULL,
    sortKey BLOB NOT NULL,
    PRIMARY KEY (projectEntryTitle_id, locale_id)
);
CREATE TABLE projectLang (
    project_id INTEGER NOT NULL REFERENCES project ON DELETE CASCADE ON UPDATE CASCADE,
    projectLangAttribute_id INTEGER NOT NULL REFERENCES projectLangAttribute ON DELETE CASCADE ON UPDATE RESTRICT,
    locale_id INTEGER NOT NULL REFERENCES locale ON DELETE CASCADE ON UPDATE RESTRICT,
    text VARCHAR NOT NULL,
    sortKey BLOB NOT NULL,
    PRIMARY KEY (project_id, projectLangAttribute_id, locale_id)
);
CREATE TABLE recipeGroupLang (
    recipeGroup_id INTEGER NOT NULL REFERENCES recipeGroup ON DELETE CASCADE ON UPDATE CASCADE,
    locale_id INTEGER NOT NULL REFERENCES locale ON DELETE CASCADE ON UPDATE CASCADE,
    text VARCHAR NOT NULL,
    sortKey BLOB NOT NULL,
    PRIMARY KEY (recipeGroup_id, locale_id)
);
CREATE TABLE materialObjectLang (
    materialObject_id INTEGER NOT NULL REFERENCES materialObject ON DELETE CASCADE ON UPDATE CASCADE,
    materialObjectLangAttribute_id INTEGER NOT NULL REFERENCES materialObjectLangAttribute ON DELETE CASCADE ON UPDATE RESTRICT,
    locale_id INTEGER NOT NULL REFERENCES locale ON DELETE CASCADE ON UPDATE RESTRICT,
    text VARCHAR NOT NULL,
    sortKey BLOB NOT NULL,
    PRIMARY KEY (materialObject_id, materialObjectLangAttribute_id, locale_id)
);
CREATE TABLE recipeNutritionalValueTypeLang (
    recipeNutritionalValueType_id INTEGER NOT NULL REFERENCES recipeNutritionalValueType ON DELETE CASCADE ON UPDATE RESTRICT,
    locale_id INTEGER NOT NULL REFERENCES locale ON DELETE CASCADE ON UPDATE RESTRICT,
    text VARCHAR NOT NULL,
    sortKey BLOB NOT NULL,
    PRIMARY KEY (recipeNutritionalValueType_id, locale_id)
);
CREATE TABLE recipeTimeTypeLang (
    recipeTimeType_id INTEGER NOT NULL REFERENCES recipeTimeType ON DELETE CASCADE ON UPDATE RESTRICT,
    locale_id INTEGER NOT NULL REFERENCES locale ON DELETE CASCADE ON UPDATE RESTRICT,
    text VARCHAR NOT NULL,
    sortKey BLOB NOT NULL,
    PRIMARY KEY (recipeTimeType_id, locale_id)
);
CREATE TABLE languageLang (
    language_id INTEGER NOT NULL REFERENCES language ON DELETE CASCADE ON UPDATE RESTRICT,
    locale_id INTEGER NOT NULL REFERENCES locale ON DELETE CASCADE ON UPDATE RESTRICT,
    text VARCHAR(50) NOT NULL,
    sortKey BLOB NOT NULL,
    PRIMARY KEY (language_id, locale_id)
);
CREATE TABLE unitNotationLang (
    unitNotation_id INTEGER NOT NULL REFERENCES unitNotation ON DELETE CASCADE ON UPDATE CASCADE,
    unitNotationLangAttribute_id INTEGER NOT NULL REFERENCES unitNotationLangAttribute ON DELETE CASCADE ON UPDATE RESTRICT,
    locale_id INTEGER NOT NULL REFERENCES locale ON DELETE CASCADE ON UPDATE RESTRICT,
    text VARCHAR NOT NULL,
    sortKey BLOB NOT NULL,
    PRIMARY KEY (unitNotation_id, unitNotationLangAttribute_id, locale_id)
);
CREATE TABLE shoppingCategoryLang (
    shoppingCategory_id INTEGER NOT NULL REFERENCES shoppingCategory ON DELETE CASCADE ON UPDATE CASCADE,
    shoppingCategoryLangAttribute_id INTEGER NOT NULL REFERENCES shoppingCategoryLangAttribute ON DELETE CASCADE ON UPDATE RESTRICT,
    locale_id INTEGER NOT NULL REFERENCES locale ON DELETE CASCADE ON UPDATE RESTRICT,
    text VARCHAR NOT NULL,
    sortKey BLOB NOT NULL,
    PRIMARY KEY (shoppingCategory_id, shoppingCategoryLangAttribute_id, locale_id)
);
CREATE TABLE ingredientFoodCategoryLang (
    ingredientFoodCategory_id INTEGER NOT NULL REFERENCES ingredientFoodCategory ON DELETE CASCADE ON UPDATE CASCADE,
    locale_id INTEGER NOT NULL REFERENCES locale ON DELETE CASCADE ON UPDATE RESTRICT,
    text VARCHAR NOT NULL,
    sortKey BLOB NOT NULL,
    PRIMARY KEY (ingredientFoodCategory_id, locale_id)
);
CREATE TABLE ingredientNotationLang (
    ingredientNotation_id INTEGER NOT NULL REFERENCES ingredientNotation ON DELETE CASCADE ON UPDATE CASCADE,
    ingredientNotationLangAttribute_id INTEGER NOT NULL REFERENCES ingredientNotationLangAttribute ON DELETE CASCADE ON UPDATE RESTRICT,
    locale_id INTEGER NOT NULL REFERENCES locale ON DELETE CASCADE ON UPDATE RESTRICT,
    text VARCHAR NOT NULL,
    sortKey BLOB NOT NULL,
    PRIMARY KEY (ingredientNotation_id, ingredientNotationLangAttribute_id, locale_id)
);
CREATE TABLE ingredientLang (
    ingredient_id INTEGER NOT NULL REFERENCES ingredient ON DELETE CASCADE ON UPDATE CASCADE,
    ingredientLangAttribute_id INTEGER NOT NULL REFERENCES ingredientLangAttribute ON DELETE CASCADE ON UPDATE RESTRICT,
    locale_id INTEGER NOT NULL REFERENCES locale ON DELETE CASCADE ON UPDATE RESTRICT,
    text VARCHAR NOT NULL,
    sortKey BLOB NOT NULL,
    isInternal BOOLEAN NOT NULL,
    PRIMARY KEY (ingredient_id, ingredientLangAttribute_id, locale_id)
);
CREATE TABLE utensilLang (
    utensil_id INTEGER NOT NULL REFERENCES utensil ON DELETE CASCADE ON UPDATE CASCADE,
    utensilLangAttribute_id INTEGER NOT NULL REFERENCES utensilLangAttribute ON DELETE CASCADE ON UPDATE RESTRICT,
    locale_id INTEGER NOT NULL REFERENCES locale ON DELETE CASCADE ON UPDATE RESTRICT,
    text VARCHAR NOT NULL,
    sortKey BLOB NOT NULL,
    PRIMARY KEY (utensil_id, utensilLangAttribute_id, locale_id)
);
CREATE TABLE recipeCategoryLang (
    recipeCategory_id INTEGER NOT NULL REFERENCES recipeCategory ON DELETE CASCADE ON UPDATE CASCADE,
    recipeCategoryLangAttribute_id INTEGER NOT NULL REFERENCES recipeCategoryLangAttribute ON DELETE CASCADE ON UPDATE RESTRICT,
    locale_id INTEGER NOT NULL REFERENCES locale ON DELETE CASCADE ON UPDATE RESTRICT,
    text VARCHAR NOT NULL,
    sortKey BLOB NOT NULL,
    PRIMARY KEY (recipeCategory_id, recipeCategoryLangAttribute_id, locale_id)
);
CREATE TABLE ingredient_country_ingredientFlag (
    ingredient_id INTEGER NOT NULL REFERENCES ingredient ON DELETE CASCADE ON UPDATE CASCADE,
    country_id INTEGER NOT NULL REFERENCES country ON DELETE CASCADE ON UPDATE RESTRICT,
    ingredientFlag_id INTEGER NOT NULL REFERENCES ingredientFlag ON DELETE CASCADE ON UPDATE RESTRICT,
    PRIMARY KEY (ingredient_id, country_id, ingredientFlag_id)
);
CREATE TABLE countryLang (
    country_id INTEGER NOT NULL REFERENCES country ON DELETE CASCADE ON UPDATE RESTRICT,
    locale_id INTEGER NOT NULL REFERENCES locale ON DELETE CASCADE ON UPDATE RESTRICT,
    text VARCHAR(50) NOT NULL,
    sortKey BLOB NOT NULL,
    PRIMARY KEY (country_id, locale_id)
);
CREATE TABLE recipeDifficulty (
    id INTEGER NOT NULL PRIMARY KEY,
    difficulty VARCHAR NOT NULL
);
CREATE TABLE recipeDifficultyLang (
    recipeDifficulty_id INTEGER NOT NULL REFERENCES recipeDifficulty ON DELETE CASCADE ON UPDATE RESTRICT,
    locale_id INTEGER NOT NULL REFERENCES locale ON DELETE CASCADE ON UPDATE RESTRICT,
    text VARCHAR NOT NULL,
    sortKey BLOB NOT NULL,
    PRIMARY KEY (recipeDifficulty_id, locale_id)
);
CREATE TABLE recipePrice (
    id INTEGER NOT NULL PRIMARY KEY,
    pricelevel VARCHAR NOT NULL
);
CREATE TABLE recipePriceLang (
    recipePrice_id INTEGER NOT NULL REFERENCES recipePrice ON DELETE CASCADE ON UPDATE RESTRICT,
    locale_id INTEGER NOT NULL REFERENCES locale ON DELETE CASCADE ON UPDATE RESTRICT,
    text VARCHAR NOT NULL,
    sortKey BLOB NOT NULL,
    PRIMARY KEY (recipePrice_id, locale_id)
);
CREATE TABLE recipe (
    id INTEGER NOT NULL PRIMARY KEY,
    uid VARCHAR NOT NULL,
    originalTitle VARCHAR NOT NULL,
    year INTEGER NOT NULL,
    servingQuantity DECIMAL(8,3) NOT NULL,
    servingUnitNotation_id INTEGER REFERENCES unitNotation ON DELETE CASCADE ON UPDATE CASCADE,
    recipeDifficulty_id INTEGER DEFAULT 0 NOT NULL REFERENCES recipeDifficulty ON DELETE CASCADE ON UPDATE RESTRICT,
    recipePrice_id INTEGER DEFAULT 0 NOT NULL REFERENCES recipePrice ON DELETE CASCADE ON UPDATE RESTRICT,
    version DECIMAL(8,2) NOT NULL,
    createdts INTEGER NOT NULL,
    lastchangedts INTEGER NOT NULL,
    isDeleted BOOLEAN NOT NULL
);
CREATE TABLE recipeSignature (
    id INTEGER NOT NULL PRIMARY KEY,
    recipe_id INTEGER NOT NULL REFERENCES recipe ON DELETE CASCADE ON UPDATE CASCADE,
    recipeSignatureType_id INTEGER NOT NULL REFERENCES recipeSignatureType ON DELETE CASCADE ON UPDATE RESTRICT,
    recipeSignature CLOB NOT NULL
);
CREATE TABLE recipeImport (
    id INTEGER NOT NULL PRIMARY KEY,
    recipe_id INTEGER NOT NULL REFERENCES recipe ON DELETE CASCADE ON UPDATE CASCADE,
    insertStatement CLOB NOT NULL
);
CREATE TABLE maintenanceRecipeHash (
    maintenanceCloudSyncHash_id INTEGER NOT NULL PRIMARY KEY REFERENCES maintenanceCloudSyncHash ON DELETE CASCADE ON UPDATE CASCADE,
    recipe_id INTEGER NOT NULL REFERENCES recipe ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE TABLE recipeStep (
    id INTEGER NOT NULL PRIMARY KEY,
    recipe_id INTEGER NOT NULL REFERENCES recipe ON DELETE CASCADE ON UPDATE CASCADE,
    recipeGroup_id INTEGER REFERENCES recipeGroup ON DELETE CASCADE ON UPDATE CASCADE,
    position INTEGER NOT NULL,
    displayNo INTEGER NOT NULL
);
CREATE TABLE guidedStep (
    id INTEGER NOT NULL PRIMARY KEY,
    recipeStep_id INTEGER NOT NULL REFERENCES recipeStep ON DELETE CASCADE ON UPDATE CASCADE,
    guidedStepType_id INTEGER NOT NULL REFERENCES guidedStepType ON DELETE CASCADE ON UPDATE RESTRICT,
    position INTEGER NOT NULL
);
CREATE TABLE guidedFreetextStep (
    guidedStep_id INTEGER NOT NULL PRIMARY KEY REFERENCES guidedStep ON DELETE CASCADE ON UPDATE CASCADE,
    guidedFreetextType_id INTEGER NOT NULL REFERENCES guidedFreetextType ON DELETE CASCADE ON UPDATE RESTRICT
);
CREATE TABLE guidedFreetextStepLang (
    guidedFreetextStep_id INTEGER NOT NULL REFERENCES guidedFreetextStep ON DELETE CASCADE ON UPDATE CASCADE,
    locale_id INTEGER NOT NULL REFERENCES locale ON DELETE CASCADE ON UPDATE RESTRICT,
    text VARCHAR NOT NULL,
    sortKey BLOB NOT NULL,
    PRIMARY KEY (guidedFreetextStep_id, locale_id)
);
CREATE TABLE guidedUtensilStep (
    guidedStep_id INTEGER NOT NULL PRIMARY KEY REFERENCES guidedStep ON DELETE CASCADE ON UPDATE CASCADE,
    guidedUtensilActionType_id INTEGER NOT NULL REFERENCES guidedUtensilActionType ON DELETE CASCADE ON UPDATE RESTRICT,
    guidedUtensilActionText_id INTEGER NOT NULL REFERENCES guidedUtensilActionText ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE TABLE guidedCreatedIngredientStep (
    guidedStep_id INTEGER NOT NULL PRIMARY KEY REFERENCES guidedStep ON DELETE CASCADE ON UPDATE CASCADE,
    guidedCreatedIngredientNotation_id INTEGER NOT NULL REFERENCES guidedCreatedIngredientNotation ON DELETE CASCADE ON UPDATE RESTRICT,
    guidedCreatedIngredientStepText_id INTEGER NOT NULL REFERENCES guidedCreatedIngredientStepText ON DELETE CASCADE ON UPDATE CASCADE,
    guidedIngredientStepWeighingAttribute_id INTEGER NOT NULL REFERENCES guidedIngredientStepWeighingAttribute ON DELETE CASCADE ON UPDATE RESTRICT,
    unitNotation_id INTEGER DEFAULT NULL REFERENCES unitNotation ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE TABLE guidedCreatedIngredientAmountRange (
    guidedCreatedIngredientStep_id INTEGER NOT NULL REFERENCES guidedCreatedIngredientStep ON DELETE CASCADE ON UPDATE CASCADE,
    rangeType_id INTEGER NOT NULL REFERENCES rangeType ON DELETE CASCADE ON UPDATE RESTRICT,
    amount DECIMAL(8,3) NOT NULL,
    PRIMARY KEY (guidedCreatedIngredientStep_id, rangeType_id)
);
CREATE TABLE guidedIngredientStep (
    guidedStep_id INTEGER NOT NULL PRIMARY KEY REFERENCES guidedStep ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE TABLE guidedStep_materialObject (
    guidedStep_id INTEGER NOT NULL REFERENCES guidedStep ON DELETE CASCADE ON UPDATE CASCADE,
    guidedStepMaterialObjectType_id INTEGER NOT NULL REFERENCES guidedStepMaterialObjectType ON DELETE CASCADE ON UPDATE RESTRICT,
    materialObject_id INTEGER NOT NULL REFERENCES materialObject ON DELETE CASCADE ON UPDATE CASCADE,
    PRIMARY KEY (guidedStep_id, guidedStepMaterialObjectType_id, materialObject_id)
);
CREATE TABLE recipeStep_materialObject (
    recipeStep_id INTEGER NOT NULL REFERENCES recipeStep ON DELETE CASCADE ON UPDATE CASCADE,
    materialObject_id INTEGER NOT NULL REFERENCES materialObject ON DELETE CASCADE ON UPDATE CASCADE,
    position INTEGER NOT NULL,
    isPrimary BOOLEAN DEFAULT 0 NOT NULL,
    PRIMARY KEY (recipeStep_id, materialObject_id)
);
CREATE TABLE recipeStep_utensil (
    recipeStep_id INTEGER NOT NULL REFERENCES recipeStep ON DELETE CASCADE ON UPDATE CASCADE,
    utensil_id INTEGER NOT NULL REFERENCES utensil ON DELETE CASCADE ON UPDATE CASCADE,
    position INTEGER NOT NULL,
    PRIMARY KEY (recipeStep_id, utensil_id)
);
CREATE TABLE recipeStepLang (
    recipeStep_id INTEGER NOT NULL REFERENCES recipeStep ON DELETE CASCADE ON UPDATE CASCADE,
    recipeStepLangAttribute_id INTEGER NOT NULL REFERENCES recipeStepLangAttribute ON DELETE CASCADE ON UPDATE RESTRICT,
    locale_id INTEGER NOT NULL REFERENCES locale ON DELETE CASCADE ON UPDATE RESTRICT,
    text VARCHAR NOT NULL,
    sortKey BLOB NOT NULL,
    PRIMARY KEY (recipeStep_id, recipeStepLangAttribute_id, locale_id)
);
CREATE TABLE recipePath (
    id INTEGER NOT NULL PRIMARY KEY,
    recipe_id INTEGER NOT NULL REFERENCES recipe ON DELETE CASCADE ON UPDATE CASCADE,
    recipePathType_id INTEGER NOT NULL REFERENCES recipePathType ON DELETE CASCADE ON UPDATE RESTRICT
);
CREATE TABLE recipePathEntry (
    id INTEGER NOT NULL PRIMARY KEY,
    recipePath_id INTEGER NOT NULL REFERENCES recipePath ON DELETE CASCADE ON UPDATE CASCADE,
    recipeStep_id INTEGER NOT NULL REFERENCES recipeStep ON DELETE CASCADE ON UPDATE CASCADE,
    position INTEGER NOT NULL,
    displayNo INTEGER NOT NULL,
    isForceDisplay BOOLEAN NOT NULL
);
CREATE TABLE recipePathLang (
    recipePath_id INTEGER NOT NULL REFERENCES recipePath ON DELETE CASCADE ON UPDATE CASCADE,
    locale_id INTEGER NOT NULL REFERENCES locale ON DELETE CASCADE ON UPDATE RESTRICT,
    text CLOB NOT NULL,
    sortKey BLOB NOT NULL,
    PRIMARY KEY (recipePath_id, locale_id)
);
CREATE TABLE recipe_company (
    recipe_id INTEGER NOT NULL REFERENCES recipe ON DELETE CASCADE ON UPDATE CASCADE,
    company_id INTEGER NOT NULL REFERENCES company ON DELETE CASCADE ON UPDATE CASCADE,
    PRIMARY KEY (recipe_id, company_id)
);
CREATE TABLE projectEntryRecipe (
    id INTEGER NOT NULL PRIMARY KEY REFERENCES projectEntry ON DELETE CASCADE ON UPDATE CASCADE,
    recipe_id INTEGER NOT NULL REFERENCES recipe ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE TABLE recipe_materialObject (
    recipe_id INTEGER NOT NULL REFERENCES recipe ON DELETE CASCADE ON UPDATE CASCADE,
    materialObject_id INTEGER NOT NULL REFERENCES materialObject ON DELETE CASCADE ON UPDATE CASCADE,
    position INTEGER NOT NULL,
    isPrimary BOOLEAN DEFAULT 0 NOT NULL,
    PRIMARY KEY (recipe_id, materialObject_id)
);
CREATE TABLE recipeLink (
    targetRecipe_id INTEGER NOT NULL REFERENCES recipe ON DELETE CASCADE ON UPDATE CASCADE,
    sourceRecipe_id INTEGER NOT NULL REFERENCES recipe ON DELETE CASCADE ON UPDATE CASCADE,
    recipeLinkType_id INTEGER NOT NULL REFERENCES recipeLinkType ON DELETE CASCADE ON UPDATE RESTRICT,
    PRIMARY KEY (targetRecipe_id, sourceRecipe_id, recipeLinkType_id)
);
CREATE TABLE ingredient_baseRecipe (
    ingredient_id INTEGER NOT NULL REFERENCES ingredient ON DELETE CASCADE ON UPDATE CASCADE,
    recipe_id INTEGER NOT NULL REFERENCES recipe ON DELETE CASCADE ON UPDATE CASCADE,
    PRIMARY KEY (ingredient_id, recipe_id)
);
CREATE TABLE recipe_tmVersion (
    recipe_id INTEGER NOT NULL REFERENCES recipe ON DELETE CASCADE ON UPDATE CASCADE,
    tmVersion_id INTEGER NOT NULL REFERENCES tmVersion ON DELETE CASCADE ON UPDATE RESTRICT,
    PRIMARY KEY (recipe_id, tmVersion_id)
);
CREATE TABLE recipeStep_tmControl (
    recipeStep_id INTEGER NOT NULL REFERENCES recipeStep ON DELETE CASCADE ON UPDATE CASCADE,
    tmControl_id INTEGER NOT NULL REFERENCES tmControl ON DELETE CASCADE ON UPDATE CASCADE,
    recipe_id INTEGER NOT NULL,
    tmVersion_id INTEGER NOT NULL,
    FOREIGN KEY (tmVersion_id, recipe_id) REFERENCES recipe_tmVersion (tmVersion_id, recipe_id) ON DELETE CASCADE ON UPDATE CASCADE,
    PRIMARY KEY (recipeStep_id, tmControl_id)
);
CREATE TABLE guidedTmSettingStep (
    guidedStep_id INTEGER NOT NULL PRIMARY KEY REFERENCES guidedStep ON DELETE CASCADE ON UPDATE CASCADE,
    recipeStep_id INTEGER NOT NULL,
    tmControl_id INTEGER NOT NULL,
    FOREIGN KEY (tmControl_id, recipeStep_id) REFERENCES recipeStep_tmControl (tmControl_id, recipeStep_id) ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE TABLE guidedTmSettingStepLang (
    guidedTmSettingStep_id INTEGER NOT NULL REFERENCES guidedTmSettingStep ON DELETE CASCADE ON UPDATE CASCADE,
    guidedTmSettingStepLangAttribute_id INTEGER NOT NULL REFERENCES guidedTmSettingStepLangAttribute ON DELETE CASCADE ON UPDATE RESTRICT,
    locale_id INTEGER NOT NULL REFERENCES locale ON DELETE CASCADE ON UPDATE RESTRICT,
    text VARCHAR NOT NULL,
    sortKey BLOB NOT NULL,
    PRIMARY KEY (guidedTmSettingStep_id, guidedTmSettingStepLangAttribute_id, locale_id)
);
CREATE TABLE recipeTime (
    id INTEGER NOT NULL PRIMARY KEY,
    recipe_id INTEGER NOT NULL REFERENCES recipe ON DELETE CASCADE ON UPDATE CASCADE,
    recipeTimeType_id INTEGER NOT NULL REFERENCES recipeTimeType ON DELETE CASCADE ON UPDATE RESTRICT
);
CREATE TABLE recipeTimeRange (
    recipeTime_id INTEGER NOT NULL REFERENCES recipeTime ON DELETE CASCADE ON UPDATE CASCADE,
    rangeType_id INTEGER NOT NULL REFERENCES rangeType ON DELETE CASCADE ON UPDATE RESTRICT,
    secondsValue INTEGER NOT NULL,
    PRIMARY KEY (recipeTime_id, rangeType_id)
);
CREATE TABLE recipeTimeLang (
    recipeTime_id INTEGER NOT NULL REFERENCES recipeTime ON DELETE CASCADE ON UPDATE CASCADE,
    locale_id INTEGER NOT NULL REFERENCES locale ON DELETE CASCADE ON UPDATE RESTRICT,
    text VARCHAR NOT NULL,
    sortKey BLOB NOT NULL,
    PRIMARY KEY (recipeTime_id, locale_id)
);
CREATE TABLE recipeIngredient (
    id INTEGER NOT NULL PRIMARY KEY,
    recipe_id INTEGER NOT NULL REFERENCES recipe ON DELETE CASCADE ON UPDATE CASCADE,
    ingredientNotation_id INTEGER NOT NULL REFERENCES ingredientNotation ON DELETE CASCADE ON UPDATE CASCADE,
    ingredientPreparation_id INTEGER REFERENCES ingredientPreparation ON DELETE CASCADE ON UPDATE CASCADE,
    isCommaSeparated BOOLEAN NOT NULL,
    recipeGroup_id INTEGER REFERENCES recipeGroup ON DELETE CASCADE ON UPDATE CASCADE,
    position INTEGER NOT NULL,
    priority INTEGER NOT NULL,
    unitNotation_id INTEGER REFERENCES unitNotation ON DELETE CASCADE ON UPDATE CASCADE,
    isOptional BOOLEAN DEFAULT 0 NOT NULL
);
CREATE TABLE recipeIngredientRange (
    recipeIngredient_id INTEGER NOT NULL REFERENCES recipeIngredient ON DELETE CASCADE ON UPDATE CASCADE,
    rangeType_id INTEGER NOT NULL REFERENCES rangeType ON DELETE CASCADE ON UPDATE RESTRICT,
    ingredientAmountValue DECIMAL(8,3) NOT NULL,
    PRIMARY KEY (recipeIngredient_id, rangeType_id)
);
CREATE TABLE recipeStep_recipeIngredient (
    recipeStep_id INTEGER NOT NULL REFERENCES recipeStep ON DELETE CASCADE ON UPDATE CASCADE,
    recipeIngredient_id INTEGER NOT NULL REFERENCES recipeIngredient ON DELETE CASCADE ON UPDATE CASCADE,
    position INTEGER NOT NULL,
    PRIMARY KEY (recipeStep_id, recipeIngredient_id)
);
CREATE TABLE guidedIngredientStepIngredient (
    guidedIngredientStep_id INTEGER NOT NULL REFERENCES guidedIngredientStep ON DELETE CASCADE ON UPDATE CASCADE,
    recipeStep_id INTEGER NOT NULL,
    recipeIngredient_id INTEGER NOT NULL,
    ingredientNotation_id INTEGER NOT NULL REFERENCES ingredientNotation ON DELETE CASCADE ON UPDATE CASCADE,
    guidedIngredientStepWeighingAttribute_id INTEGER NOT NULL REFERENCES guidedIngredientStepWeighingAttribute ON DELETE CASCADE ON UPDATE RESTRICT,
    guidedIngredientStepText_id INTEGER NOT NULL REFERENCES guidedIngredientStepText ON DELETE CASCADE ON UPDATE CASCADE,
    unitNotation_id INTEGER DEFAULT NULL REFERENCES unitNotation ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (recipeIngredient_id, recipeStep_id) REFERENCES recipeStep_recipeIngredient (recipeIngredient_id, recipeStep_id) ON DELETE CASCADE ON UPDATE CASCADE,
    PRIMARY KEY (guidedIngredientStep_id, recipeStep_id, recipeIngredient_id)
);
CREATE TABLE guidedIngredientStepIngredientAmountRange (
    guidedIngredientStep_id INTEGER NOT NULL,
    recipeStep_id INTEGER NOT NULL,
    recipeIngredient_id INTEGER NOT NULL,
    rangeType_id INTEGER NOT NULL REFERENCES rangeType ON DELETE CASCADE ON UPDATE RESTRICT,
    amount DECIMAL(8,3) NOT NULL,
    FOREIGN KEY (recipeStep_id, recipeIngredient_id, guidedIngredientStep_id) REFERENCES guidedIngredientStepIngredient (recipeStep_id, recipeIngredient_id, guidedIngredientStep_id) ON DELETE CASCADE ON UPDATE CASCADE,
    PRIMARY KEY (guidedIngredientStep_id, recipeStep_id, recipeIngredient_id, rangeType_id)
);
CREATE TABLE recipeStep_recipeIngredientRange (
    recipeStep_id INTEGER NOT NULL,
    recipeIngredient_id INTEGER NOT NULL,
    rangeType_id INTEGER NOT NULL REFERENCES rangeType ON DELETE CASCADE ON UPDATE RESTRICT,
    ingredientAmountValue DECIMAL(8,3) NOT NULL,
    FOREIGN KEY (recipeIngredient_id, recipeStep_id) REFERENCES recipeStep_recipeIngredient (recipeIngredient_id, recipeStep_id) ON DELETE CASCADE ON UPDATE CASCADE,
    PRIMARY KEY (recipeStep_id, recipeIngredient_id, rangeType_id)
);
CREATE TABLE recipeNutritionalValue (
    id INTEGER NOT NULL PRIMARY KEY,
    recipe_id INTEGER NOT NULL REFERENCES recipe ON DELETE CASCADE ON UPDATE CASCADE,
    servingUnitNotation_id INTEGER NOT NULL REFERENCES unitNotation ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE TABLE recipeNutritionalValuePortionRange (
    recipeNutritionalValue_id INTEGER NOT NULL REFERENCES recipeNutritionalValue ON DELETE CASCADE ON UPDATE CASCADE,
    rangeType_id INTEGER NOT NULL REFERENCES rangeType ON DELETE CASCADE ON UPDATE RESTRICT,
    portionAmountValue DECIMAL(8,3) NOT NULL,
    PRIMARY KEY (recipeNutritionalValue_id, rangeType_id)
);
CREATE TABLE recipeNutritionalValueData (
    recipeNutritionalValue_id INTEGER NOT NULL REFERENCES recipeNutritionalValue ON DELETE CASCADE ON UPDATE CASCADE,
    recipeNutritionalValueType_id INTEGER NOT NULL REFERENCES recipeNutritionalValueType ON DELETE CASCADE ON UPDATE RESTRICT,
    nutritionalValue DECIMAL(8,3) NOT NULL,
    unitNotation_id INTEGER NOT NULL REFERENCES unitNotation ON DELETE CASCADE ON UPDATE CASCADE,
    PRIMARY KEY (recipeNutritionalValue_id, recipeNutritionalValueType_id)
);
CREATE TABLE recipeNutritionLang (
    recipeNutrition_id INTEGER NOT NULL REFERENCES recipeNutritionalValue ON DELETE CASCADE ON UPDATE CASCADE,
    recipeNutritionLangAttribute_id INTEGER NOT NULL REFERENCES recipeNutritionLangAttribute ON DELETE CASCADE ON UPDATE RESTRICT,
    locale_id INTEGER NOT NULL REFERENCES locale ON DELETE CASCADE ON UPDATE RESTRICT,
    text VARCHAR NOT NULL,
    sortKey BLOB NOT NULL,
    PRIMARY KEY (recipeNutrition_id, recipeNutritionLangAttribute_id, locale_id)
);
CREATE TABLE recipe_utensil (
    recipe_id INTEGER NOT NULL REFERENCES recipe ON DELETE CASCADE ON UPDATE CASCADE,
    utensil_id INTEGER NOT NULL REFERENCES utensil ON DELETE CASCADE ON UPDATE CASCADE,
    position INTEGER DEFAULT 0 NOT NULL,
    PRIMARY KEY (recipe_id, utensil_id)
);
CREATE TABLE recipe_recipeCategory (
    recipe_id INTEGER NOT NULL REFERENCES recipe ON DELETE CASCADE ON UPDATE CASCADE,
    recipeCategory_id INTEGER NOT NULL REFERENCES recipeCategory ON DELETE CASCADE ON UPDATE CASCADE,
    PRIMARY KEY (recipe_id, recipeCategory_id)
);
CREATE TABLE recipeLang (
    recipe_id INTEGER NOT NULL REFERENCES recipe ON DELETE CASCADE ON UPDATE CASCADE,
    recipeLangAttribute_id INTEGER NOT NULL REFERENCES recipeLangAttribute ON DELETE CASCADE ON UPDATE RESTRICT,
    locale_id INTEGER NOT NULL REFERENCES locale ON DELETE CASCADE ON UPDATE RESTRICT,
    text VARCHAR NOT NULL,
    sortKey BLOB NOT NULL,
    PRIMARY KEY (recipe_id, recipeLangAttribute_id, locale_id)
);
CREATE INDEX maintenanceCloudSyncHash_maintenanceHashType_idx
 ON maintenanceCloudSyncHash
 ( maintenanceHashType_id ASC );
CREATE UNIQUE INDEX maintenanceMediaHash_idx
 ON maintenanceMediaHash
 ( fileURI ASC );
CREATE INDEX recipeRecentlySynced_idx
 ON recipeRecentlySynced
 ( timestamp ASC );
CREATE UNIQUE INDEX maintenanceRecipeCollectionHash_idx
 ON maintenanceRecipeCollectionHash
 ( project_id );
CREATE INDEX projectEntry_project_id_idx
 ON projectEntry
 ( project_id ASC );
CREATE INDEX recipeCategory_isPrimary_idx
 ON recipeCategory
 ( isPrimary DESC );
CREATE UNIQUE INDEX companyLocale_company_locale_idx
 ON companyLocale
 ( company_id, locale_id );
CREATE UNIQUE INDEX recipeSignature_idx
 ON recipeSignature
 ( recipe_id, recipeSignatureType_id );
CREATE INDEX recipeImport_idx
 ON recipeImport
 ( recipe_id );
CREATE UNIQUE INDEX maintenanceRecipeHash_idx
 ON maintenanceRecipeHash
 ( recipe_id );
CREATE INDEX recipeStep_idx
 ON recipeStep
 ( recipe_id );
CREATE INDEX guidedStep_idx
 ON guidedStep
 ( recipeStep_id );
CREATE INDEX recipeIngredient_recipe_id_idx
 ON recipeIngredient
 ( recipe_id ASC );
CREATE INDEX recipe_recipeCategory_recipeCategory_id_idx
 ON recipe_recipeCategory
 ( recipeCategory_id ASC );
CREATE INDEX recipeLang_langAttr_localeId_idx
 ON recipeLang
 ( recipeLangAttribute_id, locale_id );

-- EXTRA TABLES

CREATE TABLE recipe_recipeGroup (
    recipe_id INTEGER NOT NULL REFERENCES recipe ON DELETE CASCADE ON UPDATE CASCADE,
    recipeGroup_id INTEGER NOT NULL REFERENCES recipeGroup ON DELETE CASCADE ON UPDATE CASCADE,
    PRIMARY KEY (recipe_id, recipeGroup_id)
);

CREATE INDEX ingredientNotationLang_text_idx
    ON ingredientNotationLang
        ( text );

-- CREATE INDEX recipeLang_text_idx
--     ON recipeLang
--         ( text );
-- CREATE INDEX recipeLang_sortkey_idx
--     ON recipeLang
--         ( sortKey );
CREATE INDEX recipeLang_text_localeId_idx
    ON recipeLang
        ( text, locale_id );
CREATE INDEX recipeLang_sortkey_localeId_idx
    ON recipeLang
        ( sortKey, locale_id );

-- CREATE INDEX recipeCategoryLang_text_idx
--     ON recipeCategoryLang
--         ( text );
-- CREATE INDEX recipeCategoryLang_sortKey_idx
--     ON recipeCategoryLang
--         ( sortKey );
CREATE INDEX recipeCategoryLang_langAttr_localeId_idx
    ON recipeCategoryLang
        ( recipeCategoryLangAttribute_id, locale_id );
CREATE INDEX recipeCategoryLang_text_localeId_idx
    ON recipeCategoryLang
        ( text, locale_id );
CREATE INDEX recipeCategoryLang_sortKey_localeId_idx
    ON recipeCategoryLang
        ( sortKey, locale_id );