package com.recipeton.shared.analysis.cooki.en;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.configuration.RecipeAnalyzerConfiguration;
import com.recipeton.shared.analysis.configuration.cooki.RecipeAnalyzerCookiEnConfiguration;
import com.recipeton.shared.analysis.domain.RecipeIngredientAnalysis;
import com.recipeton.shared.analysis.domain.RecipeIngredientTerm;
import com.recipeton.shared.analysis.domain.RecipeToolTerm;
import com.recipeton.shared.analysis.service.RecipeInstructionAnalyzer;
import com.recipeton.shared.analysis.test.support.RecipetonAnalyzerTestAssertTrait;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.stream.Stream;

import static com.recipeton.shared.analysis.domain.RecipeInstructionTag.*;
import static com.recipeton.shared.analysis.domain.RecipeToolComposedActionTag.*;
import static com.recipeton.shared.analysis.domain.RecipeToolTag.*;
import static java.util.List.of;
import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
public class RecipeInstructionAnalyzerCookiEnSequenceTest implements RecipetonAnalyzerTestAssertTrait {

    private final RecipeAnalyzerConfiguration configuration = new RecipeAnalyzerCookiEnConfiguration();
    private final List<RecipeIngredientAnalysis> empty = createRecipeIngredientAnalyses(configuration, "");
    private final RecipeInstructionAnalyzer analyzer = createRecipeInstructionAnalyzer(configuration);

    @Test
    void testAnalyzeGivenAddInstructionAndCommandPreceededByQualificationThenSplitsSequenceAndAddsTags() {
        Stream.of(
                        "Add rice and sauté without measuring cup, [3 min/100°C/speed 1]",
                        "Add rice and sauté, without measuring cup, <nobr>3 min/100°C/speed 1</nobr>",
                        "Add rice and, without measuring cup, sauté <nobr>3 min/100°C/speed 1</nobr>",
                        "Add rice and sauté <nobr>3 min/100°C/speed 1</nobr>, without measuring cup.")
                .forEach(
                        s -> {
                            assertThat(analyzer.analyze(of(s), createRecipeIngredientAnalyses(configuration, "rice")))
                                    .hasSize(3)
                                    .satisfies(
                                            rs -> {
                                                assertThat(rs)
                                                        .element(0)
                                                        .satisfies(
                                                                ria -> {
                                                                    assertThat(ria.getText()).isEqualTo("Add rice");
                                                                    assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_ADD);
                                                                });
                                                assertThat(rs)
                                                        .element(1)
                                                        .satisfies(
                                                                ria -> {
                                                                    assertThat(ria.getText()).isEqualTo("Without measuring cup");
                                                                    assertThat(ria.getTags()).containsExactlyInAnyOrder(MEASURING_CUP_NOTUSE, TOOL_INTERNAL);
                                                                });
                                                assertThat(rs)
                                                        .element(2)
                                                        .satisfies(
                                                                ria -> {
                                                                    assertThat(ria.getText()).isEqualTo("Sauté [3 min/100°C/speed 1]");
                                                                    assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_COMMAND_MAIN, FOCUS_MAIN);
                                                                });
                                            });
                        });
    }

    @Test
    void testAnalyzeGivenCloseSteamerAndCookThenSplitsSequenceAndAddsTags() {
        assertThat(analyzer.analyze(of("Secure Varoma lid and cook <nobr>7 min</nobr>"), createRecipeIngredientAnalyses(configuration)))
                .hasSize(2)
                .satisfies(
                        rs -> {
                            assertThat(rs).element(0).satisfies(ria -> assertThat(ria.getTags()).containsExactlyInAnyOrder(STEAMER_CLOSE, TOOL_INTERNAL));
                            assertThat(rs).element(1).satisfies(ria -> assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_COMMAND_MAIN, FOCUS_MAIN));
                        });
    }

    @Test
    void testAnalyzeGivenCommandInstructionWithModifiersThenAddsTagsAndFormatsText() {
        Stream.of("Mix without setting a time [speed 1]", "Without setting a time mix [speed 1]", "Without setting a time, mix [speed 1]")
                .forEach(
                        s ->
                                assertThat(analyzer.analyze(of(s), createRecipeIngredientAnalyses(configuration, "rice")))
                                        .singleElement()
                                        .satisfies(
                                                ria -> {
                                                    assertThat(ria.getText()).isEqualTo("Mix without setting a time [speed 1]");
                                                    assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_COMMAND_MAIN, FOCUS_MAIN);
                                                }));
    }

    @Test
    void testAnalyzeGivenCommandPreceededByPreparationThenSplitsSequenceAndAddsTags() {
        Stream.of("Sauté without measuring cup, <nobr>3 min/100°C/speed 1</nobr>", "Sauté, without measuring cup, <nobr>3 min/100°C/speed 1</nobr>", "Without measuring cup, sauté <nobr>3 min/100°C/speed 1</nobr>")
                .forEach(
                        s ->
                                assertThat(analyzer.analyze(of(s), empty))
                                        .hasSize(2)
                                        .satisfies(
                                                rs -> {
                                                    assertThat(rs)
                                                            .element(0)
                                                            .satisfies(
                                                                    ria -> {
                                                                        assertThat(ria.getText()).isEqualTo("Without measuring cup");
                                                                        assertThat(ria.getTags()).containsExactlyInAnyOrder(MEASURING_CUP_NOTUSE, TOOL_INTERNAL);
                                                                    });
                                                    assertThat(rs)
                                                            .element(1)
                                                            .satisfies(
                                                                    ria -> {
                                                                        assertThat(ria.getText()).isEqualTo("Sauté [3 min/100°C/speed 1]");
                                                                        assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_COMMAND_MAIN, FOCUS_MAIN);
                                                                    });
                                                }));
    }

    @Test
    void testAnalyzeGivenCommandSucceededByPreparationThenModifiesAndSplitsSequenceAndAddsTags() {
        Stream.of("Sauté <nobr>3 min/100°C/speed 1</nobr>, without measuring cup.")
                .forEach(
                        s -> {
                            assertThat(analyzer.analyze(of(s), empty))
                                    .hasSize(2)
                                    .satisfies(
                                            rs -> {
                                                assertThat(rs)
                                                        .element(0)
                                                        .satisfies(
                                                                ria -> {
                                                                    assertThat(ria.getText()).isEqualTo("Without measuring cup");
                                                                    assertThat(ria.getTags()).containsExactlyInAnyOrder(MEASURING_CUP_NOTUSE, TOOL_INTERNAL);
                                                                });
                                                assertThat(rs)
                                                        .element(1)
                                                        .satisfies(
                                                                ria -> {
                                                                    assertThat(ria.getText()).isEqualTo("Sauté [3 min/100°C/speed 1]");
                                                                    assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_COMMAND_MAIN, FOCUS_MAIN);
                                                                });
                                            });
                        });
    }

    @Test
    void testAnalyzeGivenComposedCommandInstructionWithModifiersThenAddsTagsAndFormatsText() {
        Stream.of(
                        "Add rice and mix without setting a time [speed 1]",
                        "Add rice, mix without setting a time [speed 1]",
                        "Add rice and mix, without setting a time, [speed 1]",
                        "Add rice and without setting a time mix [speed 1]",
                        "Add rice and, without setting a time, mix [speed 1]",
                        "Add rice then, without setting a time, mix [speed 1]",
                        "Add rice and then, without setting a time, mix [speed 1]")
                .forEach(
                        s ->
                                assertThat(analyzer.analyze(of(s), createRecipeIngredientAnalyses(configuration, "rice")))
                                        .hasSize(2)
                                        .satisfies(
                                                rs -> {
                                                    assertThat(rs)
                                                            .element(0)
                                                            .satisfies(
                                                                    ria -> {
                                                                        assertThat(ria.getText()).isEqualTo("Add rice");
                                                                        assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_ADD);
                                                                    });
                                                    assertThat(rs)
                                                            .element(1)
                                                            .satisfies(
                                                                    ria -> {
                                                                        assertThat(ria.getText()).isEqualTo("Mix without setting a time [speed 1]");
                                                                        assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_COMMAND_MAIN, FOCUS_MAIN);
                                                                    });
                                                }));
    }

    @Test
    void testAnalyzeGivenCreamAsActionThenSplits() {
        assertThat(analyzer.analyze(of("Xx and cream <nobr>1 min</nobr>"), empty))
                .hasSize(2)
                .satisfies(
                        rs -> {
                            assertThat(rs)
                                    .element(0)
                                    .satisfies(
                                            ria -> {
                                                assertThat(ria.getText()).isEqualTo("Xx");
                                            });
                            assertThat(rs)
                                    .element(1)
                                    .satisfies(
                                            ria -> {
                                                assertThat(ria.getText()).isEqualTo("Cream [1 min]");
                                                assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_COMMAND_MAIN, FOCUS_MAIN);
                                            });
                        });
    }

    @Test
    void testAnalyzeGivenCreamAsIngredientThenDoesNotSplit() {
        assertThat(analyzer.analyze(of("Add banana and cream of tartar"), empty)).singleElement().satisfies(ria -> assertThat(ria.getText()).isEqualTo("Add banana and cream of tartar"));
    }

    @Test
    void testAnalyzeGivenInstructionWithPlaceInExternalCompartmentThenAddsTags() {
        assertThat(analyzer.analyze(of("Place almond flour and salt into bowl."), empty))
                .singleElement()
                .satisfies(ria -> assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_PLACE, TOOL_EXTERNAL, FOCUS_EXTERNAL, IS_ITEM_FROM_TO_TRANSFER));
    }

    @Test
    void testAnalyzeGivenInstructionWithPlaceInMainLocationThenAddsTags() {
        assertThat(analyzer.analyze(of("Place almond flour and salt into mixing bowl."), createRecipeIngredientAnalyses(configuration, "almond flour", "salt")))
                .singleElement()
                .satisfies(ria -> assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_PLACE, FOCUS_MAIN, TOOL_MAIN, IS_INGREDIENT_ADDITION, IS_ITEM_FROM_TO_TRANSFER));
    }

    @Test
    void testAnalyzeGivenInstructionWithTransferInExtenelCompartmentThenAddsTags() {
        assertThat(analyzer.analyze(of("Transfer almond flour and salt into bowl."), createRecipeIngredientAnalyses(configuration, "almond flour", "salt")))
                .singleElement()
                .satisfies(ria -> assertThat(ria.getTags()).containsExactlyInAnyOrder(TRANSFER_FROM_BOWL, TOOL_EXTERNAL, FOCUS_EXTERNAL, IS_ITEM_FROM_TO_TRANSFER));
    }

    @Test
    void testAnalyzeGivenInstructionsCommandWithParallelNonActionExplanationThenReturnsInstruction() {

        Stream.of("Cook <nobr>8 min/100°C/speed 3</nobr>. Meanwhile, sleep", "Cook <nobr>8 min/100°C/speed 3</nobr>, meanwhile, sleep")
                .forEach(
                        s ->
                                assertThat(analyzer.analyze(of(s), createRecipeIngredientAnalyses(configuration, "")))
                                        .hasSize(2)
                                        .satisfies(
                                                l -> {
                                                    assertThat(l)
                                                            .element(0)
                                                            .satisfies(
                                                                    ria -> {
                                                                        assertThat(ria.getText()).isEqualTo("Cook [8 min/100°C/speed 3]");
                                                                        assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_COMMAND_MAIN, FOCUS_MAIN);
                                                                    });
                                                    assertThat(l)
                                                            .element(1)
                                                            .satisfies(
                                                                    ria -> {
                                                                        assertThat(ria.getText()).isEqualTo("Meanwhile, sleep");
                                                                        assertThat(ria.getTags()).containsExactlyInAnyOrder(SEQUENCE_PARALLEL, QUALIFIES_PREVIOUS);
                                                                    });
                                                }));
    }

    @Test
    void testAnalyzeGivenInstructionsWithPlaceCommandAdditionCommandSequenceThenInstructionsSplittedIn4() {
        assertThat(
                        analyzer.analyze(
                                of("Place almond flour and salt into mixing bowl and mix <nobr>11 sec/speed 4</nobr>.", "Add frozen stuff and mix <nobr>4 sec/speed 9</nobr>."),
                                createRecipeIngredientAnalyses(configuration, "almond flour", "salt")))
                .hasSize(4)
                .satisfies(
                        l -> {
                            assertThat(l)
                                    .element(0)
                                    .satisfies(
                                            ria -> {
                                                assertThat(ria.getText()).isEqualTo("Place almond flour and salt into mixing bowl");
                                                assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_PLACE, FOCUS_MAIN, TOOL_MAIN, IS_INGREDIENT_ADDITION, IS_ITEM_FROM_TO_TRANSFER);
                                            });
                            assertThat(l)
                                    .element(1)
                                    .satisfies(
                                            ria -> {
                                                assertThat(ria.getText()).isEqualTo("Mix [11 sec/speed 4]");
                                                assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_COMMAND_MAIN, DO_COMMAND_MAIN_PREPARATION_DEFAULT, FOCUS_MAIN);
                                            });
                            assertThat(l)
                                    .element(2)
                                    .satisfies(
                                            ria -> {
                                                assertThat(ria.getText()).isEqualTo("Add frozen stuff");
                                                assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_ADD, FOCUS_MAIN, IS_INGREDIENT_ADDITION, IS_INGREDIENT_POTENTIAL_MISMATCH);
                                            });
                            assertThat(l)
                                    .element(3)
                                    .satisfies(
                                            ria -> {
                                                assertThat(ria.getText()).isEqualTo("Mix [4 sec/speed 9]");
                                                assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_COMMAND_MAIN, DO_COMMAND_MAIN_PREPARATION_DEFAULT, FOCUS_MAIN);
                                            });
                        });
    }

    @Test
    void testAnalyzeGivenInstructionsWithPlaceCommandAndPlaceSomewhereElseThenSomewehereElseIsNotInMain() {
        assertThat(analyzer.analyze(of("Place butter and sugar in mixing bowl then mix [2 min]. Place trays in fridge for 1½ hours"), createRecipeIngredientAnalyses(configuration, "butter", "sugar")))
                .hasSize(3)
                .satisfies(
                        l -> {
                            assertThat(l)
                                    .element(0)
                                    .satisfies(
                                            ria -> {
                                                assertThat(ria.getText()).isEqualTo("Place butter and sugar in mixing bowl");
                                                assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_PLACE, FOCUS_MAIN, TOOL_MAIN, IS_INGREDIENT_ADDITION, IS_ITEM_FROM_TO_TRANSFER);
                                            });
                            assertThat(l)
                                    .element(1)
                                    .satisfies(
                                            ria -> {
                                                assertThat(ria.getText()).isEqualTo("Mix [2 min]");
                                                assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_COMMAND_MAIN, DO_COMMAND_MAIN_PREPARATION_DEFAULT, FOCUS_MAIN);
                                            });
                            assertThat(l)
                                    .element(2)
                                    .satisfies(
                                            ria -> {
                                                assertThat(ria.getText()).isEqualTo("Place trays in fridge for 1½ hours");
                                                assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_PLACE, TOOL_EXTERNAL, REFRIGERATE, FOCUS_EXTERNAL, IS_ITEM_FROM_TO_TRANSFER);
                                            });
                        });
    }

    @Test
    void testAnalyzeGivenInstructionsWithPlaceSomewhereExternalThenLocationIsExternal() {
        Stream.of("Place in reserved tray", "Place on prepared tray")
                .forEach(
                        t ->
                                assertThat(analyzer.analyze(of(t), empty))
                                        .singleElement()
                                        .satisfies(
                                                ria -> {
                                                    assertThat(ria.getText()).isEqualTo(t);
                                                    assertThat(ria.getTags()).contains(DO_PLACE, FOCUS_EXTERNAL, TOOL_EXTERNAL);
                                                }));
    }

    @Test
    void testAnalyzeGivenInstructionsWithReservedThenAddsTag() {
        Stream.of("Place in reserved tray")
                .forEach(
                        t ->
                                assertThat(analyzer.analyze(of(t), empty))
                                        .singleElement()
                                        .satisfies(
                                                ria -> {
                                                    assertThat(ria.getText()).isEqualTo(t);
                                                    assertThat(ria.getTags()).contains(DO_PLACE, FOCUS_EXTERNAL, TOOL_EXTERNAL, IS_USE_RESERVED);
                                                }));
    }

    @Test
    void testAnalyzeGivenPlaceAndPlaceSteamerInstructionsThenSplitsSequenceAndAddsTags() {
        //            "Add broccolini to Varoma dish."
        assertThat(
                        analyzer.analyze(
                                of("Place water into mixing bowl. Place Varoma into position and line Varoma dish with a piece baking paper. Place broccolini onto Varoma dish."),
                                createRecipeIngredientAnalyses(configuration, "water", "broccolini")))
                .hasSize(4)
                .satisfies(
                        rs -> {
                            assertThat(rs).element(0).satisfies(ria -> assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_PLACE, TOOL_MAIN, FOCUS_MAIN, IS_INGREDIENT_ADDITION, IS_ITEM_FROM_TO_TRANSFER));
                            assertThat(rs).element(1).satisfies(ria -> assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_PLACE, TOOL_INTERNAL, FOCUS_ON_TOP, STEAMER_PUT, IS_ITEM_FROM_TO_TRANSFER));
                            assertThat(rs).element(2).satisfies(ria -> assertThat(ria.getTags()).containsExactlyInAnyOrder(TOOL_INTERNAL));
                            assertThat(rs).element(3).satisfies(ria -> assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_PLACE, TOOL_INTERNAL, IS_ITEM_FROM_TO_TRANSFER, FOCUS_UNDEFINED));
                        });
    }

    @Test
    void testAnalyzeGivenPlaceInstructionAndTransferInstructionWithSetAsideAndAddWithCommandThenAddIsLocationMain() {
        assertThat(analyzer.analyze(of("Place potato in mixing bowl. Transfer to bowl and set aside. Add banana and simmer [3 min]"), createRecipeIngredientAnalyses(configuration, "water", "potato", "banana")))
                .hasSize(5)
                .satisfies(
                        rs -> {
                            assertThat(rs).element(0).satisfies(ria -> assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_PLACE, TOOL_MAIN, FOCUS_MAIN, IS_INGREDIENT_ADDITION, IS_ITEM_FROM_TO_TRANSFER));
                            assertThat(rs).element(1).satisfies(ria -> assertThat(ria.getTags()).containsExactlyInAnyOrder(TRANSFER_FROM_BOWL, FOCUS_EXTERNAL, TOOL_EXTERNAL, IS_ITEM_FROM_TO_TRANSFER));
                            assertThat(rs).element(2).satisfies(ria -> assertThat(ria.getTags()).containsExactlyInAnyOrder(FOCUS_UNDEFINED));
                            assertThat(rs).element(3).satisfies(ria -> assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_ADD, FOCUS_MAIN, IS_INGREDIENT_ADDITION));
                            assertThat(rs).element(4).satisfies(ria -> assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_COMMAND_MAIN, DO_COMMAND_MAIN_PREPARATION_DEFAULT, FOCUS_MAIN));
                        });
    }

    @Test
    void testAnalyzeGivenPlaceInstructionWithUtensilInstructionIntendedForFollowingStepThenSplitted() {
        assertThat(analyzer.analyze(of("Place XXX in mixing bowl. Place simmering basket instead of measuring cup and simmer for 10 minutes"), createRecipeIngredientAnalyses(configuration, "XXX")))
                .hasSize(3)
                .satisfies(
                        rs -> {
                            assertThat(rs).element(0).satisfies(ria -> assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_PLACE, TOOL_MAIN, FOCUS_MAIN, IS_INGREDIENT_ADDITION, IS_ITEM_FROM_TO_TRANSFER));
                            assertThat(rs).element(1).satisfies(ria -> assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_PLACE, FOCUS_MAIN, TOOL_INTERNAL, SIMMERING_BASKET_AS_LID_PUT));
                        });
    }

    @Test
    void testAnalyzeGivenPlaceInstructionWithUtensilInstructionIntendedForPreviousStepThenSplittedAndQualifiesPreviousTagAdded() {
        assertThat(analyzer.analyze(of("Place XXX in mixing bowl, placing simmering basket instead of measuring cup"), createRecipeIngredientAnalyses(configuration, "XXX")))
                .hasSize(2)
                .satisfies(
                        rs -> {
                            assertThat(rs).element(0).satisfies(ria -> assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_PLACE, TOOL_MAIN, FOCUS_MAIN, IS_INGREDIENT_ADDITION, IS_ITEM_FROM_TO_TRANSFER));
                            assertThat(rs).element(1).satisfies(ria -> assertThat(ria.getTags()).containsExactlyInAnyOrder(TOOL_INTERNAL, SIMMERING_BASKET_AS_LID_PUT, QUALIFIES_PREVIOUS));
                        });
    }

    @Test
    void testAnalyzeGivenPlaceMainCompartmentAndThenAddIngredientInstructionsThenAddIngredientIsInMainLocation() {
        assertThat(analyzer.analyze(of("Place mixing bowl back into position. Add tomato"), createRecipeIngredientAnalyses(configuration, "tomato")))
                .hasSize(2)
                .satisfies(
                        l -> {
                            assertThat(l).element(0).satisfies(ria -> assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_PLACE, MIXING_BOWL_PUT, FOCUS_MAIN, TOOL_MAIN, IS_ITEM_FROM_TO_TRANSFER));
                            assertThat(l).element(1).satisfies(ria -> assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_ADD, FOCUS_MAIN, IS_INGREDIENT_ADDITION));
                        });
    }

    @Test
    void testAnalyzeInstructionGivenCommandWithoutVerbThenSplits() {
        assertThat(analyzer.analyze(of("<nobr>5 min/speed 3.5</nobr>"), empty)).singleElement().satisfies(ria -> assertThat(ria.getText()).isEqualTo("Set [5 min/speed 3.5]"));
    }

    @Test
    void testAnalyzeInstructionGivenComposedCommandWithoutVerbThenSplits() {
        Stream.of("Xxx, then <nobr>5 min/speed 3.5</nobr>")
                .forEach(
                        s ->
                                assertThat(analyzer.analyze(of(s), empty))
                                        .hasSize(2)
                                        .satisfies(
                                                l -> {
                                                    assertThat(l).element(0).satisfies(ria -> assertThat(ria.getText()).isEqualTo("Xxx"));
                                                    assertThat(l).element(1).satisfies(ria -> assertThat(ria.getText()).isEqualTo("Set [5 min/speed 3.5]"));
                                                }));
    }

    @Test
    void testAnalyzeInstructionGivenNotSplittedCommandWithoutVerbThenSplits() {
        Stream.of("Xxx. <nobr>5 min/speed 3.5</nobr>")
                .forEach(
                        s ->
                                assertThat(analyzer.analyze(of(s), empty))
                                        .hasSize(2)
                                        .satisfies(
                                                l -> {
                                                    assertThat(l).element(0).satisfies(ria -> assertThat(ria.getText()).isEqualTo("Xxx"));
                                                    assertThat(l).element(1).satisfies(ria -> assertThat(ria.getText()).isEqualTo("Set [5 min/speed 3.5]"));
                                                }));
    }

    @Test
    void testGetTermsSortedGiveInstructionWithCommandActionAundUtensilPreparationThenInstructionsAnalysisContainsCommandAndUtensilTermAndUtensilTermIsInOrderBeforeCommandTerm() {
        assertThat(analyzer.analyze(of("Sauté [10 min/steam/stir/speed 1], without measuring cup"), empty))
                .hasSize(2)
                .satisfies(
                        l -> {
                            assertThat(l)
                                    .element(0)
                                    .satisfies(
                                            ria -> {
                                                assertThat(ria.getText()).isEqualTo("Without measuring cup");
                                                assertThat(ria.getTags()).containsExactlyInAnyOrder(MEASURING_CUP_NOTUSE, TOOL_INTERNAL);
                                            });
                            assertThat(l)
                                    .element(1)
                                    .satisfies(
                                            ria -> {
                                                assertThat(ria.getText()).isEqualTo("Sauté [10 min/steam/stir/speed 1]");
                                                assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_COMMAND_MAIN, FOCUS_MAIN);
                                            });
                        });
    }

    @Test
    void testGetTermsSortedGivenInstructionWithIngredientsThenReturnsIngredientsInOrder() {
        assertThat(analyzer.analyze(of("Place egg, tomato and potato into mixing bowl"), createRecipeIngredientAnalyses(configuration, "potato", "egg", "tomato")))
                .singleElement()
                .satisfies(ria -> assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_PLACE, TOOL_MAIN, FOCUS_MAIN, IS_INGREDIENT_ADDITION, IS_ITEM_FROM_TO_TRANSFER))
                .satisfies(
                        ria ->
                                assertThat(analyzer.getTermsByConclusiveAndSortedByTypeAndOffset(ria))
                                        .hasSize(11)
                                        .anySatisfy(t -> assertThat(t).isInstanceOfSatisfying(RecipeIngredientTerm.class, rit -> assertThat(rit.getIngredient().getIngredientNotation()).isEqualTo("egg")))
                                        .anySatisfy(t -> assertThat(t).isInstanceOfSatisfying(RecipeIngredientTerm.class, rit -> assertThat(rit.getIngredient().getIngredientNotation()).isEqualTo("tomato")))
                                        .anySatisfy(t -> assertThat(t).isInstanceOfSatisfying(RecipeIngredientTerm.class, rit -> assertThat(rit.getIngredient().getIngredientNotation()).isEqualTo("potato")))
                                        .anySatisfy(t -> assertThat(t).isInstanceOfSatisfying(RecipeToolTerm.class, rit -> assertThat(rit.getTool().getName()).isEqualTo("mixing bowl"))));
    }

    @Test
    void testSplitInstructionAndCommandGivenInstructionWithSplitThenSplits() {
        Stream.of("Xx and cook <nobr>1 min</nobr>", "Xx, cook <nobr>1 min</nobr>", "Xx,cook <nobr>1 min</nobr>")
                .forEach(
                        s ->
                                assertThat(analyzer.analyze(of(s), empty))
                                        .hasSize(2)
                                        .satisfies(
                                                l -> {
                                                    assertThat(l)
                                                            .element(0)
                                                            .satisfies(
                                                                    ria -> {
                                                                        assertThat(ria.getText()).isEqualTo("Xx");
                                                                    });
                                                    assertThat(l)
                                                            .element(1)
                                                            .satisfies(
                                                                    ria -> {
                                                                        assertThat(ria.getText()).isEqualTo("Cook [1 min]");
                                                                    });
                                                }));
    }
}
