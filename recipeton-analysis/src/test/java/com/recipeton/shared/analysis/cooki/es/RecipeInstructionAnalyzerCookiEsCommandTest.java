package com.recipeton.shared.analysis.cooki.es;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.configuration.RecipeAnalyzerConfiguration;
import com.recipeton.shared.analysis.configuration.cooki.RecipeAnalyzerCookiEsConfiguration;
import com.recipeton.shared.analysis.domain.RecipeCommandProgram;
import com.recipeton.shared.analysis.domain.RecipeCommandRotation;
import com.recipeton.shared.analysis.test.support.RecipetonAnalyzerTestAssertTrait;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.Duration;

import static com.recipeton.shared.analysis.configuration.cooki.CookiConstants.COMMAND_SPEED_SLOW_VALUE;
import static com.recipeton.shared.analysis.configuration.cooki.CookiConstants.STEAMER_TEMPERATURE;
import static java.util.List.of;
import static org.assertj.core.api.Assertions.assertThat;

class RecipeInstructionAnalyzerCookiEsCommandTest implements RecipetonAnalyzerTestAssertTrait {

    private final RecipeAnalyzerConfiguration configuration = new RecipeAnalyzerCookiEsConfiguration();

    @Test
    public void testAcceptGivenInstructionThenTermsContainsSource() {
        String expected = "2 min/120°C/vel \uE002";
        assertThatRecipeCommandTermWrapping(expected, configuration).satisfies(c -> assertThat(c.getText()).isEqualTo(expected));
    }

    @Test
    public void testAcceptGivenInstructionWithCombinationOfIngredientAndCommandThenCommandPositionIsGreaterThanIngredientPosition() {

        assertThat(
                        createRecipeInstructionAnalyzer(configuration)
                                .analyze(of("Añada los camarones, y pique <nobr>5 seg/vel 5.5</nobr>, la mezcla debe tener algunos trozos visibles de camarón"), createRecipeIngredientAnalyses(configuration)))
                .element(1)
                .satisfies(
                        ria ->
                                assertThat(ria.getCommandTerms())
                                        .singleElement()
                                        .satisfies(
                                                c -> {
                                                    assertThat(c.getMatch().getOffset()).isEqualTo(7);
                                                    assertThat(c.getSpeed()).isEqualTo(new BigDecimal("5.5"));
                                                    assertThat(c.getDuration()).isEqualTo(Duration.ofSeconds(5));
                                                }));
    }

    @Test
    public void testAcceptGivenInstructionWithDoughTermsContainsDough() {
        assertThatRecipeCommandTermWrapping("Amasar \uE001/1 min", configuration)
                .satisfies(
                        c -> {
                            assertThat(c.getProgram()).isEqualTo(RecipeCommandProgram.DOUGH);
                            assertThat(c.getDuration()).isEqualTo(Duration.ofMinutes(1));
                        });
    }

    @Test
    public void testAcceptGivenInstructionWithDurationHalfSecondsThenTermsContainsDuration() {
        assertThatRecipeCommandTermWrapping("0.5 seg", configuration).satisfies(t -> assertThat(t.getDuration()).isEqualTo(Duration.ofMillis(500)));
    }

    @Test
    public void testAcceptGivenInstructionWithDurationHoursThenTermsContainsDuration() {
        assertThatRecipeCommandTermWrapping("5 h", configuration).satisfies(t -> assertThat(t.getDuration()).isEqualTo(Duration.ofHours(5)));
    }

    @Test
    public void testAcceptGivenInstructionWithDurationMinutesThenTermsContainsDuration() {
        assertThatRecipeCommandTermWrapping("5 min", configuration).satisfies(t -> assertThat(t.getDuration()).isEqualTo(Duration.ofMinutes(5)));
    }

    @Test
    public void testAcceptGivenInstructionWithDurationSecondsThenTermsContainsDuration() {
        assertThatRecipeCommandTermWrapping("5 seg", configuration).satisfies(t -> assertThat(t.getDuration()).isEqualTo(Duration.ofSeconds(5)));
    }

    @Test
    public void testAcceptGivenInstructionWithNoRecognizedContentThenTermsEmpty() {
        assertThat(createRecipeInstructionAnalyzer(configuration).analyze(of(createWrappedCommand("otherjunk", configuration)), createRecipeIngredientAnalyses(configuration)))
                .singleElement()
                .satisfies(ria -> assertThat(ria.getCommandTerms()).isEmpty());
    }

    @Test
    public void testAcceptGivenInstructionWithReverseRotationCodeThenTermsContainsReverseRotation() {
        assertThatRecipeCommandTermWrapping("5 seg/\uE003/vel 4", configuration).satisfies(c -> assertThat(c.getRotation()).isEqualTo(RecipeCommandRotation.REVERSE));
    }

    @Test
    public void testAcceptGivenInstructionWithReverseRotationThenTermsContainsReverseRotation() {
        assertThatRecipeCommandTermWrapping("5 seg/reverse/vel 4", configuration).satisfies(c -> assertThat(c.getRotation()).isEqualTo(RecipeCommandRotation.REVERSE));
    }

    @Test
    public void testAcceptGivenInstructionWithSpeedCucharaThenTermsContainsSpeedSlow() {
        assertThatRecipeCommandTermWrapping("vel cuchara", configuration).satisfies(c -> assertThat(c.getSpeed()).isEqualTo(COMMAND_SPEED_SLOW_VALUE));
    }

    @Test
    public void testAcceptGivenInstructionWithSpeedNumericFractionThenTermsContainsSpeed() {
        assertThatRecipeCommandTermWrapping("vel 0.5", configuration).satisfies(c -> assertThat(c.getSpeed()).isEqualTo(new BigDecimal("0.5")));
    }

    @Test
    public void testAcceptGivenInstructionWithSpeedNumericThenTermsContainsSpeed() {
        assertThatRecipeCommandTermWrapping("vel 5", configuration).satisfies(c -> assertThat(c.getSpeed()).isEqualTo(new BigDecimal(5)));
    }

    @Test
    public void testAcceptGivenInstructionWithSpeedSlowThenTermsContainsSpeed() {
        assertThatRecipeCommandTermWrapping("vel \uE002", configuration).satisfies(t -> assertThat(t.getSpeed()).isEqualTo(new BigDecimal("0.3")));
    }

    @Test
    public void testAcceptGivenInstructionWithSpeedSlowThenTermsContainsSpeedSlow() {
        assertThatRecipeCommandTermWrapping("vel slow", configuration).satisfies(c -> assertThat(c.getSpeed()).isEqualTo(COMMAND_SPEED_SLOW_VALUE));
    }

    @Test
    public void testAcceptGivenInstructionWithSpeedThenTermsContainsSpeed() {
        assertThatRecipeCommandTermWrapping("vel 1", configuration).satisfies(t -> assertThat(t.getSpeed()).isEqualTo(new BigDecimal("1")));
    }

    @Test
    public void testAcceptGivenInstructionWithTemperatureNumericThenTermsContainsTemperature() {
        assertThatRecipeCommandTermWrapping("50°C", configuration).satisfies(c -> assertThat(c.getTemperature()).isEqualTo((long) 50));
    }

    @Test
    public void testAcceptGivenInstructionWithTemperatureSpecialThenTermsContainsTemperature() {
        assertThatRecipeCommandTermWrapping("Varoma", configuration).satisfies(c -> assertThat(c.getTemperature()).isEqualTo(STEAMER_TEMPERATURE));
        assertThatRecipeCommandTermWrapping("vaporera", configuration).satisfies(c -> assertThat(c.getTemperature()).isEqualTo(STEAMER_TEMPERATURE));
        assertThatRecipeCommandTermWrapping("vapor", configuration).satisfies(c -> assertThat(c.getTemperature()).isEqualTo(STEAMER_TEMPERATURE));
    }

    @Test
    public void testAcceptGivenInstructionWithTimeInMinutesAndTemperatureAndSpeedThenTermAddedToInstructionAndContainsInformation() {
        assertThatRecipeCommandTermWrapping("2 min/120°C/vel \uE002", configuration)
                .satisfies(
                        c -> {
                            assertThat(c.getDuration()).isEqualTo(Duration.ofMinutes(2));
                            assertThat(c.getTemperature()).isEqualTo(120L);
                            assertThat(c.getSpeed()).isEqualTo(COMMAND_SPEED_SLOW_VALUE);
                        });
    }

    @Test
    public void testAcceptGivenInstructionWithTurboAndRangeThenTermsContainsTurbo() {
        assertThatRecipeCommandTermWrapping("Turbo/2 seg/1-2 veces", configuration)
                .satisfies(
                        c -> {
                            assertThat(c.getProgram()).isEqualTo(RecipeCommandProgram.TURBO);
                            assertThat(c.getDuration()).isEqualTo(Duration.ofSeconds(2));
                            assertThat(c.getRepetitionFrom()).isEqualTo(1);
                            assertThat(c.getRepetitionTo()).isEqualTo(2);
                        });
    }

    @Test
    public void testAcceptGivenInstructionWithTurboAndSingleThenTermsContainsTurbo() {
        assertThatRecipeCommandTermWrapping("Turbo/3 seg/2 veces", configuration)
                .satisfies(
                        c -> {
                            assertThat(c.getProgram()).isEqualTo(RecipeCommandProgram.TURBO);
                            assertThat(c.getDuration()).isEqualTo(Duration.ofSeconds(3));
                            assertThat(c.getRepetitionFrom()).isEqualTo(2);
                            assertThat(c.getRepetitionTo()).isNull();
                        });

        assertThatRecipeCommandTermWrapping("Turbo/2 seg/1 vez", configuration)
                .satisfies(
                        c -> {
                            assertThat(c.getProgram()).isEqualTo(RecipeCommandProgram.TURBO);
                            assertThat(c.getDuration()).isEqualTo(Duration.ofSeconds(2));
                            assertThat(c.getRepetitionFrom()).isEqualTo(1);
                            assertThat(c.getRepetitionTo()).isNull();
                        });
    }
}
