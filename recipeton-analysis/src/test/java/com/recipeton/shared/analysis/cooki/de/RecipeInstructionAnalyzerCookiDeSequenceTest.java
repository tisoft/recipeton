package com.recipeton.shared.analysis.cooki.de;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.configuration.RecipeAnalyzerConfiguration;
import com.recipeton.shared.analysis.configuration.cooki.RecipeAnalyzerCookiDeConfiguration;
import com.recipeton.shared.analysis.domain.RecipeIngredientAnalysis;
import com.recipeton.shared.analysis.domain.RecipeInstructionAnalysis;
import com.recipeton.shared.analysis.service.RecipeInstructionAnalyzer;
import com.recipeton.shared.analysis.test.support.RecipetonAnalyzerTestFixtureTrait;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.util.List;

import static java.util.List.of;
import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
public class RecipeInstructionAnalyzerCookiDeSequenceTest implements RecipetonAnalyzerTestFixtureTrait {

    private final RecipeAnalyzerConfiguration configuration = new RecipeAnalyzerCookiDeConfiguration();
    private final List<RecipeIngredientAnalysis> empty = createRecipeIngredientAnalyses(configuration, "");
    private final RecipeInstructionAnalyzer analyzer = createRecipeInstructionAnalyzer(configuration);

    @Test
    void testAnalyzeContainsSplittingSentenceConnectorThenSplits() {
        assertThat(analyzer.analyze(of("Pfeffer zugeben und <nobr>99 Sek./Stufe 99</nobr> zerkleinern und zu den Linsen geben"), empty).stream().map(RecipeInstructionAnalysis::getText))
                .containsExactly("Pfeffer zugeben", "[99 Sek./Stufe 99] zerkleinern", "Zu den Linsen geben");
    }

    @Test
    void testAnalyzeContainsSplittingSentenceConnectorThenSplitsForward() {
        assertThat(analyzer.analyze(of("Schiebe alles mit dem Spatel nach unten und dünste 5 Min."), empty).stream().map(RecipeInstructionAnalysis::getText)).containsExactly("Schiebe alles mit dem Spatel nach unten", "Dünste 5 Min");
    }
}
