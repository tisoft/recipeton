package com.recipeton.shared.analysis.cooki.fr;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.configuration.RecipeAnalyzerConfiguration;
import com.recipeton.shared.analysis.configuration.cooki.RecipeAnalyzerCookiFrConfiguration;
import com.recipeton.shared.analysis.test.support.RecipetonAnalyzerTestAssertTrait;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.util.stream.Stream;

import static com.recipeton.shared.analysis.cooki.fr.RecipeAnalyzerCookiFrTest.COOKI_FR_TOOLS_EXTERNAL;
import static com.recipeton.shared.analysis.domain.RecipeInstructionTag.*;
import static com.recipeton.shared.analysis.domain.RecipeToolComposedActionTag.*;
import static com.recipeton.shared.analysis.domain.RecipeToolTag.*;
import static java.util.List.of;
import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
public class RecipeInstructionAnalyzerCookiFrTagTest implements RecipetonAnalyzerTestAssertTrait {

    private final RecipeAnalyzerConfiguration configuration = new RecipeAnalyzerCookiFrConfiguration();

    @Test
    void testAnalyzeGivenCleanBowlInstructionThenAddsTags() {
        assertThatAnalyzeSingleResult(of("Nettoyer le bol de mixage à l’eau froide"), configuration).satisfies(ria -> assertThat(ria.getTags()).containsExactlyInAnyOrder(MIXING_BOWL_CLEAN, TOOL_MAIN, FOCUS_MAIN));
    }

    @Test
    void testAnalyzeGivenOvenInstructionThenAddsTag() {
        assertThatAnalyzeSingleResult(of("enfourner"), configuration).satisfies(ria1 -> assertThat(ria1.getTags()).containsExactlyInAnyOrder(OVEN_OPERATION));
        assertThatAnalyzeSingleResult(of("enfournez"), configuration).satisfies(ria -> assertThat(ria.getTags()).containsExactlyInAnyOrder(OVEN_OPERATION));
    }

    @Test
    void testAnalyzeGivenOvenPreheatInstructionThenAddsTag() {
        assertThatAnalyzeSingleResult(of("allumer le four"), configuration).satisfies(ria -> assertThat(ria.getTags()).containsExactlyInAnyOrder(OVEN_OPERATION, DO_PREHEAT, TOOL_EXTERNAL));
    }

    @Test
    void testAnalyzeGivenPlaceSteamerThenAddsTag() {
        assertThatAnalyzeSingleResult(of("Mettre le varoma en place"), configuration).satisfies(ria -> assertThat(ria.getTags()).containsExactlyInAnyOrder(STEAMER_PUT, TOOL_INTERNAL, DO_PLACE, FOCUS_ON_TOP));
    }

    @Test
    void testAnalyzeGivenPlaceUtilOnTopInstructionThenAddsTags() {
        assertThatAnalyzeSingleResult(of("Poser le panier cuisson sur le couvercle du bol"), configuration)
                .satisfies(ria -> assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_PLACE, FOCUS_ON_TOP, TOOL_MAIN, TOOL_INTERNAL, IS_ITEM_FROM_TO_TRANSFER));
    }

    @Test
    void testAnalyzeGivenReferenceExternalLocationInstructionThenAddsTags() {
        Stream.of(COOKI_FR_TOOLS_EXTERNAL)
                .flatMap(s -> Stream.of("dans une " + s, " dans une " + s, "dans le " + s, " dans le " + s, "sur une " + s, " sur une " + s))
                .forEach(s -> assertThatAnalyzeSingleResult(of(s), configuration).satisfies(ria -> assertThat(ria.getTags()).withFailMessage("No match for '" + s + "'").containsExactlyInAnyOrder(FOCUS_EXTERNAL, TOOL_EXTERNAL)));
    }

    @Test
    void testAnalyzeGivenReferenceExternalToolInstructionThenAddsTags() {
        Stream.of(COOKI_FR_TOOLS_EXTERNAL).forEach(s -> assertThatAnalyzeSingleResult(of(s), configuration).satisfies(ria -> assertThat(ria.getTags()).withFailMessage("No match for '" + s + "'").containsExactlyInAnyOrder(TOOL_EXTERNAL)));
    }

    @Test
    void testAnalyzeGivenRefrigerateInstructionThenAddsTags() {
        assertThatAnalyzeSingleResult(of("Réserver au frais"), configuration).satisfies(ria -> assertThat(ria.getTags()).containsExactlyInAnyOrder(REFRIGERATE, FOCUS_EXTERNAL));
    }

    @Test
    void testAnalyzeGivenRemoveWhiskInstructionThenAddsTags() {
        assertThatAnalyzeSingleResult(of("Ôter le fouet"), configuration).satisfies(ria -> assertThat(ria.getTags()).containsExactlyInAnyOrder(WHISK_REMOVE, TOOL_INTERNAL));
        assertThatAnalyzeSingleResult(of("en puis ôter le fouet"), configuration).satisfies(ria -> assertThat(ria.getTags()).containsExactlyInAnyOrder(WHISK_REMOVE, TOOL_INTERNAL));
    }

    @Test
    void testAnalyzeGivenReserveInstructionThenAddsTag() {
        assertThat(createRecipeInstructionAnalyzer(configuration).analyze(of("réservez au ..."), createRecipeIngredientAnalyses(configuration))).singleElement().satisfies(ria -> assertThat(ria.getTags()).doesNotContain(DO_SERVE));
    }

    @Test
    void testAnalyzeGivenServingInstructionThenAddsTag() {
        assertThatAnalyzeSingleResult(of("servez"), configuration).satisfies(ria -> assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_SERVE));
        assertThatAnalyzeSingleResult(of("servir"), configuration).satisfies(ria -> assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_SERVE));
        assertThatAnalyzeSingleResult(of("server"), configuration).satisfies(ria -> assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_SERVE));
    }
}
