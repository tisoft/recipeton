package com.recipeton.shared.analysis.cooki.en;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.configuration.RecipeAnalyzerConfiguration;
import com.recipeton.shared.analysis.configuration.cooki.RecipeAnalyzerCookiEnConfiguration;
import com.recipeton.shared.analysis.service.RecipeInstructionAnalyzer;
import com.recipeton.shared.analysis.test.support.RecipetonAnalyzerTestAssertTrait;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.util.stream.Stream;

import static com.recipeton.shared.analysis.cooki.en.RecipeToolAnalyzerCookiEnTest.COOKI_EN_TOOLS_EXTERNAL;
import static com.recipeton.shared.analysis.domain.RecipeInstructionTag.*;
import static com.recipeton.shared.analysis.domain.RecipeToolComposedActionTag.*;
import static com.recipeton.shared.analysis.domain.RecipeToolTag.*;
import static java.util.List.of;
import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
public class RecipeInstructionAnalyzerCookiEnTagTest implements RecipetonAnalyzerTestAssertTrait {

    private final RecipeAnalyzerConfiguration configuration = new RecipeAnalyzerCookiEnConfiguration();

    @Test
    void assertThatAnalyzeSingleResultContainsTagsExactly() {
        assertThatAnalyzeSingleResult(of("into varoma dish"), configuration).satisfies(ria -> assertThat(ria.getTags()).containsExactlyInAnyOrder(TOOL_INTERNAL));
    }

    @Test
    void testAnalyzeGivenReferenceExternalLocationInstructionThenAddsTags() {

        RecipeInstructionAnalyzer analyzer = createRecipeInstructionAnalyzer(configuration);

        Stream.of(COOKI_EN_TOOLS_EXTERNAL)
                .flatMap(s -> Stream.of("into " + s, " into " + s, "onto " + s, " onto " + s, "to " + s, " to " + s))
                .forEach(
                        s ->
                                assertThat(analyzer.analyze(of(s), createRecipeIngredientAnalyses(configuration)))
                                        .singleElement()
                                        .satisfies(ria -> assertThat(ria.getTags()).withFailMessage("No match for '" + s + "'").containsExactlyInAnyOrder(TOOL_EXTERNAL, FOCUS_EXTERNAL)));
    }

    @Test
    void testAnalyzeGivenReferenceExternalToolInstructionThenAddsTags() {
        Stream.of(COOKI_EN_TOOLS_EXTERNAL).forEach(s -> assertThatAnalyzeSingleResult(of(s), configuration).satisfies(ria -> assertThat(ria.getTags()).withFailMessage("No match for '" + s + "'").containsExactlyInAnyOrder(TOOL_EXTERNAL)));
    }

    @Test
    void testAnalyzeGivenReferenceMixingBowlInstructionThenDoesNotAddExternalAccessoryTag() {
        assertThatAnalyzeSingleResult(of("into mixing bowl"), configuration).satisfies(ria -> assertThat(ria.getTags()).containsExactlyInAnyOrder(FOCUS_MAIN, TOOL_MAIN));
    }

    @Test
    void testAnalyzeGivenRemoveSteamerDishInstructionThenAddsTag() {
        assertThatAnalyzeSingleResult(of("remove steamer dish"), configuration).satisfies(ria1 -> assertThat(ria1.getTags()).containsExactlyInAnyOrder(STEAMER_DISH_REMOVE, TOOL_INTERNAL));
        assertThatAnalyzeSingleResult(of("remove varoma dish"), configuration).satisfies(ria -> assertThat(ria.getTags()).containsExactlyInAnyOrder(STEAMER_DISH_REMOVE, TOOL_INTERNAL));
    }

    @Test
    void testAnalyzeGivenRemoveSteamerInstructionThenAddsTag() {
        assertThatAnalyzeSingleResult(of("remove steamer"), configuration).satisfies(ria1 -> assertThat(ria1.getTags()).containsExactlyInAnyOrder(STEAMER_REMOVE, TOOL_INTERNAL));
        assertThatAnalyzeSingleResult(of("remove varoma"), configuration).satisfies(ria -> assertThat(ria.getTags()).containsExactlyInAnyOrder(STEAMER_REMOVE, TOOL_INTERNAL));
    }

    @Test
    void testAnalyzeGivenRemoveSteamerTrayInstructionThenAddsTag() {
        assertThatAnalyzeSingleResult(of("remove steamer tray"), configuration).satisfies(ria1 -> assertThat(ria1.getTags()).containsExactlyInAnyOrder(STEAMER_TRAY_REMOVE, TOOL_INTERNAL));
        assertThatAnalyzeSingleResult(of("remove varoma tray"), configuration).satisfies(ria -> assertThat(ria.getTags()).containsExactlyInAnyOrder(STEAMER_TRAY_REMOVE, TOOL_INTERNAL));
    }

    @Test
    void testAnalyzeGivenReserveInstructionThenAddsServeTag() {
        assertThat(createRecipeInstructionAnalyzer(configuration).analyze(of("reserve in fridge"), createRecipeIngredientAnalyses(configuration))).singleElement().satisfies(ria -> assertThat(ria.getTags()).doesNotContain(DO_SERVE));
    }

    @Test
    void testAnalyzeGivenServingInstructionThenAddsTag() {
        assertThatAnalyzeSingleResult(of("serve at the moment"), configuration).satisfies(ria -> assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_SERVE));
    }
}
