package com.recipeton.shared.analysis.cooki.en;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.configuration.cooki.RecipeAnalyzerCookiEnConfiguration;
import com.recipeton.shared.analysis.service.RecipeCategoryAnalyzer;
import com.recipeton.shared.analysis.test.support.RecipetonAnalyzerTestFixtureTrait;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class RecipeCategoryAnalyzerCookiEnTest implements RecipetonAnalyzerTestFixtureTrait {

    private final RecipeCategoryAnalyzer analyzer = createRecipeCategoryAnalyzer(new RecipeAnalyzerCookiEnConfiguration());

    @Test
    public void testFormatCategoryUidGivenEndsWithPluralThenReturnsWithoutEnding() {
        assertThat(analyzer.formatCategoryUid("Childrens Birthday")).isEqualTo("childrenbirthday");
    }

    @Test
    public void testFormatCategoryUidGivenEndsWithPosessiveThenReturnsWithoutEnding() {
        assertThat(analyzer.formatCategoryUid("Children's Birthdays")).isEqualTo("childrenbirthday");
    }
}
