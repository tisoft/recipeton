package com.recipeton.shared.analysis.cooki.en;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.configuration.RecipeAnalyzerConfiguration;
import com.recipeton.shared.analysis.configuration.cooki.RecipeAnalyzerCookiEnConfiguration;
import com.recipeton.shared.analysis.service.RecipeIngredientAnalyzer;
import com.recipeton.shared.analysis.test.support.RecipetonAnalyzerTestFixtureTrait;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static com.recipeton.shared.analysis.configuration.cooki.RecipeAnalyzerCookiEnConfiguration.UNIT_PIECE_EN;
import static com.recipeton.shared.analysis.domain.MeasurementUnitDefinition.UNIT_GRAM;
import static com.recipeton.shared.analysis.domain.MeasurementUnitDefinition.UNIT_OZ;
import static org.assertj.core.api.Assertions.assertThat;

public class RecipeIngredientAnalyzerCookiEnTest implements RecipetonAnalyzerTestFixtureTrait {

    private final RecipeAnalyzerConfiguration configuration = new RecipeAnalyzerCookiEnConfiguration();
    private final RecipeIngredientAnalyzer analyzer = createRecipeIngredientAnalyzer(configuration);

    @Test
    void testAnalyzeGivenIngredientWithFromAndMagnitudeAndNameThenReturnsRecipeIngredient() {
        String text = "3 ½ oz buckwheat grain, white or brown (see Tip)";
        assertThat(analyzer.analyze(text))
                .isEqualTo(
                        createRecipeIngredientAnalysis(text, "buckwheat grain", configuration)
                                .setMagnitudeFromValue(new BigDecimal(3.5))
                                .setMagnitudeFrom("3½")
                                .setUnit(createMeasurementUnitAnalysis(UNIT_OZ))
                                .setPreparation("white or brown (see Tip)")
                                .setOptional(false));
    }

    @Test
    void testAnalyzeGivenIngredientWithMagnitudeDescriptionAndNomenclatureThenReturnsRecipeIngredient() {
        String text = "6 large eggs";
        assertThat(analyzer.analyze(text))
                .isEqualTo(
                        createRecipeIngredientAnalysis(text, "eggs", configuration)
                                .setMagnitudeFromValue(new BigDecimal(6))
                                .setMagnitudeFrom("6")
                                .setUnit(createMeasurementUnitAnalysis(UNIT_PIECE_EN, true))
                                .setPreparation("large")
                                .setOptional(false));
    }

    @Test
    void testAnalyzeGivenIngredientWithMagnitudenAndNameThenReturnsRecipeIngredient() {
        String text = "240 g mayonnaise";
        assertThat(analyzer.analyze(text))
                .isEqualTo(createRecipeIngredientAnalysis(text, "mayonnaise", configuration).setMagnitudeFromValue(new BigDecimal(240)).setMagnitudeFrom("240").setUnit(createMeasurementUnitAnalysis(UNIT_GRAM)).setOptional(false));
    }

    @Test
    void testAnalyzeGivenIngredientWithMagnitudenAndNameThenReturnsRecipeIngredient2() {
        String text = "120 g raw unsalted peanuts";
        assertThat(analyzer.analyze(text))
                .isEqualTo(
                        createRecipeIngredientAnalysis(text, "peanuts", configuration)
                                .setMagnitudeFromValue(new BigDecimal(120))
                                .setMagnitudeFrom("120")
                                .setUnit(createMeasurementUnitAnalysis(UNIT_GRAM))
                                .setPreparation("raw, unsalted")
                                .setOptional(false));
    }

    @Test
    void testAnalyzeGivenIngredientWithMagnitudenAndNameThenReturnsRecipeIngredient3() {
        String text = "10 g fresh parsley";
        assertThat(analyzer.analyze(text))
                .isEqualTo(
                        createRecipeIngredientAnalysis(text, "parsley", configuration)
                                .setMagnitudeFromValue(new BigDecimal(10))
                                .setMagnitudeFrom("10")
                                .setUnit(createMeasurementUnitAnalysis(UNIT_GRAM))
                                .setPreparation("fresh")
                                .setOptional(false));
    }

    @Test
    void testAnalyzeGivenIngredientWithMagnitudenAndNameThenReturnsRecipeIngredient4() {
        String text = "7 oz whole milk";
        assertThat(analyzer.analyze(text))
                .isEqualTo(
                        createRecipeIngredientAnalysis(text, "milk", configuration).setMagnitudeFromValue(new BigDecimal(7)).setMagnitudeFrom("7").setUnit(createMeasurementUnitAnalysis(UNIT_OZ)).setPreparation("whole").setOptional(false));
    }

    @Test
    void testAnalyzeGivenIngredientWithOfPreparationThenReturnsRecipeIngredient() {
        String text = "800 g milk of choice";
        assertThat(analyzer.analyze(text))
                .isEqualTo(
                        createRecipeIngredientAnalysis(text, "milk", configuration)
                                .setMagnitudeFromValue(new BigDecimal(800))
                                .setMagnitudeFrom("800")
                                .setUnit(createMeasurementUnitAnalysis(UNIT_GRAM))
                                .setPreparation("of choice")
                                .setOptional(false));
    }

    @Test
    void testAnalyzeGivenIngredientWithPreparationsBeforeThenReturnsRecipeIngredient() {
        String text = "400 g pouring (whipping) cream";
        assertThat(analyzer.analyze(text))
                .isEqualTo(
                        createRecipeIngredientAnalysis(text, "cream", configuration)
                                .setMagnitudeFromValue(new BigDecimal(400))
                                .setMagnitudeFrom("400")
                                .setUnit(createMeasurementUnitAnalysis(UNIT_GRAM))
                                .setPreparation("pouring, (whipping)")
                                .setOptional(false));
    }
}
