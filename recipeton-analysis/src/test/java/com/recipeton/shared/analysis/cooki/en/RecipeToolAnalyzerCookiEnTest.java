package com.recipeton.shared.analysis.cooki.en;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.configuration.RecipeAnalyzerConfiguration;
import com.recipeton.shared.analysis.configuration.cooki.RecipeAnalyzerCookiEnConfiguration;
import com.recipeton.shared.analysis.service.RecipeToolAnalyzer;
import com.recipeton.shared.analysis.test.support.RecipetonAnalyzerTestAssertTrait;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
public class RecipeToolAnalyzerCookiEnTest implements RecipetonAnalyzerTestAssertTrait {

    public static final String[] COOKI_EN_TOOLS_EXTERNAL = {
        "casserole",
        "casseroles",
        "colander",
        "colanders",
        "container",
        "containers",
        "dish",
        "dishes",
        "jar",
        "jars",
        "jug",
        "jugs",
        "plate",
        "plates",
        "plater",
        "platers",
        "jug",
        "jugs",
        "ramekin",
        "ramekins",
        "sheet",
        "sheets",
        "tin",
        "tray",
        "trays",
        "oven tray",
        "oven sheet",
        "grilling plate"
    };

    private final RecipeAnalyzerConfiguration configuration = new RecipeAnalyzerCookiEnConfiguration();

    private final RecipeToolAnalyzer analyzer = createRecipeToolAnalyzer(configuration);

    @Test
    void testAnalyzeGivenReferenceExternalLocationInstructionThenAddsTags() {
        Stream.of(COOKI_EN_TOOLS_EXTERNAL)
                .flatMap(s -> Stream.of("into " + s, " into " + s, "onto " + s, " onto " + s, "to " + s, " to " + s))
                .forEach(s -> assertThat(analyzer.isToolPresent(s)).withFailMessage("No match for '" + s + "'").isTrue());
    }
}
