package com.recipeton.shared.analysis.cooki.fr;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.configuration.RecipeAnalyzerConfiguration;
import com.recipeton.shared.analysis.configuration.cooki.RecipeAnalyzerCookiFrConfiguration;
import com.recipeton.shared.analysis.domain.RecipeIngredientAnalysis;
import com.recipeton.shared.analysis.service.RecipeIngredientAnalyzer;
import com.recipeton.shared.analysis.test.support.RecipetonAnalyzerTestFixtureTrait;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static com.recipeton.shared.analysis.configuration.cooki.RecipeAnalyzerCookiFrConfiguration.UNIT_PIECE_FR;
import static com.recipeton.shared.analysis.domain.MeasurementUnitDefinition.UNIT_GRAM;
import static org.assertj.core.api.Assertions.assertThat;

// WIP!
public class RecipeIngredientAnalyzerCookiFrTest implements RecipetonAnalyzerTestFixtureTrait {

    private final RecipeAnalyzerConfiguration configuration = new RecipeAnalyzerCookiFrConfiguration();
    private final RecipeIngredientAnalyzer analyzer = createRecipeIngredientAnalyzer(configuration);

    @Test
    void testAnalyzeGivenIngredientDuSelThenReturnsRecipeIngredient() {
        String text = "du sel";
        assertThat(analyzer.analyze(text)).isEqualTo(createRecipeIngredientAnalysis(text, "sel", configuration).setUnit(createRecipeMeasurementUnitAnalysis("du", configuration)).setOptional(false));
    }

    @Test
    void testAnalyzeGivenIngredientThenReturnsRecipeIngredient() {
        String text = "600 g de mûres";
        RecipeIngredientAnalysis analyze = analyzer.analyze(text);
        assertThat(analyze)
                .isEqualTo(createRecipeIngredientAnalysis(text, "mûres", configuration).setMagnitudeFromValue(new BigDecimal(600)).setMagnitudeFrom("600").setUnit(createRecipeMeasurementUnitAnalysis("g", configuration)).setOptional(false));
    }

    @Test
    void testAnalyzeGivenIngredientWithCSoupeThenReturnsRecipeIngredient() {
        String text = "1 c. à soupe d'huile d'olive";
        RecipeIngredientAnalysis analyze = analyzer.analyze(text);
        assertThat(analyze)
                .isEqualTo(
                        createRecipeIngredientAnalysis(text, "huile de olive", configuration)
                                .setMagnitudeFromValue(new BigDecimal(1))
                                .setMagnitudeFrom("1")
                                .setUnit(createRecipeMeasurementUnitAnalysis("CàS", configuration))
                                .setOptional(false));
    }

    @Test
    void testAnalyzeGivenIngredientWithMagnitudAndNameThenReturnsRecipeIngredient() {
        String text = "10 g d'eau";
        assertThat(analyzer.analyze(text))
                .isEqualTo(createRecipeIngredientAnalysis(text, "eau", configuration).setMagnitudeFromValue(new BigDecimal(10)).setMagnitudeFrom("10").setUnit(createMeasurementUnitAnalysis(UNIT_GRAM)).setOptional(false));
    }

    @Test
    void testAnalyzeGivenIngredientWithMinusThenReturnsRecipeIngredient() {
        String text = "½ branche de céleri-rave, coupée en morceaux";
        RecipeIngredientAnalysis analyze = analyzer.analyze(text);
        RecipeIngredientAnalysis expected =
                createRecipeIngredientAnalysis(text, "céleri-rave", configuration)
                        .setMagnitudeFromValue(new BigDecimal(0.5))
                        .setMagnitudeFrom("½")
                        .setUnit(createRecipeMeasurementUnitAnalysis("branche", configuration))
                        .setPreparation("coupée en morceaux")
                        .setOptional(false);
        assertThat(analyze).isEqualTo(expected);
    }

    @Test
    void testAnalyzeGivenIngredientWithSpecialCharactersThenReturnsRecipeIngredient() {
        String text = "1 jaune d'œuf battu";
        RecipeIngredientAnalysis analyze = analyzer.analyze(text);
        RecipeIngredientAnalysis expected =
                createRecipeIngredientAnalysis(text, "jaune de oeuf", configuration)
                        .setMagnitudeFromValue(new BigDecimal(1))
                        .setMagnitudeFrom("1")
                        .setUnit(createMeasurementUnitAnalysis(UNIT_PIECE_FR, true))
                        .setPreparation("battu")
                        .setOptional(false);
        assertThat(analyze).isEqualTo(expected);
    }

    @Test
    void testAnalyzeGivenIngredientWithUnitAndNotAmountThenReturnsRecipeIngredient() {
        String text = "pincée de sel";
        assertThat(analyzer.analyze(text)).isEqualTo(createRecipeIngredientAnalysis(text, "sel", configuration).setUnit(createRecipeMeasurementUnitAnalysis("pincée", configuration)).setOptional(false));
    }
}
