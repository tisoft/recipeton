package com.recipeton.shared.analysis.cooki.fr;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.configuration.RecipeAnalyzerConfiguration;
import com.recipeton.shared.analysis.configuration.cooki.RecipeAnalyzerCookiFrConfiguration;
import com.recipeton.shared.analysis.service.RecipeInstructionAnalyzer;
import com.recipeton.shared.analysis.test.support.RecipetonAnalyzerTestAssertTrait;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import static java.util.List.of;
import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
public class RecipeInstructionAnalyzerCookiFrIngredientTest implements RecipetonAnalyzerTestAssertTrait {

    private final RecipeAnalyzerConfiguration configuration = new RecipeAnalyzerCookiFrConfiguration();
    private final RecipeInstructionAnalyzer analyzer = createRecipeInstructionAnalyzer(configuration);

    @Test
    void testAnalyzeGivenIngredientWithApostropheThenIngredienAdded() {
        assertThat(analyzer.analyze(of("Ajouter l'extrait d'amande amère dans le bol"), createRecipeIngredientAnalyses(configuration, "quelques gouttes d'extrait naturel d'amande amère")))
                .singleElement()
                .satisfies(
                        ria ->
                                assertThat(ria.getRecipeIngredientTerms())
                                        .anySatisfy(
                                                t -> {
                                                    assertThat(t.isConclusive()).isTrue();
                                                    assertThat(t.getIngredient().getTextSource()).isEqualTo("quelques gouttes d'extrait naturel d'amande amère");
                                                }));
    }

    @Test
    void testAnalyzeGivenInstructionWithIngredientAndIngredientHasManyLessWordsThenIngredientIsDetected() {
        assertThat(analyzer.analyze(of("Mettre le caraf d'eau et œuf dans le bol"), createRecipeIngredientAnalyses(configuration, "100 g d'eau frais")))
                .singleElement()
                .satisfies(
                        ria ->
                                assertThat(ria.getRecipeIngredientTerms())
                                        .anySatisfy(t -> assertThat(t.getIngredient().getIngredientNotation()).isEqualTo("eau"))
                                        .anySatisfy(
                                                t -> {
                                                    assertThat(t.getIngredient().getIngredientNotation()).isEqualTo("oeuf");
                                                    assertThat(t.isDetachedMatch()).isTrue();
                                                }));
    }

    @Test
    void testAnalyzeGivenInstructionWithIngredientWithMinusSymbolAndIngredientWithNoMinusThenIngredientIsDetected() {
        assertThat(analyzer.analyze(of("Mettre le céleri-rave dans le bol"), createRecipeIngredientAnalyses(configuration, "½ branche de céleri rave, coupée en morceaux")))
                .singleElement()
                .satisfies(
                        ria ->
                                assertThat(ria.getRecipeIngredientTerms())
                                        .anySatisfy(
                                                t -> {
                                                    assertThat(t.isConclusive()).isTrue();
                                                    assertThat(t.getIngredient().getIngredientNotation()).isEqualTo("céleri rave");
                                                }));
    }

    @Test
    void testAnalyzeGivenPlaceUtensilInstructionAndIngredientThenIngredientNotAdded() {
        assertThat(analyzer.analyze(of("Mettre en place le Varoma"), createRecipeIngredientAnalyses(configuration, "glace vaunille"))).singleElement().satisfies(ria -> assertThatNoRecipeIngredientTermsAreAddedByNormalAnalysis(ria));
    }
}
