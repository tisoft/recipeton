package com.recipeton.shared.analysis.cooki.fr;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.configuration.RecipeAnalyzerConfiguration;
import com.recipeton.shared.analysis.configuration.cooki.RecipeAnalyzerCookiFrConfiguration;
import com.recipeton.shared.analysis.domain.RecipeIngredientAnalysis;
import com.recipeton.shared.analysis.domain.RecipeIngredientTerm;
import com.recipeton.shared.analysis.domain.RecipeInstructionAnalysis;
import com.recipeton.shared.analysis.service.RecipeInstructionAnalyzer;
import com.recipeton.shared.analysis.test.support.RecipetonAnalyzerTestAssertTrait;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Stream;

import static com.recipeton.shared.analysis.domain.RecipeInstructionTag.*;
import static com.recipeton.shared.analysis.domain.RecipeToolComposedActionTag.MEASURING_CUP_NOTUSE;
import static com.recipeton.shared.analysis.domain.RecipeToolComposedActionTag.TRANSFER_FROM_BOWL;
import static com.recipeton.shared.analysis.domain.RecipeToolTag.*;
import static java.util.List.of;
import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
public class RecipeInstructionAnalyzerCookiFrSequenceTest implements RecipetonAnalyzerTestAssertTrait {

    private final RecipeAnalyzerConfiguration configuration = new RecipeAnalyzerCookiFrConfiguration();
    private final List<RecipeIngredientAnalysis> empty = createRecipeIngredientAnalyses(configuration);
    private final RecipeInstructionAnalyzer analyzer = createRecipeInstructionAnalyzer(configuration);

    @Test
    void testAnalyzeCommandThenSplits() {
        assertThat(analyzer.analyze(of("Mettre les blancs et fouetter <nobr>5 min/vitesse 3.5</nobr>"), empty).stream().map(RecipeInstructionAnalysis::getText)).containsExactly("Mettre les blancs", "Fouetter [5 min/vitesse 3.5]");
    }

    @Test
    void testAnalyzeCommandWithoutVerbThenSplits() {
        assertThat(analyzer.analyze(of("Xxx, puis <nobr>5 min/vitesse 3.5</nobr>"), empty).stream().map(RecipeInstructionAnalysis::getText)).containsExactly("Xxx", "Régler [5 min/vitesse 3.5]");
    }

    @Test
    void testAnalyzeContainsAbbreviatureWithDotThenReturnsStringNotSplittingInAbbreviatureDot() {
        assertThat(analyzer.analyze(of("A approx. b"), empty).stream().map(RecipeInstructionAnalysis::getText)).containsExactly("A approx. b");
        assertThat(analyzer.analyze(of("A approx. b. Cc"), empty).stream().map(RecipeInstructionAnalysis::getText)).containsExactly("A approx. b", "Cc");
    }

    @Test
    void testAnalyzeGivenCommandAndAddOperationThenAddOperationIsFocusMain() {
        assertThat(analyzer.analyze(of("Rissoler [5 min/120°C/vitesse 5]. Ajouter le cube de bouillon"), createRecipeIngredientAnalyses(configuration, "cube de bouillon")))
                .hasSize(2)
                .satisfies(
                        l -> {
                            assertThat(l).element(0).satisfies(ria -> assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_COMMAND_MAIN, FOCUS_MAIN));
                            assertThat(l).element(1).satisfies(ria -> assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_ADD, IS_INGREDIENT_ADDITION, FOCUS_MAIN));
                        });
    }

    @Test
    void testAnalyzeGivenInstructionWithAddAndToMainCompartmentAndNoIngredientThenTagCreatedIngredient() {
        assertThat(analyzer.analyze(of("Mettre le lait dans le bol et cuire <nobr>11 min/90°C/vitesse 2</nobr>."), createRecipeIngredientAnalyses(configuration, "lait")))
                .hasSize(2)
                .satisfies(
                        l -> {
                            assertThat(l)
                                    .element(0)
                                    .satisfies(
                                            ria -> {
                                                assertThat(ria.getText()).isEqualTo("Mettre le lait dans le bol");
                                                assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_PLACE, FOCUS_MAIN, TOOL_MAIN, IS_INGREDIENT_ADDITION, IS_ITEM_FROM_TO_TRANSFER);
                                            });
                            assertThat(l)
                                    .element(1)
                                    .satisfies(
                                            ria -> {
                                                assertThat(ria.getText()).isEqualTo("Cuire [11 min/90°C/vitesse 2]");
                                                assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_COMMAND_MAIN, DO_COMMAND_MAIN_PREPARATION_DEFAULT, FOCUS_MAIN);
                                            });
                        });
    }

    @Test
    void testAnalyzeGivenInstructionsCommandWithParallelNonActionExplanationThenReturnsInstruction() {

        Stream.of("Cuire <nobr>8 min/100°C/vitesse 3</nobr>. Pendant ce temps, tailler", "Cuire <nobr>8 min/100°C/vitesse 3</nobr>, pendant ce temps, tailler")
                .forEach(
                        s ->
                                assertThat(analyzer.analyze(of(s), createRecipeIngredientAnalyses(configuration, "")))
                                        .hasSize(2)
                                        .satisfies(
                                                l -> {
                                                    assertThat(l)
                                                            .element(0)
                                                            .satisfies(
                                                                    ria -> {
                                                                        assertThat(ria.getText()).isEqualTo("Cuire [8 min/100°C/vitesse 3]");
                                                                        assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_COMMAND_MAIN, FOCUS_MAIN);
                                                                    });
                                                    assertThat(l)
                                                            .element(1)
                                                            .satisfies(
                                                                    ria -> {
                                                                        assertThat(ria.getText()).isEqualTo("Pendant ce temps, tailler");
                                                                        assertThat(ria.getTags()).containsExactlyInAnyOrder(SEQUENCE_PARALLEL, QUALIFIES_PREVIOUS);
                                                                    });
                                                }));
    }

    @Test
    void testAnalyzeGivenInstructionsAddToMainCompartmentAndPourOnExternalAndReferenceIngredientThenIngredientIsNotAdded() {
        assertThat(analyzer.analyze(of("Mettre le lait dans le bol. Transvaser dans un carafe"), createRecipeIngredientAnalyses(configuration, "lait")))
                .hasSize(2)
                .satisfies(
                        l -> {
                            assertThat(l).element(0).satisfies(ria -> assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_PLACE, FOCUS_MAIN, TOOL_MAIN, IS_INGREDIENT_ADDITION, IS_ITEM_FROM_TO_TRANSFER));
                            assertThat(l).element(1).satisfies(ria -> assertThat(ria.getTags()).containsExactlyInAnyOrder(TRANSFER_FROM_BOWL, TOOL_EXTERNAL, FOCUS_EXTERNAL, IS_ITEM_FROM_TO_TRANSFER));
                        });
    }

    @Test
    void testAnalyzeGivenMultipleAddInstructionWithSameIngredientAndContextualUnitThenIngredientAddedToAllInstructions() {
        List<RecipeInstructionAnalysis> recipeInstructionAnalyses = analyzer.analyze(of("Mettre 1 banaan dans le bol", "Ajouter les 2 banaan restants"), of(createRecipeIngredientAnalysis("3 banaan", configuration)));

        assertThat(recipeInstructionAnalyses)
                .hasSize(2)
                .satisfies(
                        l -> {
                            assertThat(l)
                                    .element(0)
                                    .satisfies(
                                            ria -> {
                                                assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_PLACE, FOCUS_MAIN, TOOL_MAIN, IS_INGREDIENT_ADDITION, IS_ITEM_FROM_TO_TRANSFER);
                                                List<RecipeIngredientTerm> recipeIngredientTerms = ria.getRecipeIngredientTerms();
                                                assertThat(recipeIngredientTerms)
                                                        .singleElement()
                                                        .satisfies(
                                                                t -> {
                                                                    assertThat(t.getIngredient().getIngredientNotation()).isEqualTo("banaan");
                                                                    assertThat(t.getIngredient().getMagnitudeFromValue()).isEqualTo(new BigDecimal(3));
                                                                    assertThat(t.getMagnitudeFromValue()).isEqualTo(new BigDecimal(1));
                                                                    assertThat(t.isPartialUse()).isTrue();
                                                                    assertThat(t.isConclusive()).isTrue();
                                                                });
                                            });
                            assertThat(l)
                                    .element(1)
                                    .satisfies(
                                            ria -> {
                                                assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_ADD, FOCUS_MAIN, IS_INGREDIENT_ADDITION);
                                                List<RecipeIngredientTerm> recipeIngredientTerms = ria.getRecipeIngredientTerms();
                                                assertThat(recipeIngredientTerms)
                                                        .singleElement()
                                                        .satisfies(
                                                                t -> {
                                                                    assertThat(t.getIngredient().getIngredientNotation()).isEqualTo("banaan");
                                                                    assertThat(t.getIngredient().getMagnitudeFromValue()).isEqualTo(new BigDecimal(3));
                                                                    assertThat(t.getMagnitudeFromValue()).isEqualTo(new BigDecimal(2));
                                                                    assertThat(t.isPartialUse()).isTrue();
                                                                    assertThat(t.isConclusive()).isTrue();
                                                                });
                                            });
                        });
    }

    @Test
    void testAnalyzeGivenPlaceInstructionWithUtensilInstructionIntendedForPreviousStepThenReorganizesAndSplits() {
        assertThat(analyzer.analyze(of("Rissoler <nobr>5 min/120°C/vitesse 5</nobr>, sans le gobelet doseur"), createRecipeIngredientAnalyses(configuration)))
                .hasSize(2)
                .satisfies(
                        l -> {
                            assertThat(l).element(0).satisfies(ria -> assertThat(ria.getTags()).containsExactlyInAnyOrder(TOOL_INTERNAL, MEASURING_CUP_NOTUSE));
                            assertThat(l).element(1).satisfies(ria -> assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_COMMAND_MAIN, FOCUS_MAIN));
                        });
    }

    @Test
    void testAnalyzeGivenMultipleAddInstructionWithSameIngredientAndUnitThenIngredientAddedToAllInstructions() {
        List<RecipeInstructionAnalysis> recipeInstructionAnalyses =
                analyzer.analyze(of("Mettre 125 g de crème liquide dans le bol", "Ajouter les 100 g de crème liquide restants"), of(createRecipeIngredientAnalysis("225 g de crème liquide 30-40% m.g.", configuration)));

        assertThat(recipeInstructionAnalyses)
                .hasSize(2)
                .satisfies(
                        l -> {
                            assertThat(l)
                                    .element(0)
                                    .satisfies(
                                            ria -> {
                                                assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_PLACE, FOCUS_MAIN, TOOL_MAIN, IS_INGREDIENT_ADDITION, IS_ITEM_FROM_TO_TRANSFER);
                                                List<RecipeIngredientTerm> recipeIngredientTerms = ria.getRecipeIngredientTerms();
                                                assertThat(recipeIngredientTerms)
                                                        .singleElement()
                                                        .satisfies(
                                                                t -> {
                                                                    assertThat(t.getIngredient().getIngredientNotation()).isEqualTo("crème");
                                                                    assertThat(t.getIngredient().getMagnitudeFromValue()).isEqualTo(new BigDecimal(225));
                                                                    assertThat(t.getMagnitudeFromValue()).isEqualTo(new BigDecimal(125));
                                                                    assertThat(t.isPartialUse()).isTrue();
                                                                    assertThat(t.isConclusive()).isTrue();
                                                                });
                                            });
                            assertThat(l)
                                    .element(1)
                                    .satisfies(
                                            ria -> {
                                                assertThat(ria.getTags()).containsExactlyInAnyOrder(DO_ADD, FOCUS_MAIN, IS_INGREDIENT_ADDITION);
                                                List<RecipeIngredientTerm> recipeIngredientTerms = ria.getRecipeIngredientTerms();
                                                assertThat(recipeIngredientTerms)
                                                        .singleElement()
                                                        .satisfies(
                                                                t -> {
                                                                    assertThat(t.getIngredient().getIngredientNotation()).isEqualTo("crème");
                                                                    assertThat(t.getIngredient().getMagnitudeFromValue()).isEqualTo(new BigDecimal(225));
                                                                    assertThat(t.getMagnitudeFromValue()).isEqualTo(new BigDecimal(100));
                                                                    assertThat(t.isPartialUse()).isTrue();
                                                                    assertThat(t.isConclusive()).isTrue();
                                                                });
                                            });
                        });
    }

    @Test
    void testAnalyzeGivenTwoIngredientsWithSameNameAndDifferentAmountThenIngredientsAreDetectedInMostSimilarInstruction() {
        assertThat(
                        analyzer.analyze(
                                of("Mettre les 200 g de bâtonnets aux noisettes dans le bol de mixage", "Enfoncer les 120 g de bâtonnets aux noisettes coupés en deux tout le long du bord"),
                                createRecipeIngredientAnalyses(configuration, "200 g de bâtons aux noisettes", "120 g de bâtons aux noisettes, coupés en deux dans la largeur, pour le cercle")))
                .hasSize(2)
                .satisfies(
                        l -> {
                            assertThat(l)
                                    .element(0)
                                    .satisfies(
                                            ria -> {
                                                List<RecipeIngredientTerm> recipeIngredientTerms = ria.getRecipeIngredientTerms();
                                                assertThat(recipeIngredientTerms)
                                                        .anySatisfy(
                                                                t -> {
                                                                    assertThat(t.getIngredient().getMagnitudeFrom()).isEqualTo("200");
                                                                    assertThat(t.isConclusive()).isTrue();
                                                                })
                                                        .noneSatisfy(t -> assertThat(t.isContextualMatch()).isTrue())
                                                        .noneSatisfy(t -> assertThat(t.getIngredient().getMagnitudeFrom()).isEqualTo("120"));
                                            });
                            assertThat(l)
                                    .element(1)
                                    .satisfies(
                                            ria -> {
                                                List<RecipeIngredientTerm> recipeIngredientTerms = ria.getRecipeIngredientTerms();
                                                assertThat(recipeIngredientTerms)
                                                        .anySatisfy(
                                                                t -> {
                                                                    assertThat(t.getIngredient().getMagnitudeFrom()).isEqualTo("120");
                                                                    assertThat(t.isConclusive()).isFalse();
                                                                })
                                                        .noneSatisfy(t -> assertThat(t.isContextualMatch()).isTrue())
                                                        .noneSatisfy(t -> assertThat(t.getIngredient().getMagnitudeFrom()).isEqualTo("200"));
                                            });
                        });
    }
}
