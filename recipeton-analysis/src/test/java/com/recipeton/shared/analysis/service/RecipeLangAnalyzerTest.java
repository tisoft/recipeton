package com.recipeton.shared.analysis.service;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.test.support.RecipetonAnalyzerTestFixtureTrait;
import org.junit.jupiter.api.Test;

import static com.recipeton.shared.analysis.configuration.RecipeLangAnalyzerConfiguration.TOKEN_LENGTH_MIN;
import static org.assertj.core.api.Assertions.assertThat;

public class RecipeLangAnalyzerTest implements RecipetonAnalyzerTestFixtureTrait {

    private final RecipeLangAnalyzer analyzer = createRecipeLangAnalyzer(createRecipeAnalyzerConfiguration());

    @Test
    void testFormatCategoryUuidGivenNameWithCapitalThenReturnsNameInLowercase() {
        assertThat(analyzer.toUid("BLA")).isEqualTo("bla");
    }

    @Test
    void testFormatCategoryUuidGivenNameWithParenthesesThenReturnsNameWithoutParentheses() {
        assertThat(analyzer.toUid("some(thing)")).isEqualTo("something");
    }

    @Test
    void testFormatCategoryUuidGivenNameWithSpacesThenReturnsNameWithoutSpaces() {
        assertThat(analyzer.toUid("some thing")).isEqualTo("thingsome");
    }

    @Test
    void testFormatCategoryUuidGivenNameWithSpecialCharactersThenReturnsNameWithoutSpecialCharacters() {
        assertThat(analyzer.toUid("navidadyañonuevo")).isEqualTo("navidadyanonuevo");
        assertThat(analyzer.toUid("fácil")).isEqualTo("facil");
    }

    @Test
    void testFormatCategoryUuidGivenNameWithSymbolsThenReturnsNameWithoutParentheses() {
        assertThat(analyzer.toUid("some+thing")).isEqualTo("something");
        assertThat(analyzer.toUid("some_thing")).isEqualTo("something");
        assertThat(analyzer.toUid("some-thing")).isEqualTo("something");
    }

    @Test
    void testGetTokenizationGivenMultipleWordAndParenthesesCharactersThanMinThenReturnsWordsLongerThanMin() {
        assertThat(analyzer.getTokenization("AAA (C) BB", 2))
                .satisfies(
                        t -> {
                            assertThat(t.getText()).isEqualTo("AAA (C) BB");
                            assertThat(t.getPositions()).containsExactly(createTokenizationPosition(0, 3), createTokenizationPosition(8, 2));
                            assertThat(t.getTokens()).containsExactly("aaa", "bb");
                        });
    }

    @Test
    void testGetTokenizationGivenMultipleWordAndShorterThatnMinThenReturnsWordsLongerThanMin() {
        assertThat(analyzer.getTokenization("AAA C BB", 2))
                .satisfies(
                        t -> {
                            assertThat(t.getText()).isEqualTo("AAA C BB");
                            assertThat(t.getPositions()).containsExactly(createTokenizationPosition(0, 3), createTokenizationPosition(6, 2));
                            assertThat(t.getTokens()).containsExactly("aaa", "bb");
                        });
    }

    @Test
    void testGetTokenizationGivenMultipleWordAndSymbolCharactersThanMinThenReturnsWordsLongerThanMin() {
        assertThat(analyzer.getTokenization("AAA, BB", 2))
                .satisfies(
                        t -> {
                            assertThat(t.getText()).isEqualTo("AAA, BB");
                            assertThat(t.getPositions()).containsExactly(createTokenizationPosition(0, 4), createTokenizationPosition(5, 2));
                            assertThat(t.getTokens()).containsExactly("aaa", "bb");
                        });
    }

    @Test
    void testGetTokenizationGivenMultipleWordThenReturnsSingleWordWithTokenAndPositionEqualsZero() {
        assertThat(analyzer.getTokenization("AAA BBB", TOKEN_LENGTH_MIN))
                .satisfies(
                        t -> {
                            assertThat(t.getText()).isEqualTo("AAA BBB");
                            assertThat(t.getPositions()).containsExactly(createTokenizationPosition(0, 3), createTokenizationPosition(4, 3));
                            assertThat(t.getTokens()).containsExactly("aaa", "bbb");
                        });
    }

    @Test
    void testGetTokenizationGivenSingleWordThenReturnsSingleWordWithTokenAndPositionEqualsZero() {
        assertThat(analyzer.getTokenization("AbC", TOKEN_LENGTH_MIN))
                .satisfies(
                        t -> {
                            assertThat(t.getText()).isEqualTo("AbC");
                            assertThat(t.getPositions()).containsExactly(createTokenizationPosition(0, 3));
                            assertThat(t.getTokens()).containsExactly("abc");
                        });
    }

    @Test
    void testGetTokenizationGivenWordWithSymbolCharactersThanMinThenReturnsWordAndPosition() {
        assertThat(analyzer.getTokenization("a+-_()b", 1))
                .satisfies(
                        t -> {
                            assertThat(t.getText()).isEqualTo("a+-_()b");
                            assertThat(t.getPositions()).containsExactly(createTokenizationPosition(0, 7));
                            assertThat(t.getTokens()).containsExactly("ab");
                        });
    }
}
