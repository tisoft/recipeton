package com.recipeton.shared.analysis.cooki.es;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.configuration.cooki.RecipeAnalyzerCookiEsConfiguration;
import com.recipeton.shared.analysis.domain.RecipeToolComposedActionTag;
import com.recipeton.shared.analysis.test.support.RecipetonAnalyzerTestAssertTrait;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import static java.util.List.of;
import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
public class RecipeInstructionAnalyzerCookiEsUtensilActionTest implements RecipetonAnalyzerTestAssertTrait {

    private final RecipeAnalyzerCookiEsConfiguration configuration = new RecipeAnalyzerCookiEsConfiguration();

    @Test
    void testAnalyzeGivenInstructionWithButterflyActionThenUtensilActionIsDetected() {
        assertThatAnalyzeSingleResultRecipeToolComposedActionTermAndTagEquals("Coloque la mariposa", RecipeToolComposedActionTag.WHISK_PUT, configuration);
        assertThatAnalyzeSingleResultRecipeToolComposedActionTermAndTagEquals("Coloque la mariposa en las cuchillas", RecipeToolComposedActionTag.WHISK_PUT, configuration);

        assertThatAnalyzeSingleResultRecipeToolComposedActionTermAndTagEquals("Retire la mariposa", RecipeToolComposedActionTag.WHISK_REMOVE, configuration);
        assertThatAnalyzeSingleResultRecipeToolComposedActionTermAndTagEquals("Quite la mariposa", RecipeToolComposedActionTag.WHISK_REMOVE, configuration);
    }

    @Test
    void testAnalyzeGivenInstructionWithEmptyMixingBowlActionThenUtensilActionIsDetected() {
        assertThatAnalyzeSingleResultRecipeToolComposedActionTermAndTagEquals("Vierta el helado a un tazón", RecipeToolComposedActionTag.TRANSFER_FROM_BOWL, configuration);
        assertThatAnalyzeSingleResultRecipeToolComposedActionTermAndTagEquals("Vierta el helado en un tazón", RecipeToolComposedActionTag.TRANSFER_FROM_BOWL, configuration);
        assertThatAnalyzeSingleResultRecipeToolComposedActionTermAndTagEquals("Retire a un tazón", RecipeToolComposedActionTag.TRANSFER_FROM_BOWL, configuration);
        assertThatAnalyzeSingleResultRecipeToolComposedActionTermAndTagEquals("Retire a un bol", RecipeToolComposedActionTag.TRANSFER_FROM_BOWL, configuration);
        assertThatAnalyzeSingleResultRecipeToolComposedActionTermAndTagEquals("Retire del vaso", RecipeToolComposedActionTag.TRANSFER_FROM_BOWL, configuration);
        assertThatAnalyzeSingleResultRecipeToolComposedActionTermAndTagEquals("Saque del vaso", RecipeToolComposedActionTag.TRANSFER_FROM_BOWL, configuration);
        assertThatAnalyzeSingleResultRecipeToolComposedActionTermAndTagEquals("Saque del vaso", RecipeToolComposedActionTag.TRANSFER_FROM_BOWL, configuration);

        assertThatAnalyzeSingleResultRecipeToolComposedActionTermAndTagEquals("Transfiera a un tazón", RecipeToolComposedActionTag.TRANSFER_FROM_BOWL, configuration);
    }

    @Test
    void testAnalyzeGivenInstructionWithOvenThenUtensilActionIsDetected() {
        assertThatAnalyzeSingleResultRecipeToolComposedActionTermAndTagEquals("precaliente el horno", RecipeToolComposedActionTag.OVEN_OPERATION, configuration);
        assertThatAnalyzeSingleResultRecipeToolComposedActionTermAndTagEquals("precaliente el horno a 180 grados", RecipeToolComposedActionTag.OVEN_OPERATION, configuration);
    }

    @Test
    void testAnalyzeGivenInstructionWithSpatulaMixActionThenUtensilActionIsDetected() {
        assertThatAnalyzeSingleResultRecipeToolComposedActionTermAndTagEquals("Mezcle con la espátula", RecipeToolComposedActionTag.SPATULA_MIX, configuration);
        assertThatAnalyzeSingleResultRecipeToolComposedActionTermAndTagEquals("Mezcle la cosa con la espátula", RecipeToolComposedActionTag.SPATULA_MIX, configuration);
        assertThatAnalyzeSingleResultRecipeToolComposedActionTermAndTagEquals("Termine de mezclar con la espátula", RecipeToolComposedActionTag.SPATULA_MIX, configuration);
        assertThatAnalyzeSingleResultRecipeToolComposedActionTermAndTagEquals("Termine de mezclar la cosa esa con la espátula", RecipeToolComposedActionTag.SPATULA_MIX, configuration);
    }

    @Test
    void testAnalyzeGivenInstructionWithSpatulaMixWellActionThenUtensilActionIsDetected() {
        assertThatAnalyzeSingleResultRecipeToolComposedActionTermAndTagEquals("Mezcle bien con la espátula", RecipeToolComposedActionTag.SPATULA_MIX_WELL, configuration);
        assertThatAnalyzeSingleResultRecipeToolComposedActionTermAndTagEquals("Mezcle muy bien con la espátula", RecipeToolComposedActionTag.SPATULA_MIX_WELL, configuration);
        assertThatAnalyzeSingleResultRecipeToolComposedActionTermAndTagEquals("Mezcle muy bien la cosa con la espátula", RecipeToolComposedActionTag.SPATULA_MIX_WELL, configuration);
        assertThatAnalyzeSingleResultRecipeToolComposedActionTermAndTagEquals("Mezcle la cosa muy bien con la espátula", RecipeToolComposedActionTag.SPATULA_MIX_WELL, configuration);
        assertThatAnalyzeSingleResultRecipeToolComposedActionTermAndTagEquals("Termine de mezclar muy bien con espátula", RecipeToolComposedActionTag.SPATULA_MIX_WELL, configuration);

        assertThatAnalyzeSingleResult(of("Mezcle bien con varillas"), configuration).satisfies(ria -> assertThat(ria.getTerms()).isEmpty());
    }

    @Test
    void testAnalyzeGivenInstructionWithSpatulaScrapActionThenUtensilActionIsDetected() {
        assertThatAnalyzeSingleResultRecipeToolComposedActionTermAndTagEquals("Baje los restos del interior del vaso con la ayuda de la espátula", RecipeToolComposedActionTag.SPATULA_SCRAP_SIDE, configuration);
        assertThatAnalyzeSingleResultRecipeToolComposedActionTermAndTagEquals("Baje los restos de las paredes del vaso con la ayuda de la espátula", RecipeToolComposedActionTag.SPATULA_SCRAP_SIDE, configuration);
        assertThatAnalyzeSingleResultRecipeToolComposedActionTermAndTagEquals("Baje los restos de las paredes con la ayuda de la espátula", RecipeToolComposedActionTag.SPATULA_SCRAP_SIDE, configuration);
        assertThatAnalyzeSingleResultRecipeToolComposedActionTermAndTagEquals("Con la espátula baje hacia el fondo del vaso", RecipeToolComposedActionTag.SPATULA_SCRAP_SIDE, configuration);
    }
}
