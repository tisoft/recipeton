package com.recipeton.shared.analysis.cooki.fr;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.configuration.RecipeAnalyzerConfiguration;
import com.recipeton.shared.analysis.configuration.cooki.RecipeAnalyzerCookiFrConfiguration;
import com.recipeton.shared.analysis.domain.RecipeInstructionAnalysis;
import com.recipeton.shared.analysis.test.support.RecipetonAnalyzerTestAssertTrait;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.util.stream.Stream;

import static java.util.List.of;
import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
public class RecipeInstructionAnalyzerCookiFrTest implements RecipetonAnalyzerTestAssertTrait {
    public static final String INSTRUCTION_1 = "Mettre la farine et le sucre dans le bol";
    public static final String INSTRUCTION_2 = "mixer <nobr>11 sec/vitesse 9</nobr>";

    private final RecipeAnalyzerConfiguration configuration = new RecipeAnalyzerCookiFrConfiguration();

    @Test
    void testAnalyzeGivenInstructionWithAddAndToMainCompartmentAndCommandActionThenInstructionsSplitted() {
        Stream.of(
                        INSTRUCTION_1 + ", et puis " + INSTRUCTION_2 + ".",
                        INSTRUCTION_1 + " puis " + INSTRUCTION_2 + ".",
                        INSTRUCTION_1 + ", faites " + INSTRUCTION_2 + ".",
                        INSTRUCTION_1 + ", puis " + INSTRUCTION_2 + ".",
                        INSTRUCTION_1 + ", " + INSTRUCTION_2 + ".")
                .forEach(
                        s ->
                                assertThat(createRecipeInstructionAnalyzer(configuration).analyze(of(s), createRecipeIngredientAnalyses(configuration, "")))
                                        .map(RecipeInstructionAnalysis::getText)
                                        .containsExactly(INSTRUCTION_1, "Mixer [11 sec/vitesse 9]"));
    }
}
