package com.recipeton.shared.analysis.cooki.es;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.configuration.cooki.RecipeAnalyzerCookiEsConfiguration;
import com.recipeton.shared.analysis.domain.RecipeToolAnalysis;
import com.recipeton.shared.analysis.service.RecipeToolAnalyzer;
import com.recipeton.shared.analysis.test.support.RecipetonAnalyzerTestFixtureTrait;
import org.junit.jupiter.api.Test;

import java.util.stream.Stream;

import static com.recipeton.shared.analysis.cooki.es.RecipeAnalyzerCookiEsTest.COOKI_ES_TOOLS_EXTERNAL;
import static com.recipeton.shared.analysis.cooki.es.RecipeAnalyzerCookiEsTest.COOKI_ES_TOOLS_UTENSIL;
import static org.assertj.core.api.Assertions.assertThat;

public class RecipeToolAnalyzerCookiEsTest implements RecipetonAnalyzerTestFixtureTrait {

    private final RecipeToolAnalyzer analyzer = createRecipeToolAnalyzer(new RecipeAnalyzerCookiEsConfiguration());

    @Test
    void testAnalyzeGivenReferenceExternalLocationInstructionThenAddsTags() {

        Stream.concat(Stream.of(COOKI_ES_TOOLS_EXTERNAL), Stream.of(COOKI_ES_TOOLS_UTENSIL))
                .flatMap(
                        s ->
                                Stream.of(
                                        "en un " + s,
                                        " en un " + s,
                                        "en unos " + s,
                                        " en unos " + s,
                                        "en una " + s,
                                        " en una " + s,
                                        "en el " + s,
                                        " en el " + s,
                                        "en la " + s,
                                        " en la " + s,
                                        "a un " + s,
                                        " a un " + s,
                                        "a la " + s,
                                        " a la " + s,
                                        "a el " + s,
                                        " a el " + s))
                .forEach(s -> assertThat(analyzer.isToolPresent(s)).withFailMessage("No match for '" + s + "'").isTrue());
    }

    @Test
    void testAnalyzeUtensilGivenUtensilSetsUid() {
        assertThat(analyzer.analyzeRecipeTool("bandeja de 20 cm")).isEqualTo(new RecipeToolAnalysis().setUid("cmbandeja20").setText("bandeja de 20 cm"));

        assertThat(analyzer.analyzeRecipeTool("Tazón de metal de 16 cm de diámetro")).isEqualTo(new RecipeToolAnalysis().setUid("tasonmetaldiametrocm16").setText("Tazón de metal de 16 cm de diámetro"));
    }
}
