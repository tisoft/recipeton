package com.recipeton.shared.analysis.cooki.es;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.configuration.cooki.RecipeAnalyzerCookiEsConfiguration;
import com.recipeton.shared.analysis.domain.RecipeToolComposedActionTag;
import com.recipeton.shared.analysis.test.support.RecipetonAnalyzerTestAssertTrait;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import static java.util.List.of;
import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
public class RecipeInstructionAnalyzerCookiEsTest implements RecipetonAnalyzerTestAssertTrait {

    private final RecipeAnalyzerCookiEsConfiguration configuration = new RecipeAnalyzerCookiEsConfiguration();

    @Test
    void testAnalyzeGivenInstructionWithJustStopWordsThenReturnsNoAnalysis() {
        assertThat(createRecipeInstructionAnalyzer(configuration).analyze(of("demás después"), createRecipeIngredientAnalyses(configuration, ""))).isEmpty();
    }

    @Test
    void testAnalyzeGivenInstructionWithOvenThenUtensilActionIsDetected() {
        assertThatAnalyzeSingleResultRecipeToolComposedActionTermAndTagEquals("precaliente el horno", RecipeToolComposedActionTag.OVEN_OPERATION, configuration);
        assertThatAnalyzeSingleResultRecipeToolComposedActionTermAndTagEquals("precaliente el horno a 180 grados", RecipeToolComposedActionTag.OVEN_OPERATION, configuration);
    }

    @Test
    void testAnalyzeGivenInstructionWithSplitTokenSequenceWhenFoundThenInstructionIsSplittedAndSequenceIsSubstituted() {
        assertThat(createRecipeInstructionAnalyzer(configuration).analyze(of("Ponga algo, cocine algo"), createRecipeIngredientAnalyses(configuration)))
                .hasSize(2)
                .satisfies(
                        l -> {
                            assertThat(l).element(0).satisfies(i -> assertThat(i.getText()).isEqualTo("Ponga algo"));
                            assertThat(l).element(1).satisfies(i -> assertThat(i.getText()).isEqualTo("Cocine algo"));
                        });
    }

    @Test
    void testAnalyzeGivenInstructionWithSplitTokenSequenceWitMoreThanOneWordWhenFoundThenInstructionIsSplittedAndSequenceIsSubstituted() {
        assertThat(createRecipeInstructionAnalyzer(configuration).analyze(of("Ponga algo, haga puré eso"), createRecipeIngredientAnalyses(configuration)))
                .hasSize(2)
                .satisfies(
                        l -> {
                            assertThat(l).element(0).satisfies(i -> assertThat(i.getText()).isEqualTo("Ponga algo"));
                            assertThat(l).element(1).satisfies(i -> assertThat(i.getText()).isEqualTo("Haga puré eso"));
                        });
    }

    @Test
    void testAnalyzeGivenInstructionWithSplitTokenSequenceWithWordEndingInYThenInstructionIsNotSplitted() {
        assertThat(createRecipeInstructionAnalyzer(configuration).analyze(of("Ponga algo muy caliente"), createRecipeIngredientAnalyses(configuration)))
                .singleElement()
                .satisfies(i -> assertThat(i.getText()).isEqualTo("Ponga algo muy caliente"));
    }

    @Test
    void testAnalyzeGivenInstructionWithSplitTokenSequenceWithYThenInstructionIsSplittedAndSequenceIsSubstituted() {
        assertThat(createRecipeInstructionAnalyzer(configuration).analyze(of("Ponga algo, trocee eso"), createRecipeIngredientAnalyses(configuration)))
                .hasSize(2)
                .satisfies(
                        l -> {
                            assertThat(l).element(0).satisfies(i -> assertThat(i.getText()).isEqualTo("Ponga algo"));
                            assertThat(l).element(1).satisfies(i -> assertThat(i.getText()).isEqualTo("Trocee eso"));
                        });
    }
}
