package com.recipeton.shared.analysis.cooki.es;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.configuration.cooki.RecipeAnalyzerCookiEsConfiguration;
import com.recipeton.shared.analysis.domain.RecipeDifficultyDefinition;
import com.recipeton.shared.analysis.domain.RecipePriceDefinition;
import com.recipeton.shared.analysis.service.RecipeAnalyzer;
import com.recipeton.shared.analysis.test.support.RecipetonAnalyzerTestFixtureTrait;
import com.recipeton.shared.domain.RecipeDefinition;
import org.junit.jupiter.api.Test;

import static java.util.List.of;
import static org.assertj.core.api.Assertions.assertThat;

public class RecipeAnalyzerCookiEsTest implements RecipetonAnalyzerTestFixtureTrait {

    public static final String[] COOKI_ES_TOOLS_UTENSIL = {"varoma", "vaporera", "steamer", "cestillo", "cubilete", "espátula", "mariposa", "nevera", "frigorífico", "congelador", "horno"};

    public static final String[] COOKI_ES_TOOLS_EXTERNAL = {
        "bandeja", "bandejas",
        "bol", "boles",
        "cazo", "cazos",
        "cazuela", "cazuelas",
        "cazuelita", "cazuelitas",
        "charola", "charolas",
        "copa", "copas",
        "fuente", "fuentes",
        "jarra", "jarras",
        "jarrita", "jarritas",
        "molde", "moldes",
        "olla", "ollas",
        "plato", "platos",
        "platillo", "platillos",
        "recipiente", "recipientes",
        "refractario", "refractarios",
        "remequín", "remequines",
        "sartén", "sartenes",
        "sopera", "soperas",
        "taza", "tazas",
        "tacita", "tacitas",
        "tazón", "tazones",
    };

    private final RecipeAnalyzer analyzer = createRecipeStandardAnalyzer(new RecipeAnalyzerCookiEsConfiguration());

    public RecipeDefinition createRecipeDefinition(String name) {
        RecipeDefinition recipeDefinition = new RecipeDefinition();
        recipeDefinition.setName(name);
        return recipeDefinition;
    }

    @Test
    public void testAnalyzeRecipeDifficultyGivenAdvancedStringThenReturnsAdvanced() {
        assertThat(analyzer.analyzeRecipeDifficulty(new RecipeDefinition().setDifficulty("avanzado"))).isEqualTo(RecipeDifficultyDefinition.ADVANCED);
    }

    @Test
    public void testAnalyzeRecipeDifficultyGivenEasyStringThenReturnsEasy() {
        assertThat(analyzer.analyzeRecipeDifficulty(new RecipeDefinition().setDifficulty("Fácil"))).isEqualTo(RecipeDifficultyDefinition.EASY);
    }

    @Test
    public void testAnalyzeRecipeDifficultyGivenMediumStringThenReturnsMeidum() {
        assertThat(analyzer.analyzeRecipeDifficulty(new RecipeDefinition().setDifficulty("medio"))).isEqualTo(RecipeDifficultyDefinition.MEDIUM);
    }

    @Test
    public void testAnalyzeRecipeDifficultyGivenUnknowStringThenReturnsUnknown() {
        assertThat(analyzer.analyzeRecipeDifficulty(new RecipeDefinition().setDifficulty("xxx"))).isEqualTo(RecipeDifficultyDefinition.UNKNOWN);
    }

    @Test
    public void testAnalyzeRecipePriceGivenCheapKeywordThenReturnsNoInformation() {
        assertThat(analyzer.analyzeRecipePrice(new RecipeDefinition().setKeywords(of("barato")))).isEqualTo(RecipePriceDefinition.LOW);
    }

    @Test
    public void testAnalyzeRecipePriceGivenNoKeywordsThenReturnsNoInformation() {
        assertThat(analyzer.analyzeRecipePrice(new RecipeDefinition())).isEqualTo(RecipePriceDefinition.NO_INFORMATION);
    }

    @Test
    public void testAnalyzeRecipePriceGivenUnmatchedStringThenReturnsNoInformation() {
        assertThat(analyzer.analyzeRecipePrice(new RecipeDefinition().setKeywords(of("xxxx")))).isEqualTo(RecipePriceDefinition.NO_INFORMATION);
    }

    @Test
    public void testFormatTitleGivenAsciiCharactersOnlyThenReturnsSameCapitalized() {
        assertThat(analyzer.analyzeTitle(createRecipeDefinition("Abc"))).isEqualTo("Abc");
    }

    @Test
    public void testFormatTitleGivenNotCapitalizedThenReturnsCapitalized() {
        assertThat(analyzer.analyzeTitle(createRecipeDefinition("abc"))).isEqualTo("Abc");
        assertThat(analyzer.analyzeTitle(createRecipeDefinition("ABC"))).isEqualTo("Abc");
    }

    @Test
    public void testFormatTitleGivenNotTrimmedThenReturnsTrimmed() {
        assertThat(analyzer.analyzeTitle(createRecipeDefinition(" Abc"))).isEqualTo("Abc");
        assertThat(analyzer.analyzeTitle(createRecipeDefinition("Abc "))).isEqualTo("Abc");
    }

    @Test
    public void testFormatTitleGivenQuoteCharactersThenReturnsWithoutQuouteCharaters() {
        assertThat(analyzer.analyzeTitle(createRecipeDefinition("'Abc'"))).isEqualTo("Abc");
        assertThat(analyzer.analyzeTitle(createRecipeDefinition("\"Abc\""))).isEqualTo("Abc");
    }

    @Test
    public void testFormatTitleGivenSpecialCharactersInFirstPositionThenReturnsSpecialCharactersReplacedByAsciiEquivalent() {
        assertThat(analyzer.analyzeTitle(createRecipeDefinition("óntologo"))).isEqualTo("Ontologo");
    }

    @Test
    public void testFormatTitleGivenSpecialCharactersInNotFirstPositionThenReturnsSpecialCharactersNotReplaced() {
        assertThat(analyzer.analyzeTitle(createRecipeDefinition("Situación crítica"))).isEqualTo("Situación crítica");
    }
}
