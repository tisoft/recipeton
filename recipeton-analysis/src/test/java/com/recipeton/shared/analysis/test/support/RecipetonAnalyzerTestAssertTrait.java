package com.recipeton.shared.analysis.test.support;

/*-
 * #%L
 * recipeton-analysis
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.configuration.RecipeAnalyzerConfiguration;
import com.recipeton.shared.analysis.domain.*;
import org.assertj.core.api.AbstractObjectAssert;
import org.assertj.core.api.ListAssert;
import org.assertj.core.api.ObjectAssert;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.List.of;
import static org.assertj.core.api.Assertions.assertThat;

public interface RecipetonAnalyzerTestAssertTrait extends RecipetonAnalyzerTestFixtureTrait {

    default ObjectAssert<RecipeInstructionAnalysis> assertThatAnalyzeSingleResult(List<String> instructions, RecipeAnalyzerConfiguration configuration) {
        return assertThat(createRecipeInstructionAnalyzer(configuration).analyze(instructions, createRecipeIngredientAnalyses(configuration))).singleElement();
    }

    default void assertThatAnalyzeSingleResultRecipeToolComposedActionTermAndTagEquals(String instruction, RecipeToolComposedActionTag tag, RecipeAnalyzerConfiguration configuration) {
        assertThatAnalyzeSingleResult(of(instruction), configuration)
                .satisfies(ria -> assertThat(ria.getTerms().stream().filter(t -> t instanceof RecipeToolComposedActionTerm).map(t -> ((RecipeToolComposedActionTerm) t).getTag())).contains(tag));
    }

    default ListAssert<RecipeIngredientTerm> assertThatNoRecipeIngredientTermsAreAddedByNormalAnalysis(RecipeInstructionAnalysis recipeInstructionAnalysis) {
        return assertThat(recipeInstructionAnalysis.getRecipeIngredientTerms()).noneSatisfy(t -> assertThat(t.getMatch().isContextual()).isFalse());
    }

    default AbstractObjectAssert<?, RecipeCommandTerm> assertThatRecipeCommandTerm(String wrappedCommand, RecipeAnalyzerConfiguration configuration) {
        return assertThatAnalyzeSingleResult(of(wrappedCommand), configuration).extracting(ria -> ria.getCommandTerms().get(0));
    }

    default AbstractObjectAssert<?, RecipeCommandTerm> assertThatRecipeCommandTermWrapping(String instruction, RecipeAnalyzerConfiguration configuration) {
        return assertThatRecipeCommandTerm(createWrappedCommand(instruction, configuration), configuration);
    }

    default Stream<String> extractRecipeIngredientTermNotations(RecipeInstructionAnalysis recipeInstructionAnalysis) {
        return extractRecipeIngredientTermNotations(recipeInstructionAnalysis.getTerms());
    }

    default Stream<String> extractRecipeIngredientTermNotations(Collection<RecipeTerm> terms) {
        return extractRecipeIngredientTerms(terms).map(t -> t.getIngredient().getIngredientNotation());
    }

    private Stream<RecipeIngredientTerm> extractRecipeIngredientTerms(Collection<RecipeTerm> terms) {
        return terms.stream().filter(t -> t instanceof RecipeIngredientTerm).map(t -> (RecipeIngredientTerm) t);
    }

    default List<RecipeIngredientTerm> extractRecipeIngredientTermsByIngredientNotation(String ingredientNotation, RecipeInstructionAnalysis recipeInstructionAnalysis) {
        return extractRecipeIngredientTerms(recipeInstructionAnalysis.getTerms()).filter(t -> t.getIngredient().getIngredientNotation().equals(ingredientNotation)).collect(Collectors.toList());
    }
}
