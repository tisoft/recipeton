package com.recipeton.shared.analysis.configuration.cooki;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.configuration.*;
import com.recipeton.shared.analysis.domain.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;

import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.recipeton.shared.analysis.configuration.RecipeCategoryAnalyzerConfiguration.FUNCTION_REMOVE_PARENTHESES_AND_CAPITALIZE_FIRST;
import static com.recipeton.shared.analysis.configuration.RecipeMagnitudeAnalyzerConfiguration.*;
import static com.recipeton.shared.analysis.configuration.cooki.CookiConstants.*;
import static com.recipeton.shared.analysis.domain.MeasurementUnitDefinition.*;
import static com.recipeton.shared.analysis.domain.RecipeCategoryDefinition.*;
import static com.recipeton.shared.analysis.domain.RecipeCommandProgram.*;
import static com.recipeton.shared.analysis.domain.RecipeInstructionRegexMatcher.toRecipeInstructionMatcher;
import static com.recipeton.shared.analysis.domain.RecipeInstructionTag.*;
import static com.recipeton.shared.analysis.domain.RecipeToolTag.TOOL_EXTERNAL;
import static com.recipeton.shared.analysis.service.RecipeInstructionSplitter.DO_NOT_SPLIT;
import static com.recipeton.shared.analysis.service.RecipeInstructionSplitter.Replacement;
import static com.recipeton.shared.analysis.service.RecipeInstructionSplitter.Replacement.toCurrent;
import static com.recipeton.shared.analysis.service.RecipeInstructionSplitter.Replacement.toNext;
import static com.recipeton.shared.util.MapUtil.toMap;
import static com.recipeton.shared.util.MapUtil.toMapP;
import static com.recipeton.shared.util.StreamUtil.concat;
import static com.recipeton.shared.util.TextUtil.*;
import static java.util.List.of;
import static java.util.regex.Pattern.compile;
import static org.apache.commons.lang3.StringUtils.containsIgnoreCase;

// WIP! COPIED FROM Es
public class RecipeAnalyzerCookiDeConfiguration extends RecipeAnalyzerConfiguration {

    public static final MeasurementUnitDefinition UNIT_PIECE_DE = new MeasurementUnitDefinition("Stück", UNIT_PIECE, false, "stücken");
    public static final MeasurementUnitDefinition UNIT_PIECE_STANGEL = new MeasurementUnitDefinition("Stängel", UNIT_PIECE_DE, false);
    public static final MeasurementUnitDefinition UNIT_PINCH_DE = new MeasurementUnitDefinition("Prise", UNIT_PINCH, false, "prisen");

    public static final String[] COOKI_DE_ACTION_REGEXES_ACTIVE = {"dünste"};

    public static final String[] COOKI_DE_ACTION_REGEXES_PASSIVE = {
        "abschmecken",
        "abspülen",
        "auffangen",
        "auffüllen",
        "aufrollen",
        "aufsetzen",
        "bestreichen",
        "dünsten",
        "einhängen",
        "einsetzen",
        "einwiegen",
        "füllen",
        "garen",
        "geben",
        "hacken",
        "herausnehmen",
        "kochen",
        "lassen",
        "leeren",
        "legen",
        "machen",
        "pürieren",
        "reichen",
        "servieren",
        "stellen",
        "umfüllen",
        "vermischen",
        "verschließen",
        "wiegen",
        "würzen",
        "zerkleinern",
        "zugeben"
    };

    private static final Map<RecipeToolDefinition, RecipeInstructionMatcher> RECIPE_TOOL_MATCHERS =
            toMapP(
                    Pair.of(new RecipeToolDefinition("casserole", true, TOOL_EXTERNAL), toRecipeInstructionMatcher("\\b(?:casserole|casseroles)\\b")),
                    Pair.of(new RecipeToolDefinition("dish", true, TOOL_EXTERNAL), toRecipeInstructionMatcher("\\b(?:dish|dishes)\\b"))
                    // ... complete
                    );

    public RecipeAnalyzerCookiDeConfiguration() {
        super(
                "COOKI_DE",
                rd -> containsIgnoreCase(rd.getLocale(), "de") && containsIgnoreCase(rd.getSource(), "cooki"),
                createRecipeLangAnalyzerConfiguration(),
                createRecipeCategoryAnalyzerConfiguration(),
                createRecipeCommandAnalyzerConfiguration(),
                createRecipeMagnitudeAnalyzerConfiguration(),
                createRecipeMeasurementAnalyzerConfiguration(),
                createRecipeInstructionAnalyzerConfiguration(),
                createRecipeIngredientAnalyzerConfiguration(),
                createRecipeToolAnalyzerConfiguration(),
                createRecipeCustomizationConfiguration());
    }

    public static RecipeCategoryAnalyzerConfiguration createRecipeCategoryAnalyzerConfiguration() {
        return new RecipeCategoryAnalyzerConfiguration()
                .addRecipeCategoryByPrincipal(UID_BASIC, "Grundrezepte")
                .addRecipeCategoryByPrincipal(UID_START, "Vorspeisen und Salate")
                .addRecipeCategoryByPrincipal(UID_SOUP, "Suppen und Suppeneinlagen", "Suppen")
                .addRecipeCategoryByPrincipal(UID_PASTARICE, "Pasta und Reisgerichte", "Pasta- und Reisgerichte", "Nudel- und Reisgerichte")
                .addRecipeCategoryByPrincipal(UID_MAINMEAT, "Hauptspeisen mit Fleisch und Geflügel", "Hauptgerichte mit Fleisch")
                .addRecipeCategoryByPrincipal(UID_MAINFISH, "Hauptspeisen mit Fisch und Meeresfrüchten", "Hauptgerichte mit Fisch")
                .addRecipeCategoryByPrincipal(UID_MAINVEG, "Hauptspeisen, vegetarisch")
                .addRecipeCategoryByPrincipal(UID_MAINOTHER, "Hauptgerichte", "Hauptgerichte, sonstige")
                .addRecipeCategoryByPrincipal(UID_SIDE, "Beilagen")
                .addRecipeCategoryByPrincipal(UID_BAKE, "Brot und Brötchen", "Backen", "Brot und Gebäck")
                .addRecipeCategoryByPrincipal(UID_BAKE_SAVORY, "Backen, herzhaft", "Backen, pikant")
                .addRecipeCategoryByPrincipal(UID_DESSERT, "Desserts, Pâtisserie und Süssigkeiten", "Desserts", "Desserts und Süßigkeiten")
                .addRecipeCategoryByPrincipal(UID_BAKE_SWEET, "Backen, süß", "Backen, süss")
                .addRecipeCategoryByPrincipal(UID_SAUCE_SWEET, "Saucen, Dips und Brotaufstriche, süß", "Saucen, Dips und Aufstriche, süß", "Saucen, Dips und Brotaufstriche, süss")
                .addRecipeCategoryByPrincipal(UID_DRINK, "Getränke")
                .addRecipeCategoryByPrincipal(UID_SNACK, "Snacks", "Snacks und Finger Food", "Snacks und Fingerfood")
                .addRecipeCategoryByPrincipal(UID_SAUCE_SAVO, "Saucen, Dips und Brotaufstriche", "Saucen, Dips und Brotaufstriche - pikant", "Saucen, Dips und Aufstriche, herzhaft", "Saucen, Dips und Brotaufstriche, herzhaft")
                .addRecipeCategoryByPrincipal(UID_BABY, "Baby`s Beikost", "Säuglingsnahrung")
                .addRecipeCategoryByPrincipal(UID_BREAK, "Frühstück")
                .addRecipeCategoryByPrincipal(UID_MENU, "Menüs und mehr")
                .setRecipeCategoryNamePreprocessor(
                        new Function<>() {
                            private final Pattern cleanupPattern = compile("(?i)" + String.join("|", "kochen", "küche"));

                            @Override
                            public String apply(String s) {
                                return FUNCTION_REMOVE_PARENTHESES_AND_CAPITALIZE_FIRST.apply(removeAll(s, cleanupPattern));
                            }
                        });
    }

    public static RecipeCommandAnalyzerConfiguration createRecipeCommandAnalyzerConfiguration() {
        return new RecipeCommandAnalyzerConfiguration()
                .setCommandProgramPredicates(
                        toMap(
                                DOUGH, stringPredicatePatterns("(?i)" + TM_DOUGH + "|dough"),
                                FERMENT, of(),
                                HEAT_ONLY, of(),
                                SOUS_VIDE, of(),
                                TURBO, stringPredicatePatterns("(?i)turbo")))
                .setCommandDurationPredicates(toMap(RecipeCommandAnalyzerConfiguration.HALF_SECOND, stringPredicatePatterns("^0\\.5 Sek\\.")))
                .setCommandDurationHoursPattern(compile("^([0-9]+) Std\\."))
                .setCommandDurationMinutesPattern(compile("^([0-9]+) Min\\."))
                .setCommandDurationSecondsPattern(compile("^([0-9]+) Sek\\."))
                .setCommandTemperaturePredicates(toMap(STEAMER_TEMPERATURE, stringPredicatePatterns("(?i)varoma|steam")))
                .setCommandTemperaturePattern(compile("^([0-9]+)°C"))
                .setCommandSpeedPredicates(toMap(COMMAND_SPEED_SLOW_VALUE, stringPredicatePatterns("(?i)stuffe " + TM_SLOW + "|stuffe slow")))
                .setCommandRotationReversePredicates(stringPredicatePatterns("(?i)" + TM_REVERSE + "|reverse"))
                .setCommandSpeedPattern(compile("stuffe ([//.0-9]+)"))
                .setCommandRepetitionFromPattern(compile("^([0-9]+) (?:mal)"))
                .setCommandRepetitionFromToPattern(compile("^([0-9]+)-([0-9]+) mal"));
    }

    public static RecipeCustomizationConfiguration createRecipeCustomizationConfiguration() {
        return new RecipeCustomizationConfiguration()
                //                        .setBackgroundReportOriginalTitleLabel("Original title")
                //                        .setBackgroundReportAuthorLabel("Author")
                //                        .setBackgroundReportCollectionsLabel("Collections")
                //                        .setBackgroundReportSourceLabel("Source")
                //                        .setRecipeAdditionUnclearStepSubtitle("Attention! Instruction à vérifier")
                //                        .setRecipeUnclearTitle("<b>ATTENCION!</b> Recette douteuse!")
                //                        .setRecipeUnclearBody("Certains ingrédients n'ont pas été trouvés dans les instructions.<br>"
                //                                + "<b>Vérifier manuellement</b>  lorsque vous devez mettre des ingrédients dans la recette.<br>"
                //                                + "<i>Corrigez la recette si possible!</i><br><br>"
                //                                + "<b>Ingrédients douteux:</b><br>")

                .setRecipeDifficultDefinitionPredicates(
                        toMap(
                                RecipeDifficultyDefinition.EASY, stringPredicatePattern("(?i)einfach"),
                                RecipeDifficultyDefinition.MEDIUM, stringPredicatePattern("(?i)medium"),
                                RecipeDifficultyDefinition.ADVANCED, stringPredicatePattern("(?i)aufwändig")))
                .setRecipePriceDefinitionPredicates(toMap(RecipePriceDefinition.LOW, stringPredicatePattern("(?i)preiswert|geldsparend")));
    }

    public static RecipeIngredientAnalyzerConfiguration createRecipeIngredientAnalyzerConfiguration() {
        return new RecipeIngredientAnalyzerConfiguration("oder")
                .setIngredientPreparationTokens(
                        Set.of(
                                "abgeschnitten",
                                "abgetropft",
                                "abgezupft",
                                "altbacken",
                                "am",
                                "ausgelöst",
                                "eingeweicht",
                                "entkernt",
                                "etwas",
                                "fest",
                                "frisch",
                                "frische",
                                "gekauft",
                                "geräuchert",
                                "geschnetzelt",
                                "getrocknet",
                                "geviertelt",
                                "gewürfelt",
                                "gut",
                                "halbiert",
                                "in",
                                "mehlig",
                                "ohne",
                                "parboiled",
                                "selbstgemacht",
                                "weich"
                                // "gut ausgedrückt",
                                // "in mundgerechten Stücken",
                                //                "am Stück",
                                //                "etwas mehr nach Geschmack",
                                //                "etwas mehr zum Einfetten",
                                //                "fest kochend",
                                //                "frisch gemahlen",
                                //                "in Scheiben",
                                //                "in Streifen",
                                //                "in Stücken",
                                //                "in Wedges",
                                //                "in dicken Streifen",
                                //                "in kleinen Stücken",
                                //                "mehlig kochend",
                                //                "ohne Stiele",
                                //                "ohne Vorkochen",
                                ))
                .setIngredientOptionalIndicatorTokens(
                        Set.of(
                                // "(optional)???"
                                ))
                .setIngredientForbiddenRegexes(concat(Stream.of(COOKI_DE_ACTION_REGEXES_ACTIVE), Stream.of(COOKI_DE_ACTION_REGEXES_PASSIVE)).collect(Collectors.toSet()));
    }

    public static RecipeInstructionAnalyzerConfiguration createRecipeInstructionAnalyzerConfiguration() {
        return new RecipeInstructionAnalyzerConfiguration(
                concat(
                                Stream.of(
                                        toNext(" approx.", DO_NOT_SPLIT), // Careful. Due to limitationa all need to start from space or punctuation
                                        toNext(".", "")),
                                Stream.of(COOKI_DE_ACTION_REGEXES_ACTIVE)
                                        .map(String::toLowerCase)
                                        .flatMap(
                                                v ->
                                                        Stream.of(
                                                                        " und ", // Careful. Due to limitations all need to start from space or punctuation
                                                                        ", ")
                                                                .map(s -> toNext(s + v, StringUtils.capitalize(v)))),
                                Stream.of(COOKI_DE_ACTION_REGEXES_PASSIVE)
                                        .map(String::toLowerCase)
                                        .flatMap(
                                                v ->
                                                        Stream.of(
                                                                        " und ", // Careful. Due to limitationa all need to start from space or punctuation
                                                                        ", ")
                                                                .map(s -> toCurrent(v + s, " " + v))))
                        .toArray(Replacement[]::new),
                toMapP(
                        Pair.of(DO_ADD, toRecipeInstructionMatcher("(?i) zugeben")),
                        Pair.of(DO_PLACE, toRecipeInstructionMatcher("(?i) geben")),
                        //                        Pair.of(RecipeAnalysisTag.ADD_BACK, toRecipeInstructionMatcher("(?i)regrese|retorne")),
                        //                                Pair.of(DO_WEIGH, toRecipeInstructionMatcher("(?i)(?:weigh .*into it|weigh in )")),
                        Pair.of(DO_SERVE, toRecipeInstructionMatcher("(?i)servieren")),
                        //                                Pair.of(DO_PREHEAT, toRecipeInstructionMatcher("(?i)\\bpreheat\\b")),

                        Pair.of(FOCUS_MAIN, toRecipeInstructionMatcher("(?i)in den mixtopf"))
                        //                                Pair.of(LOCATION_ON_TOP, toRecipeInstructionMatcher("(?i)onto mixing bowl lid")),
                        //                                Pair.of(LOCATION_EXTERNAL, toRecipeInstructionMatcher("(?i)" + REGEX_LOCATION_EXTERNAL)),
                        //                                Pair.of(LOCATION_UNDEFINED, toRecipeInstructionMatcher("(?i)set aside")),
                        //
                        //                                Pair.of(ALL_INGREDIENTS, toRecipeInstructionMatcher("(?i)all ingredients")),
                        //                                Pair.of(ALL_INGREDIENTS_WITH_EXCEPTIONS, toRecipeInstructionMatcher("(?i)all ingredients.*except")),
                        //                                Pair.of(ALL_INGREDIENTS_REMAINING, toRecipeInstructionMatcher("(?i)(?:all )?remaining.*ingredients")),
                        //
                        //                                Pair.of(PARALLEL, toRecipeInstructionMatcher("(?i)meanwhile"))
                        ),
                compile("[ ]?(?:,|\\bund\\b)[ ]?"));
    }

    public static RecipeLangAnalyzerConfiguration createRecipeLangAnalyzerConfiguration() {
        return new RecipeLangAnalyzerConfiguration(
                Set.of(
                        "a",
                        "ab",
                        "aber",
                        "ach",
                        "acht",
                        "achte",
                        "achten",
                        "achter",
                        "achtes",
                        "ag",
                        "alle",
                        "allein",
                        "allem",
                        "allen",
                        "aller",
                        "allerdings",
                        "alles",
                        "allgemeinen",
                        "als",
                        "also",
                        "am",
                        "an",
                        "andere",
                        "anderen",
                        "andern",
                        "anders",
                        "au",
                        "auch",
                        "auf",
                        "aus",
                        "ausser",
                        "außer",
                        "ausserdem",
                        "außerdem",
                        "b",
                        "bald",
                        "bei",
                        "beide",
                        "beiden",
                        "beim",
                        "beispiel",
                        "bekannt",
                        "bereits",
                        "besonders",
                        "besser",
                        "besten",
                        "bin",
                        "bis",
                        "bisher",
                        "bist",
                        "c",
                        "d",
                        "da",
                        "dabei",
                        "dadurch",
                        "dafür",
                        "dagegen",
                        "daher",
                        "dahin",
                        "dahinter",
                        "damals",
                        "damit",
                        "danach",
                        "daneben",
                        "dank",
                        "dann",
                        "daran",
                        "darauf",
                        "daraus",
                        "darf",
                        "darfst",
                        "darin",
                        "darüber",
                        "darum",
                        "darunter",
                        "das",
                        "dasein",
                        "daselbst",
                        "dass",
                        "daß",
                        "dasselbe",
                        "davon",
                        "davor",
                        "dazu",
                        "dazwischen",
                        "dein",
                        "deine",
                        "deinem",
                        "deiner",
                        "dem",
                        "dementsprechend",
                        "demgegenüber",
                        "demgemäss",
                        "demgemäß",
                        "demselben",
                        "demzufolge",
                        "den",
                        "denen",
                        "denn",
                        "denselben",
                        "der",
                        "deren",
                        "derjenige",
                        "derjenigen",
                        "dermassen",
                        "dermaßen",
                        "derselbe",
                        "derselben",
                        "des",
                        "deshalb",
                        "desselben",
                        "dessen",
                        "deswegen",
                        "d.h",
                        "dich",
                        "die",
                        "diejenige",
                        "diejenigen",
                        "dies",
                        "diese",
                        "dieselbe",
                        "dieselben",
                        "diesem",
                        "diesen",
                        "dieser",
                        "dieses",
                        "dir",
                        "doch",
                        "dort",
                        "drei",
                        "drin",
                        "dritte",
                        "dritten",
                        "dritter",
                        "drittes",
                        "du",
                        "durch",
                        "durchaus",
                        "dürfen",
                        "dürft",
                        "durfte",
                        "durften",
                        "e",
                        "eben",
                        "ebenso",
                        "ehrlich",
                        "ei",
                        "ei,",
                        "eigen",
                        "eigene",
                        "eigenen",
                        "eigener",
                        "eigenes",
                        "ein",
                        "einander",
                        "eine",
                        "einem",
                        "einen",
                        "einer",
                        "eines",
                        "einige",
                        "einigen",
                        "einiger",
                        "einiges",
                        "einmal",
                        "eins",
                        "elf",
                        "en",
                        "ende",
                        "endlich",
                        "entweder",
                        "er",
                        "Ernst",
                        "erst",
                        "erste",
                        "ersten",
                        "erster",
                        "erstes",
                        "es",
                        "etwa",
                        "etwas",
                        "euch",
                        "f",
                        "früher",
                        "fünf",
                        "fünfte",
                        "fünften",
                        "fünfter",
                        "fünftes",
                        "für",
                        "g",
                        "gab",
                        "ganz",
                        "ganze",
                        "ganzen",
                        "ganzer",
                        "ganzes",
                        "gar",
                        "gedurft",
                        "gegen",
                        "gegenüber",
                        "gehabt",
                        "gehen",
                        "geht",
                        "gekannt",
                        "gekonnt",
                        "gemacht",
                        "gemocht",
                        "gemusst",
                        "genug",
                        "gerade",
                        "gern",
                        "gesagt",
                        "geschweige",
                        "gewesen",
                        "gewollt",
                        "geworden",
                        "gibt",
                        "ging",
                        "gleich",
                        "gott",
                        "gross",
                        "groß",
                        "grosse",
                        "große",
                        "grossen",
                        "großen",
                        "grosser",
                        "großer",
                        "grosses",
                        "großes",
                        "gut",
                        "gute",
                        "guter",
                        "gutes",
                        "h",
                        "habe",
                        "haben",
                        "habt",
                        "hast",
                        "hat",
                        "hatte",
                        "hätte",
                        "hatten",
                        "hätten",
                        "heisst",
                        "her",
                        "heute",
                        "hier",
                        "hin",
                        "hinter",
                        "hoch",
                        "i",
                        "ich",
                        "ihm",
                        "ihn",
                        "ihnen",
                        "ihr",
                        "ihre",
                        "ihrem",
                        "ihren",
                        "ihrer",
                        "ihres",
                        "im",
                        "immer",
                        "in",
                        "indem",
                        "infolgedessen",
                        "ins",
                        "irgend",
                        "ist",
                        "j",
                        "ja",
                        "jahr",
                        "jahre",
                        "jahren",
                        "je",
                        "jede",
                        "jedem",
                        "jeden",
                        "jeder",
                        "jedermann",
                        "jedermanns",
                        "jedoch",
                        "jemand",
                        "jemandem",
                        "jemanden",
                        "jene",
                        "jenem",
                        "jenen",
                        "jener",
                        "jenes",
                        "jetzt",
                        "k",
                        "kam",
                        "kann",
                        "kannst",
                        "kaum",
                        "kein",
                        "keine",
                        "keinem",
                        "keinen",
                        "keiner",
                        "kleine",
                        "kleinen",
                        "kleiner",
                        "kleines",
                        "kommen",
                        "kommt",
                        "können",
                        "könnt",
                        "konnte",
                        "könnte",
                        "konnten",
                        "kurz",
                        "l",
                        "lang",
                        "lange",
                        "leicht",
                        "leide",
                        "lieber",
                        "los",
                        "m",
                        "machen",
                        "macht",
                        "machte",
                        "mag",
                        "magst",
                        "mahn",
                        "man",
                        "manche",
                        "manchem",
                        "manchen",
                        "mancher",
                        "manches",
                        "mann",
                        "mehr",
                        "mein",
                        "meine",
                        "meinem",
                        "meinen",
                        "meiner",
                        "meines",
                        "mensch",
                        "menschen",
                        "mich",
                        "mir",
                        "mit",
                        "mittel",
                        "mochte",
                        "möchte",
                        "mochten",
                        "mögen",
                        "möglich",
                        "mögt",
                        "morgen",
                        "muss",
                        "muß",
                        "müssen",
                        "musst",
                        "müsst",
                        "musste",
                        "mussten",
                        "n",
                        "na",
                        "nach",
                        "nachdem",
                        "nahm",
                        "natürlich",
                        "neben",
                        "nein",
                        "neue",
                        "neuen",
                        "neun",
                        "neunte",
                        "neunten",
                        "neunter",
                        "neuntes",
                        "nicht",
                        "nichts",
                        "nie",
                        "niemand",
                        "niemandem",
                        "niemanden",
                        "noch",
                        "nun",
                        "nur",
                        "o",
                        "ob",
                        "oben",
                        "oder",
                        "offen",
                        "oft",
                        "ohne",
                        "Ordnung",
                        "p",
                        "q",
                        "r",
                        "recht",
                        "rechte",
                        "rechten",
                        "rechter",
                        "rechtes",
                        "richtig",
                        "rund",
                        "s",
                        "sa",
                        "sache",
                        "sagt",
                        "sagte",
                        "sah",
                        "satt",
                        "schlecht",
                        "Schluss",
                        "schon",
                        "sechs",
                        "sechste",
                        "sechsten",
                        "sechster",
                        "sechstes",
                        "sehr",
                        "sei",
                        "seid",
                        "seien",
                        "sein",
                        "seine",
                        "seinem",
                        "seinen",
                        "seiner",
                        "seines",
                        "seit",
                        "seitdem",
                        "selbst",
                        "sich",
                        "sie",
                        "sieben",
                        "siebente",
                        "siebenten",
                        "siebenter",
                        "siebentes",
                        "sind",
                        "so",
                        "solang",
                        "solche",
                        "solchem",
                        "solchen",
                        "solcher",
                        "solches",
                        "soll",
                        "sollen",
                        "sollte",
                        "sollten",
                        "sondern",
                        "sonst",
                        "sowie",
                        "später",
                        "statt",
                        "t",
                        "tag",
                        "tage",
                        "tagen",
                        "tat",
                        "teil",
                        "tel",
                        "tritt",
                        "trotzdem",
                        "tun",
                        "u",
                        "über",
                        "überhaupt",
                        "übrigens",
                        "uhr",
                        "um",
                        "und",
                        "und?",
                        "uns",
                        "unser",
                        "unsere",
                        "unserer",
                        "unter",
                        "v",
                        "vergangenen",
                        "viel",
                        "viele",
                        "vielem",
                        "vielen",
                        "vielleicht",
                        "vier",
                        "vierte",
                        "vierten",
                        "vierter",
                        "viertes",
                        "vom",
                        "von",
                        "vor",
                        "w",
                        "wahr?",
                        "während",
                        "währenddem",
                        "währenddessen",
                        "wann",
                        "war",
                        "wäre",
                        "waren",
                        "wart",
                        "warum",
                        "was",
                        "wegen",
                        "weil",
                        "weit",
                        "weiter",
                        "weitere",
                        "weiteren",
                        "weiteres",
                        "welche",
                        "welchem",
                        "welchen",
                        "welcher",
                        "welches",
                        "wem",
                        "wen",
                        "wenig",
                        "wenige",
                        "weniger",
                        "weniges",
                        "wenigstens",
                        "wenn",
                        "wer",
                        "werde",
                        "werden",
                        "werdet",
                        "wessen",
                        "wie",
                        "wieder",
                        "will",
                        "willst",
                        "wir",
                        "wird",
                        "wirklich",
                        "wirst",
                        "wo",
                        "wohl",
                        "wollen",
                        "wollt",
                        "wollte",
                        "wollten",
                        "worden",
                        "wurde",
                        "würde",
                        "wurden",
                        "würden",
                        "x",
                        "y",
                        "z",
                        "z.b",
                        "zehn",
                        "zehnte",
                        "zehnten",
                        "zehnter",
                        "zehntes",
                        "zeit",
                        "zu",
                        "zuerst",
                        "zugleich",
                        "zum",
                        "zunächst",
                        "zur",
                        "zurück",
                        "zusammen",
                        "zwanzig",
                        "zwar",
                        "zwei",
                        "zweite",
                        "zweiten",
                        "zweiter",
                        "zweites",
                        "zwischen",
                        "zwölf",
                        "euer",
                        "eure",
                        "hattest",
                        "hattet",
                        "jedes",
                        "mußt",
                        "müßt",
                        "sollst",
                        "sollt",
                        "soweit",
                        "weshalb",
                        "wieso",
                        "woher",
                        "wohin"));
    }

    public static RecipeMagnitudeAnalyzerConfiguration createRecipeMagnitudeAnalyzerConfiguration() {
        return new RecipeMagnitudeAnalyzerConfiguration(
                toMap(
                        "eins", ONE,
                        "zwei", TWO,
                        "drei", THREE,
                        "vier", FOUR,
                        "fünf", FIVE,
                        "sechs", SIX,
                        "sieben", SEVEN,
                        "acht", EIGHT,
                        "neun", NINE,
                        "zehn", TEN));
    }

    public static RecipeMeasurementAnalyzerConfiguration createRecipeMeasurementAnalyzerConfiguration() {
        return new RecipeMeasurementAnalyzerConfiguration(UNIT_PIECE_DE, UNIT_GRAM)
                .setMeasurementUnitDefinitions(
                        UNIT_PIECE_DE,
                        UNIT_PIECE_STANGEL,
                        new MeasurementUnitDefinition("dose", UNIT_PIECE_DE, false, "dosen"),
                        //
                        UNIT_PINCH_DE,
                        //
                        new MeasurementUnitDefinition("el", UNIT_TBSP, true),
                        new MeasurementUnitDefinition("tl", UNIT_TSP, true),
                        //
                        UNIT_GRAM,
                        UNIT_LITER,
                        UNIT_MILILITER);
    }

    public static RecipeToolAnalyzerConfiguration createRecipeToolAnalyzerConfiguration() {
        return new RecipeToolAnalyzerConfiguration(
                RECIPE_TOOL_MATCHERS,
                toMapP(
                        //                                Pair.of(TOOL_MAIN, toRecipeInstructionMatcher("(?i)mixing bowl")),
                        //                                Pair.of(TOOL_UTENSIL, toRecipeInstructionMatcher("(?i)varoma dish|varoma tray|varoma|steamer|simmering basket|measuring cup")),
                        //                                Pair.of(TOOL_EXTERNAL, toRecipeInstructionMatcher("(?i)" + REGEX_TOOL_EXTERNAL)),
                        //
                        //                                Pair.of(OVEN_OPERATION, toRecipeInstructionMatcher("(?i)preheat.*oven|bake.*for")),
                        //                                Pair.of(REFRIGERATE, toRecipeInstructionMatcher("(?i)(?:keep|place|store).*(?:refrigerator|fridge|refrigerated)|^refrigerate")),
                        //
                        //                                Pair.of(FREEZE, toRecipeInstructionMatcher("(?i)(?:chill.*fridge|freeze.*(?:minutes|hour))")),
                        //
                        //                                Pair.of(SIMMERING_BASKET, toRecipeInstructionMatcher("(?i)using.*simmering basket.*(drain|strain)")),
                        //                                Pair.of(SIMMERING_BASKET_PUT_INSIDE, toRecipeInstructionMatcher("(?i)(?:insert).*simmering basket")),
                        //                                Pair.of(SIMMERING_BASKET_REMOVE, toRecipeInstructionMatcher("(?i)(?:remove).*simmering basket")),
                        //                                Pair.of(SIMMERING_BASKET_AS_LID_PUT, toRecipeInstructionMatcher("(?i)(?:placing|place|with) simmering basket (instead|in place) of measuring cup")), // Needs context analysis to
                        // place before tm step
                        //                                Pair.of(QUALIFIES_PREVIOUS, toRecipeInstructionMatcher("(?i)placing simmering basket instead of measuring")), // Needs context analysis to place before tm step
                        ////
                        ////                        Pair.of(RecipeAnalysisTag.SIMMERING_BASKET_AS_LID_REMOVE, defineMe()),
                        ////                        Pair.of(RecipeAnalysisTag.SIMMERING_BASKET_REMOVE_MEASURING_CUP_PUT, defineMe()),
                        //
                        //                                Pair.of(MIXING_BOWL, toRecipeInstructionMatcher("(?i)(?:remove).*mixing.*bowl lid")),
                        //                                Pair.of(MIXING_BOWL_PUT, toRecipeInstructionMatcher("(?i)place mixing bowl.*back into position")),
                        //                                Pair.of(MIXING_BOWL_REMOVE, toRecipeInstructionMatcher("(?i)(?:remove).*(?:mixing bowl(?!.*? lid))")),
                        //                                Pair.of(MIXING_BOWL_EMPTY, toRecipeInstructionMatcher("(?i)(?:empty).*mixing.*bowl")),
                        //                                Pair.of(MIXING_BOWL_CLEAN, toRecipeInstructionMatcher("(?i)(?:clean|rinse).*mixing.*bowl")),
                        //
                        //                                Pair.of(TRANSFER_FROM_BOWL, toRecipeInstructionMatcher("(?i)(?:transfer|pour.*over).*" + REGEX_LOCATION_EXTERNAL)),
                        //
                        ////                        Pair.of(RecipeAnalysisTag.MEASURING_CUP, defineMe()),
                        //                                Pair.of(MEASURING_CUP_NOTUSE, toRecipeInstructionMatcher("(?i)(?:remove|without).*measuring cup")),
                        //                                Pair.of(MEASURING_CUP_PUT, toRecipeInstructionMatcher("(?i)insert.*measuring cup|with measuring cup in place")),
                        //                                Pair.of(MEASURING_CUP_HOLD_VIBRATION, toRecipeInstructionMatcher("(?i)hold.*measuring cup")),
                        //
                        ////                        Pair.of(RecipeAnalysisTag.BUTTERFLY, defineMe()),
                        //                                Pair.of(WHISK_PUT, toRecipeInstructionMatcher("(?i)insert.*butterfly")),
                        //                                Pair.of(WHISK_REMOVE, toRecipeInstructionMatcher("(?i)remove.*butterfly")),
                        //
                        //                                Pair.of(SPATULA, toRecipeInstructionMatcher("(?i)with aid.*spatula")),
                        //                                Pair.of(SPATULA_MIX, toRecipeInstructionMatcher("(?i)mix with.*spatula")),
                        //                                Pair.of(SPATULA_SCRAP_SIDE, toRecipeInstructionMatcher("(?i)scrape.*bowl.*spatula")),
                        //                                Pair.of(SPATULA_MIX_WELL, toRecipeInstructionMatcher("(?i)mix.*well with.*spatula")),
                        //
                        //                                Pair.of(STEAMER_PUT, toRecipeInstructionMatcher("(?i)(?:place|return).*(?:varoma|steamer).*(?:to|in|in).*position")),
                        //                                Pair.of(STEAMER_REMOVE, toRecipeInstructionMatcher("(?i)remove.*(?:varoma|steamer)(?!( tray| dish))")),
                        //                                Pair.of(STEAMER_CLOSE, toRecipeInstructionMatcher("(?i)(?:secure|close).*(?:varoma|steamer)")),
                        //                                Pair.of(STEAMER_TRAY_PUT, toRecipeInstructionMatcher("(?i)insert.*(?:varoma|steamer).*tray")),
                        //                                Pair.of(STEAMER_TRAY_REMOVE, toRecipeInstructionMatcher("(?i)remove.*(?:varoma|steamer).*tray")),
                        //                                Pair.of(STEAMER_DISH_PUT, toRecipeInstructionMatcher("(?i)place.*into (varoma|steamer) dish|set varoma dish into position")),
                        //                                Pair.of(STEAMER_DISH_REMOVE, toRecipeInstructionMatcher("(?i)remove.*(varoma|steamer) dish")),
                        //
                        //                                Pair.of(OTHER_OPERATION, toRecipeInstructionMatcher("(?i)insert.*(?:blade cover)"))
                        ////                        Pair.of(RecipeAnalysisTag.USEFUL_ITEMS, defineMe()),
                        ////                                Pair.of(KITCHEN_EQUIPMENT, toRecipeInstructionMatcher())
                        ));
    }
}
