package com.recipeton.shared.analysis.service.filter;

/*-
 * #%L
 * recipeton-analysis
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.configuration.RecipeInstructionAnalyzerConfiguration;
import com.recipeton.shared.analysis.domain.RecipeIngredientAnalysis;
import com.recipeton.shared.analysis.service.*;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.recipeton.shared.util.StreamUtil.concat;

public class RecipeInstructionAnalysisFilterFactory {

    private final RecipeMagnitudeAnalyzer recipeMagnitudeAnalyzer;
    private final RecipeMeasurementAnalyzer recipeMeasurementAnalyzer;
    private final RecipeCommandAnalyzer recipeCommandAnalyzer;
    private final RecipeToolAnalyzer recipeToolAnalyzer;
    private final RecipeInstructionIngredientAnalyzer recipeInstructionIngredientAnalyzer;
    private final RecipeLangAnalyzer recipeLangAnalyzer;

    private final RecipeInstructionAnalyzerConfiguration configuration;

    public RecipeInstructionAnalysisFilterFactory(
            RecipeMagnitudeAnalyzer recipeMagnitudeAnalyzer,
            RecipeMeasurementAnalyzer recipeMeasurementAnalyzer,
            RecipeCommandAnalyzer recipeCommandAnalyzer,
            RecipeToolAnalyzer recipeToolAnalyzer,
            RecipeInstructionIngredientAnalyzer recipeInstructionIngredientAnalyzer,
            RecipeLangAnalyzer recipeLangAnalyzer,
            RecipeInstructionAnalyzerConfiguration configuration) {
        this.recipeMagnitudeAnalyzer = recipeMagnitudeAnalyzer;
        this.recipeMeasurementAnalyzer = recipeMeasurementAnalyzer;
        this.recipeCommandAnalyzer = recipeCommandAnalyzer;
        this.recipeToolAnalyzer = recipeToolAnalyzer;
        this.recipeInstructionIngredientAnalyzer = recipeInstructionIngredientAnalyzer;
        this.recipeLangAnalyzer = recipeLangAnalyzer;
        this.configuration = configuration;
    }

    public RecipeInstructionAnalysisFilter createRecipeIngredientRecipeInstructionAnalysisFilter(RecipeIngredientAnalysis recipeIngredientAnalysis, String... ingredientTokens) {
        return new IngredientRecipeInstructionAnalysisFilter(recipeIngredientAnalysis, recipeMagnitudeAnalyzer, recipeMeasurementAnalyzer, recipeInstructionIngredientAnalyzer, ingredientTokens);
    }

    public List<RecipeInstructionAnalysisFilter> createRecipeInstructionAnalysisPreFilters() {

        return concat(
                        configuration.getRecipeInstructionTagMatchers().entrySet().stream().map(e -> new RecipeInstructionTagAnalysisFilter(e.getKey(), e.getValue())),
                        recipeToolAnalyzer.getRecipeToolComposedActionTagMatchers().entrySet().stream().map(e -> new RecipeToolComposedActionAnalysisFilter(e.getKey(), e.getValue())),
                        recipeToolAnalyzer.getRecipeToolDefinitionMatchers().entrySet().stream().map(p -> new RecipeToolAnalysisFilter(p.getKey(), p.getValue())),
                        recipeCommandAnalyzer.getCommandDelimiters().stream().map(p -> new RecipeCommandAnalysisFilter(recipeCommandAnalyzer, p)),
                        Stream.of(
                                new ItemTransferAnalysisFilter(configuration),
                                new FocusByToolAnalysisFilter(),
                                new FocusByContextAnalysisFilter(),
                                new IngredientWeighPlacementPropagationAnalysisFilter(),
                                new IngredientAdditionTagAnalysisFilter(),
                                new IngredientEstimationAnalysisFilter(configuration, recipeLangAnalyzer),
                                new ParallelQualificationTagAnalysisFilter(),
                                new CloseMainCompartmentContextAnalysisFilter()))
                .collect(Collectors.toList());
    }
}
