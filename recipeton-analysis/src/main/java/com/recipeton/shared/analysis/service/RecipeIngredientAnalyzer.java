package com.recipeton.shared.analysis.service;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.configuration.RecipeIngredientAnalyzerConfiguration;
import com.recipeton.shared.analysis.domain.*;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.recipeton.shared.analysis.configuration.RecipeIngredientAnalyzerConfiguration.INGREDIENT_PREPARATION_SEPARATOR_SYMBOLS;
import static com.recipeton.shared.analysis.configuration.RecipeIngredientAnalyzerConfiguration.INGREDIENT_PREPARATION_START_SYMBOLS;
import static com.recipeton.shared.analysis.configuration.RecipeMagnitudeAnalyzerConfiguration.RANGE_SEPARATOR;
import static com.recipeton.shared.util.StreamUtil.toStream;

@RequiredArgsConstructor
public class RecipeIngredientAnalyzer {

    private final RecipeLangAnalyzer recipeLangAnalyzer;
    private final RecipeMagnitudeAnalyzer recipeMagnitudeAnalyzer;
    private final RecipeMeasurementAnalyzer recipeMeasurementAnalyzer;

    private final RecipeIngredientAnalyzerConfiguration configuration;

    public RecipeIngredientAnalysis analyze(String textSource) {
        if (StringUtils.isBlank(textSource)) {
            return null;
        }

        String ingredientAlternativeSeparatorToken = configuration.getIngredientAlternativeSeparatorTokenSet();
        int separatorIndex = ingredientAlternativeSeparatorToken == null ? -1 : StringUtils.indexOf(textSource, ingredientAlternativeSeparatorToken);
        if (separatorIndex == -1) {
            return analyzeSingleIngredient(textSource.trim());
        }
        return analyzeSingleIngredient(textSource.substring(0, separatorIndex).trim()).setAlternative(analyze(StringUtils.substringAfter(textSource, ingredientAlternativeSeparatorToken)));
    }

    public List<RecipeIngredientAnalysis> analyze(List<String> texts) {
        return toStream(texts).map(this::analyze).collect(Collectors.toList());
    }

    private RecipeIngredientAnalysis analyzeSingleIngredient(String textSource) { // NOPMD Complex

        String textNormalized = recipeLangAnalyzer.normalizeText(textSource);
        String textSourceFormatted = recipeLangAnalyzer.normalizeLanguage(textNormalized);

        String unitsFormatted = recipeMeasurementAnalyzer.formatRecipeMeasurementUnits(textSourceFormatted);
        String fractionFormatted = recipeMagnitudeAnalyzer.formatMagnitude(unitsFormatted);
        String formatted = fractionFormatted.replaceAll("/n", " ").trim();

        AnalyzeState state = new AnalyzeState(textSource, recipeLangAnalyzer.normalizeLanguage(formatted));

        while (!state.isDone()) {
            int tokenIndex = state.getTokenIndex();
            String token = state.next();
            if (tokenIndex == 0 && !state.isMagnitudeFromFound()) {
                BigDecimal magnitude = recipeMagnitudeAnalyzer.getMagnitude(token).orElse(null);
                if (magnitude != null) {
                    state.getRecipeIngredientAnalysis().setMagnitudeFrom(token).setMagnitudeFromValue(magnitude);
                    continue;
                }
            }

            if (tokenIndex == 1 && !state.isMagnitudeRangeSeparatorFound() && state.isMagnitudeFromFound() && RANGE_SEPARATOR.equals(token)) {
                state.setMagnitudeRangeSeparatorFound();
                continue;
            }

            if (tokenIndex == 2 && state.isMagnitudeRangeSeparatorFound()) {
                BigDecimal magnitude = recipeMagnitudeAnalyzer.getMagnitude(token).orElse(null);
                if (magnitude != null) {
                    state.getRecipeIngredientAnalysis().setMagnitudeTo(token).setMagnitudeToValue(magnitude);
                    continue;
                }
            }

            if (!state.isNotationParseStarted()) {
                //            if (state.isMagnitudeFromFound() && !state.isNotationParseStarted()) {
                MeasurementUnitDefinition measurementUnitDefinition = recipeMeasurementAnalyzer.findMeasurementUnitByText(token).orElse(null);
                if (measurementUnitDefinition != null) {
                    state.getRecipeIngredientAnalysis().setUnit(new RecipeMeasurementUnitAnalysis(token, measurementUnitDefinition, false)); // NOPMD
                    continue;
                }
            }

            if (!state.isUnitParsed() && !state.isNotationParseStarted() && (state.isMagnitudeFromFound() || state.isUnitPresent()) && recipeMeasurementAnalyzer.isUnitSeparator(token)) {
                state.setUnitParsed();
                continue;
            }

            // If notation has not started and preparation tokens are present. Qualifiers before notation)
            if (!state.isNotationParseStarted() && configuration.getIngredientPreparationTokens().contains(token)) {
                state.addPreparationBeforeNotation(token);
                continue;
            }

            // Parsing notation and find token (... apple [diced] ...) or word starting with symbol (... apple (two pieces) ...)
            if (!state.isNotationParseEnded()
                    && state.isNotationParseStarted()
                    && (configuration.getIngredientPreparationTokens().contains(token) || INGREDIENT_PREPARATION_START_SYMBOLS.stream().anyMatch(p -> StringUtils.startsWith(token, p)))) {
                state.setNotationParseEnded();
            }

            if (!state.isNotationParseEnded()) {
                String notationToken = token;
                for (String separator : INGREDIENT_PREPARATION_SEPARATOR_SYMBOLS) {
                    if (StringUtils.endsWith(token, separator)) {
                        String stripped = StringUtils.stripEnd(token, separator);
                        if (configuration.getIngredientPreparationTokens().contains(stripped)) { // ... pear [sliced,] ...
                            state.addPreparationAfterNotation(token);
                            notationToken = null;
                        } else { // ... [pear,] ....
                            notationToken = stripped;
                        }
                        state.setNotationParseEnded();
                        break;
                    }
                }
                state.doNotationAppend(notationToken);
                continue;
            }

            if (!state.isOptional() && configuration.getIngredientOptionalIndicatorTokens().contains(token)) {
                state.setOptional();
                continue;
            }

            state.addPreparationAfterNotation(token);
        }

        return state.doFinalize();
    }

    public List<RecipeIngredientAnalysis> findIngredientsByUnmatched(List<RecipeIngredientAnalysis> recipeIngredientAnalyses, List<RecipeInstructionAnalysis> recipeInstructionAnalyses) {
        Set<RecipeIngredientAnalysis> foundIngredientAnalyses =
                recipeInstructionAnalyses.stream()
                        .flatMap(ria -> ria.getTerms().stream())
                        .filter(t -> t instanceof RecipeIngredientTerm)
                        .map(t -> (RecipeIngredientTerm) t)
                        .map(RecipeIngredientTerm::getIngredient)
                        .collect(Collectors.toSet());

        return recipeIngredientAnalyses.stream()
                .filter(a -> !a.isOptional())
                .filter(a -> !configuration.getIngredientOnUnmatchedIgnoreNotations().contains(a.getIngredientNotation()))
                .filter(a -> !foundIngredientAnalyses.contains(a))
                .collect(Collectors.toList());
    }

    public final class AnalyzeState {

        private final List<String> preparationBeforeNotation = new ArrayList<>();
        private final List<String> preparationAfterNotation = new ArrayList<>();
        @Getter private final RecipeIngredientAnalysis recipeIngredientAnalysis;
        private final Deque<String> stack;
        private final int stackInitialSize;

        private final StringBuilder ingredientNotation = new StringBuilder(); // NOPMD it is ok

        @Getter private boolean magnitudeRangeSeparatorFound;
        @Getter private boolean unitParsed;
        @Getter private boolean notationParseEnded;

        AnalyzeState(String textSource, String text) {
            stack = Stream.of(configuration.getIngredientTokenSplitPattern().split(text)).collect(Collectors.toCollection(ArrayDeque::new));

            stackInitialSize = stack.size();

            recipeIngredientAnalysis = new RecipeIngredientAnalysis(textSource).setText(text);
        }

        public void addPreparationAfterNotation(String token) {
            this.preparationAfterNotation.add(token);
        }

        public void addPreparationBeforeNotation(String token) {
            preparationBeforeNotation.add(token);
        }

        public RecipeIngredientAnalysis doFinalize() {
            // CHECK if preparation has terms like "para engrasar" or "al gusto", create new boolean 'virtuallyOptional' and treat as optional when not found

            String notation = this.ingredientNotation.toString();
            String uid = recipeLangAnalyzer.toUid(notation);
            recipeIngredientAnalysis.setIngredientNotation(uid, notation);

            String preparation = null;
            if (!preparationBeforeNotation.isEmpty()) {
                preparation = String.join(", ", preparationBeforeNotation);
            }
            if (!preparationAfterNotation.isEmpty()) {
                String preparationAfter = String.join(" ", preparationAfterNotation);
                if (preparation == null) {
                    preparation = preparationAfter;
                } else {
                    preparation = preparation + ", " + preparationAfter;
                }
            }
            recipeIngredientAnalysis.setPreparation(preparation);

            if (recipeIngredientAnalysis.getUnit() == null) {
                recipeIngredientAnalysis.setUnit(recipeMeasurementAnalyzer.getDefaultRecipeMeasurementUnitAnalysis(recipeIngredientAnalysis.getMagnitudeFromValue()));
            }

            return recipeIngredientAnalysis;
        }

        private void doNotationAppend(String notationToken) {
            if (StringUtils.isBlank(notationToken)) {
                return;
            }
            if (StringUtils.isNotBlank(ingredientNotation)) {
                ingredientNotation.append(" ");
            }
            ingredientNotation.append(notationToken);
        }

        public int getTokenIndex() {
            return stackInitialSize - stack.size();
        }

        boolean isDone() {
            return stack.isEmpty();
        }

        public boolean isMagnitudeFromFound() {
            return recipeIngredientAnalysis.isMagnitudeFromPresent();
        }

        public boolean isNotationParseStarted() {
            return ingredientNotation.length() > 0;
        }

        public boolean isOptional() {
            return recipeIngredientAnalysis.isOptional();
        }

        public boolean isUnitPresent() {
            return recipeIngredientAnalysis.isUnitPresent();
        }

        public String next() {
            return stack.pop();
        }

        public void setMagnitudeRangeSeparatorFound() {
            this.magnitudeRangeSeparatorFound = true;
        }

        public void setNotationParseEnded() {
            notationParseEnded = true;
        }

        private void setOptional() {
            recipeIngredientAnalysis.setOptional(true);
        }

        public void setUnitParsed() {
            this.unitParsed = true;
        }
    }
}
