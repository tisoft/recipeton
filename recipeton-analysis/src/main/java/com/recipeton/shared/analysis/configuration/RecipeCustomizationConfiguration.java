package com.recipeton.shared.analysis.configuration;

/*-
 * #%L
 * recipeton-analysis
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.domain.RecipeDifficultyDefinition;
import com.recipeton.shared.analysis.domain.RecipePriceDefinition;
import com.recipeton.shared.domain.RecipeDefinition;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.apache.commons.lang3.StringUtils;

import java.util.Map;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.regex.Pattern;

import static com.recipeton.shared.util.TextUtil.normalizeAsciiFirstChar;
import static com.recipeton.shared.util.TextUtil.removeAll;
import static java.util.regex.Pattern.compile;

@Data
@Accessors(chain = true)
@ToString(onlyExplicitlyIncluded = true)
public class RecipeCustomizationConfiguration {
    private static final Pattern TITLE_QUOTES_PATTERN = compile("^'|'$|^\"|\"$");
    public static final Function<String, String> RECIPE_TITLE_FORMAT_FUNCTION_DEFAULT = s -> StringUtils.capitalize(removeAll(normalizeAsciiFirstChar(s), TITLE_QUOTES_PATTERN).trim().toLowerCase());

    private Function<String, String> recipeTitleFormatFunction = RECIPE_TITLE_FORMAT_FUNCTION_DEFAULT;
    private Function<String, String> instructionParallelFormatFunction = s -> s;

    private Map<RecipeDifficultyDefinition, Predicate<String>> recipeDifficultDefinitionPredicates;
    private Map<RecipePriceDefinition, Predicate<String>> recipePriceDefinitionPredicates;
    private Function<RecipeDefinition, String> recipeLocaleLabelFunction = RecipeDefinition::getLocale;

    private String measuringLidAndCupBody = "Place lid and measuring cup on top";

    private String backgroundReportOriginalTitleLabel = "Original title";
    private String backgroundReportAuthorLabel = "Author";
    private String backgroundReportSourceLabel = "Source";
    private String backgroundReportCollectionsLabel = "Collections";

    private String recipeAdditionUnclearStepSubtitle = "<b>Attention!</b> Check that...";

    private String recipeUnclearTitle = "<b>WARNING!</b> Possible ingredient mismatches!";
    private String recipeUnclearBody =
            "Some ingredients were not found in instructions.<br>" + "<b>Check manually!</b> recipe overview.<br>" + "<i>If possible. Correct recipe and reimport!</i><br><br>" + "<b>Problematic ingredients:</b><br>";

    public String getTextAsParallel(String text) {
        return instructionParallelFormatFunction.apply(text);
    }
}
