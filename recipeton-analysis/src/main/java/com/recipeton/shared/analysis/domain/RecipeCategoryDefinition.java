package com.recipeton.shared.analysis.domain;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import lombok.Data;
import org.apache.commons.lang3.StringUtils;

@Data
public class RecipeCategoryDefinition {

    public static final String RECIPE_CATEGORY_PRINCIPAL_UID_PREFIX = "0_";
    public static final String RECIPE_CATEGORY_NAME_EXTRA_PREFIX = "#";

    public static final String UID_BASIC = "000basic";
    public static final String UID_START = "010start";
    public static final String UID_SOUP = "020soup";
    public static final String UID_PASTARICE = "030pastarice";
    public static final String UID_MAINMEAT = "040mainmeat";
    public static final String UID_MAINFISH = "050mainfish";
    public static final String UID_MAINVEG = "060mainveg";
    public static final String UID_MAINOTHER = "070mainother";
    public static final String UID_SIDE = "080side";
    public static final String UID_BAKE = "090bake";
    public static final String UID_BAKE_SAVORY = "100bakesavory";
    public static final String UID_DESSERT = "110dessert";
    public static final String UID_BAKE_SWEET = "120bakesweet";
    public static final String UID_SAUCE_SWEET = "130saucesweet";
    public static final String UID_DRINK = "140drink";
    public static final String UID_SNACK = "150snack";
    public static final String UID_SAUCE_SAVO = "160saucesavory";
    public static final String UID_BABY = "170baby";
    public static final String UID_BREAK = "180break";
    public static final String UID_MENU = "190menu";

    private final String uid;
    private final String name;

    public static String createRecipeCategoryUidByPrincipal(String uidPart) {
        return RECIPE_CATEGORY_PRINCIPAL_UID_PREFIX + uidPart;
    }

    public static boolean isRecipeCategoryUidExtra(String uid) {
        return StringUtils.startsWith(uid, RECIPE_CATEGORY_NAME_EXTRA_PREFIX);
    }

    public static boolean isRecipeCategoryUidPrincipal(String uid) {
        return StringUtils.startsWith(uid, RECIPE_CATEGORY_PRINCIPAL_UID_PREFIX);
    }

    public boolean isCurated() {
        return isRecipeCategoryUidPrincipal(uid);
    }
}
