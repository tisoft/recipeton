package com.recipeton.shared.analysis.service.filter;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.domain.RecipeInstructionAnalysis;
import com.recipeton.shared.analysis.domain.RecipeInstructionMatch;
import com.recipeton.shared.analysis.domain.RecipeInstructionTag;
import com.recipeton.shared.analysis.domain.RecipeInstructionTerm;

import static com.recipeton.shared.analysis.domain.RecipeInstructionTag.*;
import static com.recipeton.shared.analysis.domain.RecipeToolComposedActionTag.MIXING_BOWL_PUT;

public class IngredientWeighPlacementPropagationAnalysisFilter implements RecipeInstructionAnalysisFilter {

    private static final String QUERY = "isPrevOnTopOfCompartment?";

    private void addTerm(RecipeInstructionAnalysis recipeInstructionAnalysis, RecipeInstructionTag tag) {
        recipeInstructionAnalysis.addTerm(new RecipeInstructionTerm(tag, RecipeInstructionMatch.ofContext(QUERY)));
    }

    @Override
    public RecipeInstructionAnalysis apply(RecipeInstructionAnalysis recipeInstructionAnalysis) { // NOPMD BS Complex
        // When Utensils are declared and matched then this should check if LOCATION_EXTERNAL is UTENSIL that is LOCATION_ON_TOP (from context) and add LOCATION_ON_TOP in that case
        if (!recipeInstructionAnalysis.isTagPresent(DO_WEIGH) || recipeInstructionAnalysis.isTagsAnyPresent(FOCUS_ON_TOP, FOCUS_MAIN)) {
            return recipeInstructionAnalysis;
        }

        // Think about looking further back... will need to check also for REMOVE operations then
        RecipeInstructionAnalysis prev = recipeInstructionAnalysis.getPrev();
        if (prev != null && prev.isTagPresent(MIXING_BOWL_PUT)) {
            addTerm(recipeInstructionAnalysis, FOCUS_ON_TOP);
            return recipeInstructionAnalysis;
        }

        while (prev != null) {
            if (prev.isTagPresent(FOCUS_ON_TOP)) {
                addTerm(recipeInstructionAnalysis, FOCUS_ON_TOP);
                return recipeInstructionAnalysis;
            }
            if (prev.isTagPresent(FOCUS_MAIN)) {
                addTerm(recipeInstructionAnalysis, FOCUS_MAIN);
                return recipeInstructionAnalysis;
            }
            if (prev.isTagsAnyPresent(TAGS_DO_ADDITION)) { // cut exploration
                return recipeInstructionAnalysis;
            }
            prev = prev.getPrev();
        }
        return recipeInstructionAnalysis;
    }
}
