package com.recipeton.shared.analysis.configuration.cooki;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.configuration.*;
import com.recipeton.shared.analysis.domain.*;
import com.recipeton.shared.analysis.service.RecipeInstructionSplitter;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;

import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.recipeton.shared.analysis.configuration.RecipeCategoryAnalyzerConfiguration.FUNCTION_REMOVE_PARENTHESES_AND_CAPITALIZE_FIRST;
import static com.recipeton.shared.analysis.configuration.RecipeMagnitudeAnalyzerConfiguration.*;
import static com.recipeton.shared.analysis.configuration.cooki.CookiConstants.*;
import static com.recipeton.shared.analysis.domain.MeasurementUnitDefinition.*;
import static com.recipeton.shared.analysis.domain.RecipeCategoryDefinition.*;
import static com.recipeton.shared.analysis.domain.RecipeCommandProgram.*;
import static com.recipeton.shared.analysis.domain.RecipeInstructionRegexMatcher.toRecipeInstructionMatcher;
import static com.recipeton.shared.analysis.domain.RecipeInstructionTag.*;
import static com.recipeton.shared.analysis.domain.RecipeToolComposedActionTag.*;
import static com.recipeton.shared.analysis.domain.RecipeToolTag.*;
import static com.recipeton.shared.analysis.service.RecipeInstructionSplitter.DO_NOT_SPLIT;
import static com.recipeton.shared.analysis.service.RecipeInstructionSplitter.Replacement.toNext;
import static com.recipeton.shared.util.MapUtil.toMap;
import static com.recipeton.shared.util.MapUtil.toMapP;
import static com.recipeton.shared.util.StreamUtil.concat;
import static com.recipeton.shared.util.TextUtil.*;
import static java.util.regex.Pattern.compile;
import static org.apache.commons.lang3.StringUtils.containsIgnoreCase;

public class RecipeAnalyzerCookiFrConfiguration extends RecipeAnalyzerConfiguration { // NOPMD Long
    public static final MeasurementUnitDefinition UNIT_PIECE_FR = new MeasurementUnitDefinition("piece", UNIT_PIECE, false, "morceau");

    public static final String COOKI_FR_ACTION_DEFAULT = "régler";

    public static final String[] COOKI_FR_ACTION_REGEXES = {
        COOKI_FR_ACTION_DEFAULT,
        "battre",
        "blanchir",
        "casser le blanc",
        "chauffe[rz]",
        "compote[rz]",
        "concasse[rz]",
        "cuire",
        "dore[rz]",
        "dilue[rz]",
        "dissoudre",
        "égoutte[rz]",
        "émieter",
        "émulsifier",
        "émulsionne[rz]",
        "fondre",
        "fouette[rz]",
        "frire",
        "frirez",
        "hache[rz]",
        "lisser",
        "laisse[rz]",
        "mijote[rz]",
        "mixe[rz]",
        "monter", // monter le blanc
        "moudre",
        "mélange[rz]",
        "ondre",
        "piler",
        "porter à ébullition",
        "pulvériser",
        "pétrir",
        "revenir",
        "rissoler",
        "râper",
        "répartir",
        "réchauffer",
        "réduire",
        "suer",

        // --
        "ajoute[rz]",
        "dépose[rz]",
        "enfourne[rz]",
        "fermer",
        "insére[rz]",
        "Incorpore[rz]",
        "mettre",
        "mettez",
        "nettoye[rz]",
        "pese[rz]",
        "place[rz]",
        "pose[rz]",
        "racler",
        "remue[rz]",
        "remettre",
        "remettez",
        "rince[rz]",
        "réfrigére[rz]",
        "tempérer",
        "transvase[rz]",
        "verse[rz]",
        "vide[rz]",
        "étale[rz]",
        "ôte[rz]",

        // ---
        "réserve[rz]",
        "poursuivre la cuisson",
        "prolonger la cuisson",
        "tourner le sélecteur de vitesse"
    };
    public static final Set<String> COOKI_FR_INGREDIENT_PREPARATION_TOKENS =
            Set.of(
                    // Complete!!!!!
                    "ajuster",
                    "battu",
                    "bien",
                    "brossés",
                    "ciselée",
                    "ciselées",
                    "ciselés",
                    "concassées",
                    "confit",
                    "congelé",
                    "corsé",
                    "coupé",
                    "coupée",
                    "coupées",
                    "coupés",
                    "courtes",
                    "cuite",
                    "cuits",
                    "discrétion",
                    "décortiquées",
                    "découpé",
                    "dégraissé",
                    "dénoyautées",
                    "dénoyautés",
                    "désarêté",
                    "désossées",
                    "détaillées",
                    "effeuillé",
                    "effilées",
                    //                        "en boîte",
                    //                        "en grains",
                    //                        "en morceaux",
                    "en", // en conserve
                    "entier",
                    "entières",
                    "fendue",
                    "fendus",
                    "finement",
                    "frais",
                    "fraîche",
                    "fraîchement",
                    "froid",
                    "froide",
                    "fumés",
                    "grises",
                    "hachée",
                    "liquide",
                    "longues",
                    "légèrement",
                    "min.",
                    "mou",
                    "moulu",
                    "moyen",
                    "moyenne",
                    "moyens",
                    "nature",
                    "nettoyée",
                    "parfumées",
                    "partiellement",
                    "pelé",
                    "pluches",
                    "plus",
                    "pour",
                    "pressé",
                    "préalablement",
                    "prêts",
                    "ramolli",
                    "rassis",
                    "rincées",
                    "rincés",
                    "râpé",
                    "réhydratée",
                    "réhydratés",
                    "sans",
                    "saumonée",
                    "sec",
                    "seulement",
                    "spéciale",
                    "séparée",
                    "séparés",
                    "tartiner",
                    "température",
                    "très",
                    "type",
                    "vidée",
                    "à",
                    "écalé",
                    "écossés",
                    "égoutté",
                    "égouttées",
                    "égouttés",
                    "émietté",
                    "émiettée",
                    "émondées",
                    "épaisse",
                    "épluché",
                    "épluchées",
                    "épépiné",
                    "épépinés",
                    "équeuté",
                    "équeutée",
                    "équeutées",
                    "équeutés");
    private static final Map<RecipeToolDefinition, RecipeInstructionMatcher> RECIPE_TOOL_MATCHERS =
            toMapP(
                    Pair.of(new RecipeToolDefinition("bol", true, TOOL_MAIN), toRecipeInstructionMatcher("(?i)\\bbol\\b")),
                    Pair.of(new RecipeToolDefinition("varoma", true, TOOL_INTERNAL), toRecipeInstructionMatcher("(?i)\\bvaroma\\b")),
                    Pair.of(new RecipeToolDefinition("panier cuisson", true, TOOL_INTERNAL), toRecipeInstructionMatcher("(?i)\\bpanier cuisson\\b")),
                    Pair.of(new RecipeToolDefinition("plateau vapeur", true, TOOL_INTERNAL), toRecipeInstructionMatcher("(?i)\\bplateau vapeur\\b")),
                    Pair.of(new RecipeToolDefinition("gobelet doseur", false, TOOL_INTERNAL), toRecipeInstructionMatcher("(?i)\\bgobelet doseur\\b")),
                    Pair.of(new RecipeToolDefinition("fouet", false, TOOL_INTERNAL), toRecipeInstructionMatcher("(?i)\\bfouet\\b")),
                    Pair.of(new RecipeToolDefinition("spatule", false, TOOL_INTERNAL), toRecipeInstructionMatcher("(?i)\\bspatule\\b")),
                    Pair.of(new RecipeToolDefinition("four", true, TOOL_EXTERNAL), toRecipeInstructionMatcher("(?i)\\bfour\\b")),
                    Pair.of(new RecipeToolDefinition("réfrigérateur", true, TOOL_EXTERNAL), toRecipeInstructionMatcher("(?i)\\bréfrigérateur\\b")),
                    Pair.of(new RecipeToolDefinition("plan de travail", true, TOOL_EXTERNAL), toRecipeInstructionMatcher("(?i)\\bplan de travail\\b")),
                    Pair.of(new RecipeToolDefinition("carafe", true, TOOL_EXTERNAL), toRecipeInstructionMatcher("(?i)\\bcarafe[s]?\\b")),
                    Pair.of(new RecipeToolDefinition("casserole", true, TOOL_EXTERNAL), toRecipeInstructionMatcher("(?i)\\bcasserole[s]?\\b")),
                    Pair.of(new RecipeToolDefinition("cadre", true, TOOL_EXTERNAL), toRecipeInstructionMatcher("(?i)\\bcadre[s]?\\b")),
                    Pair.of(new RecipeToolDefinition("cercle", true, TOOL_EXTERNAL), toRecipeInstructionMatcher("(?i)\\bcercle[s]?\\b")),
                    Pair.of(new RecipeToolDefinition("cocotte", true, TOOL_EXTERNAL), toRecipeInstructionMatcher("(?i)\\bcocotte[s]?\\b")),
                    Pair.of(new RecipeToolDefinition("compotier", true, TOOL_EXTERNAL), toRecipeInstructionMatcher("(?i)\\bcompotier[s]?\\b")),
                    Pair.of(new RecipeToolDefinition("fond de tarte", true, TOOL_EXTERNAL), toRecipeInstructionMatcher("(?i)\\bfond de tarte\\b")),
                    Pair.of(new RecipeToolDefinition("coupe", true, TOOL_EXTERNAL), toRecipeInstructionMatcher("(?i)\\bcoupe[s]?\\b")),
                    Pair.of(new RecipeToolDefinition("moule", true, TOOL_EXTERNAL), toRecipeInstructionMatcher("(?i)\\bmoule[s]?\\b")),
                    Pair.of(new RecipeToolDefinition("plaque", true, TOOL_EXTERNAL), toRecipeInstructionMatcher("(?i)\\bplaque[s]?\\b")),
                    Pair.of(new RecipeToolDefinition("plat", true, TOOL_EXTERNAL), toRecipeInstructionMatcher("(?i)\\bplat[s]?\\b")),
                    Pair.of(new RecipeToolDefinition("pichet", true, TOOL_EXTERNAL), toRecipeInstructionMatcher("(?i)\\bpichet[s]?\\b")),
                    Pair.of(new RecipeToolDefinition("poche à douille", true, TOOL_EXTERNAL), toRecipeInstructionMatcher("(?i)\\bpoche à douille\\b")),
                    Pair.of(new RecipeToolDefinition("poéle", true, TOOL_EXTERNAL), toRecipeInstructionMatcher("(?i)\\bpoéle[s]?\\b")),
                    Pair.of(new RecipeToolDefinition("pot", true, TOOL_EXTERNAL), toRecipeInstructionMatcher("(?i)\\bpot[s]?\\b")),
                    Pair.of(new RecipeToolDefinition("récipient", true, TOOL_EXTERNAL), toRecipeInstructionMatcher("(?i)\\brécipient[s]?\\b")),
                    Pair.of(new RecipeToolDefinition("ramequin", true, TOOL_EXTERNAL), toRecipeInstructionMatcher("(?i)\\bramequin[s]?\\b")),
                    Pair.of(new RecipeToolDefinition("saucière", true, TOOL_EXTERNAL), toRecipeInstructionMatcher("(?i)\\bsaucière[s]?\\b")),
                    Pair.of(new RecipeToolDefinition("saladier", true, TOOL_EXTERNAL), toRecipeInstructionMatcher("(?i)\\bsaladier[s]?\\b")),
                    Pair.of(new RecipeToolDefinition("verre", true, TOOL_EXTERNAL), toRecipeInstructionMatcher("(?i)\\bverre[s]?\\b")),
                    Pair.of(new RecipeToolDefinition("wok", true, TOOL_EXTERNAL), toRecipeInstructionMatcher("(?i)\\bwok[s]?\\b")));

    private static final String REGEX_TOOL_EXTERNAL_CONTAINER = RecipeToolAnalyzerConfiguration.getCombinedRegex(RECIPE_TOOL_MATCHERS.entrySet().stream().filter(e -> e.getKey().isContainer() && e.getKey().getTag().equals(TOOL_EXTERNAL)));
    public static final String REGEX_ITEM_TRANSFER_NEXUS = "\\b(?:dans|sur)\\b";
    private static final String REGEX_FOCUS_EXTERNAL_CONTAINER = REGEX_ITEM_TRANSFER_NEXUS + ".*(?:" + REGEX_TOOL_EXTERNAL_CONTAINER + ")";

    private static final String REGEX_PARALLEL = "pendant ce temps";
    private static final Pattern PARALLEL_TEXT_PATTERN = compile("(?i)" + REGEX_PARALLEL + "[,]? ");

    private static final String REGEX_SANS_GOBELET_DOSEUR = "(?:sans|retirer|retirez|Ôter|ôter).*gobelet doseur";

    public RecipeAnalyzerCookiFrConfiguration() {
        super(
                "COOKI_FR",
                rd -> containsIgnoreCase(rd.getLocale(), "fr") && containsIgnoreCase(rd.getSource(), "cooki"),
                createRecipeLangAnalyzerConfiguration(),
                createRecipeCategoryAnalyzerConfiguration(),
                createRecipeCommandAnalyzerConfiguration(),
                createRecipeMagnitudeAnalyzerConfiguration(),
                createRecipeMeasurementAnalyzerConfiguration(),
                createRecipeInstructionAnalyzerConfiguration(),
                createRecipeIngredientAnalyzerConfiguration(),
                createRecipeToolAnalyzerConfiguration(),
                createRecipeCustomizationConfiguration());
    }

    public static Function<String, String> createCookiFrInstructionPreprocessor() {
        return new Function<>() {
            private static final String SEPARATORS_BASE = " et(?:,| puis| y| faire)? |[,]? puis(?: le| l'| faire)? |[,]? faire |[,]? faites |[,]? en ";
            private static final String REGEX_ACTION_PREPARATIONS = "(?<prep>(?:" + REGEX_SANS_GOBELET_DOSEUR + "))";

            private final String regexSentenceSeparators = "(?<sep>[,]?" + SEPARATORS_BASE + "|(?<!" + REGEX_PARALLEL + "), )";

            private final String regexParallel = "(?<par>" + REGEX_PARALLEL + ")";
            private final Pattern separatorParallelPattern = compile("(?i)" + regexSentenceSeparators + regexParallel);

            private final String regexActions = "(?<act>" + String.join("|", COOKI_FR_ACTION_REGEXES) + ")";

            private final Pattern separatorCommandWithoutActionPattern = compile("(?i)" + "(?<sep>[,]?" + SEPARATORS_BASE + ")" + REGEX_COMMAND);
            private final Pattern beginCommandWithoutActionPattern = compile("(?i)" + REGEX_SENTENCE_BEGIN + REGEX_COMMAND);

            private final Pattern commandPreparationPattern = compile("(?i)" + REGEX_COMMAND + "[,]? " + REGEX_ACTION_PREPARATIONS);

            private final Pattern separatorPreparationActionPattern = compile("(?i)" + regexSentenceSeparators + REGEX_ACTION_PREPARATIONS + "[,]? " + regexActions);
            private final Pattern separatorActionPreparationPattern = compile("(?i)" + regexSentenceSeparators + regexActions + "[,]? " + REGEX_ACTION_PREPARATIONS + "[,]?");
            private final Pattern beginPreparationActionPattern = compile("(?i)" + REGEX_SENTENCE_BEGIN + REGEX_ACTION_PREPARATIONS + "[,]? " + regexActions);
            private final Pattern beginActionPreparationPattern = compile("(?i)" + REGEX_SENTENCE_BEGIN + regexActions + "[,]? " + REGEX_ACTION_PREPARATIONS + "[,]?");

            private final Pattern separatorActionPattern = compile("(?i)" + regexSentenceSeparators + regexActions);

            private final Pattern actionsAfterDotPattern = compile("(?i)(?<=\\. )" + regexActions);
            private final Pattern parallelAfterDotPattern = compile("(?i)(?<=\\. )" + regexParallel);
            private final Pattern preparationsAfterDotPattern = compile("(?i)(?<=\\. )" + REGEX_ACTION_PREPARATIONS);

            @Override
            public String apply(String text) {
                String preprocessed = text;

                preprocessed = separatorParallelPattern.matcher(preprocessed).replaceAll(". ${par}");

                preprocessed = separatorCommandWithoutActionPattern.matcher(preprocessed).replaceAll("${sep}" + COOKI_FR_ACTION_DEFAULT + " ${cmd}");
                preprocessed = beginCommandWithoutActionPattern.matcher(preprocessed).replaceAll("${sep}" + COOKI_FR_ACTION_DEFAULT + " ${cmd}");

                preprocessed = commandPreparationPattern.matcher(preprocessed).replaceAll("${prep} ${cmd}");

                preprocessed = separatorPreparationActionPattern.matcher(preprocessed).replaceAll(". ${prep}. ${act}");
                preprocessed = separatorActionPreparationPattern.matcher(preprocessed).replaceAll(". ${prep}. ${act}");
                preprocessed = beginPreparationActionPattern.matcher(preprocessed).replaceAll("${prep}. ${act}");
                preprocessed = beginActionPreparationPattern.matcher(preprocessed).replaceAll("${prep}. ${act}");

                preprocessed = separatorActionPattern.matcher(preprocessed).replaceAll(". ${act}");

                preprocessed = actionsAfterDotPattern.matcher(preprocessed).replaceAll(mr -> StringUtils.capitalize(mr.group()));
                preprocessed = preparationsAfterDotPattern.matcher(preprocessed).replaceAll(mr -> StringUtils.capitalize(mr.group()));
                preprocessed = parallelAfterDotPattern.matcher(preprocessed).replaceAll(mr -> StringUtils.capitalize(mr.group()));

                return preprocessed;
            }
        };
    }

    public static RecipeCategoryAnalyzerConfiguration createRecipeCategoryAnalyzerConfiguration() {
        return new RecipeCategoryAnalyzerConfiguration()
                .addRecipeCategoryByPrincipal(UID_BASIC, "Basiques", "Les basiques")
                .addRecipeCategoryByPrincipal(UID_START, "Entrées", "Entrées et salades")
                .addRecipeCategoryByPrincipal(UID_SOUP, "Soupes")
                .addRecipeCategoryByPrincipal(UID_PASTARICE, "Pâtes et riz")
                .addRecipeCategoryByPrincipal(UID_MAINMEAT, "Viande et poulet", "Plat principal - Viandes", "Plat principal - Viande et poulet")
                .addRecipeCategoryByPrincipal(UID_MAINFISH, "Poissons et fruits de mer", "Plat principal - Poissons et fruits de mer", "Poissons", "Fruits de mer")
                .addRecipeCategoryByPrincipal(UID_MAINVEG, "Végétarien", "Plat principal - Végétariens")
                .addRecipeCategoryByPrincipal(UID_MAINOTHER, "Autre principal", "Plat principal", "Plat principal - Autres")
                .addRecipeCategoryByPrincipal(UID_SIDE, "Accompagnements")
                .addRecipeCategoryByPrincipal(UID_BAKE, "Pains et rouleaux", "Pains et viennoiseries")
                .addRecipeCategoryByPrincipal(UID_BAKE_SAVORY, "Tartes, quiches et pizzas", "Quiches, pizzas et cakes", "Tartes, quiches et snacks")
                .addRecipeCategoryByPrincipal(UID_DESSERT, "Desserts et confiseries", "Desserts et sucreries", "Entremets et glaces")
                .addRecipeCategoryByPrincipal(UID_BAKE_SWEET, "Pâtisserie, gâteaux et tartes", "Pâtisseries et tartes sucrées", "Gâteaux et biscuits")
                .addRecipeCategoryByPrincipal(UID_SAUCE_SWEET, "Confitures, coulis et pâtes à tartiner", "Pâtes à tartiner", "Coulis", "Confitures")
                .addRecipeCategoryByPrincipal(UID_DRINK, "Boissons")
                .addRecipeCategoryByPrincipal(UID_SNACK, "Snacks", "En-cas", "Apéritifs, mises en bouche et snacks")
                .addRecipeCategoryByPrincipal(UID_SAUCE_SAVO, "Sauces, dips et confitures, sucré", "Sauces, dips et tartinades", "Sauces, dips et tartinades, salé", "Sauces, crèmes et tartinades", "Sauces et tartinades")
                .addRecipeCategoryByPrincipal(UID_BABY, "Recettes pour bébés", "Recettes pour les tout-petits")
                .addRecipeCategoryByPrincipal(UID_BREAK, "Petit Déjeuner", "Petits-déjeuners")
                .addRecipeCategoryByPrincipal(UID_MENU, "Menus")
                .setRecipeCategoryNamePreprocessor(
                        new Function<>() {
                            private final Pattern cleanupPattern = compile("(?i)" + String.join("|", "^Le ", "^Cuisiner ", "Rapide"));

                            @Override
                            public String apply(String s) {
                                return FUNCTION_REMOVE_PARENTHESES_AND_CAPITALIZE_FIRST.apply(removeAll(s, cleanupPattern));
                            }
                        });
    }

    public static RecipeCommandAnalyzerConfiguration createRecipeCommandAnalyzerConfiguration() {
        return new RecipeCommandAnalyzerConfiguration()
                .setCommandProgramPredicates(
                        toMap(
                                DOUGH, stringPredicatePatterns("(?i)" + TM_DOUGH + "|dough"),
                                FERMENT, stringPredicatePatterns("(?i)ferment"),
                                //                        HEAT_ONLY, stringPredicatePatterns("(?i)calentar"),
                                //                        SOUS_VIDE, stringPredicatePatterns("(?i)Al vacio"),
                                TURBO, stringPredicatePatterns("(?i)turbo")))
                .setCommandDurationPredicates(toMap(RecipeCommandAnalyzerConfiguration.HALF_SECOND, stringPredicatePatterns("0\\.5 sec", "0,5 sec")))
                .setCommandDurationHoursPattern(compile("^([0-9]+) h"))
                .setCommandDurationMinutesPattern(compile("^([0-9]+) min"))
                .setCommandDurationSecondsPattern(compile("^([0-9]+)(?:,0)? sec"))
                .setCommandTemperaturePredicates(toMap(STEAMER_TEMPERATURE, stringPredicatePatterns("(?i)varoma|steam")))
                .setCommandTemperaturePattern(compile("^([0-9]+)°C"))
                .setCommandSpeedPredicates(toMap(COMMAND_SPEED_SLOW_VALUE, stringPredicatePatterns("(?i)vitesse " + TM_SLOW + "|vitesse slow")))
                .setCommandRotationReversePredicates(stringPredicatePatterns("(?i)" + TM_REVERSE + "|reverse"))
                .setCommandSpeedPattern(compile("vitesse ([//.0-9]+)"))
                .setCommandRepetitionFromPattern(compile("^([0-9]+) (?:fois)"))
                .setCommandRepetitionFromToPattern(compile("^([0-9]+)-([0-9]+) fois"));
    }

    public static RecipeCustomizationConfiguration createRecipeCustomizationConfiguration() {
        return new RecipeCustomizationConfiguration()
                .setMeasuringLidAndCupBody("Insérer le couvercle et le gobelet doseur")
                .setBackgroundReportOriginalTitleLabel("Titre original")
                .setBackgroundReportAuthorLabel("Auteur")
                .setBackgroundReportCollectionsLabel("Collections")
                .setBackgroundReportSourceLabel("Source")
                .setRecipeAdditionUnclearStepSubtitle("<b>Attention!</b> Vérifiez que...")
                .setRecipeUnclearTitle("<b>ATTENCION!</b> Recette douteuse!")
                .setRecipeUnclearBody(
                        "Certains ingrédients n'ont pas été trouvés dans les instructions.<br>"
                                + "<b>Vérifier manuellement</b> lorsque vous devez mettre des ingrédients dans la recette.<br>"
                                + "<i>Corrigez la recette si possible!</i><br><br>"
                                + "<b>Ingrédients douteux:</b>")
                .setRecipeDifficultDefinitionPredicates(
                        toMap(
                                RecipeDifficultyDefinition.EASY, stringPredicatePattern("(?i)facile"),
                                RecipeDifficultyDefinition.MEDIUM, stringPredicatePattern("(?i)moyen"),
                                RecipeDifficultyDefinition.ADVANCED, stringPredicatePattern("(?i)difficile")))
                .setRecipePriceDefinitionPredicates(toMap(RecipePriceDefinition.LOW, stringPredicatePattern("(?i)economique|bon marché")))
                .setInstructionParallelFormatFunction(s -> StringUtils.capitalize(replaceAll(s, PARALLEL_TEXT_PATTERN, "")));
    }

    public static RecipeIngredientAnalyzerConfiguration createRecipeIngredientAnalyzerConfiguration() {
        return new RecipeIngredientAnalyzerConfiguration("ou ")
                .setIngredientTokenSplitPattern(compile(" |d'"))
                .setIngredientPreparationTokens(COOKI_FR_INGREDIENT_PREPARATION_TOKENS)
                .setIngredientOptionalIndicatorTokens(Set.of("(facultatif)", "(optionnel)", "à ajuster en fonction des goûts"))
                .setIngredientOnUnmatchedIgnoreNotations(
                        Set.of(
                                "sel", // Will need match on Salee
                                "poivre" // Poivree
                                // eau => humecter
                                ))
                .setIngredientForbiddenRegexes(
                        concat(
                                        Stream.of(COOKI_FR_ACTION_REGEXES), Stream.of("place") // specific conflictive words glace - place
                                        )
                                .collect(Collectors.toSet()));
    }

    public static RecipeInstructionAnalyzerConfiguration createRecipeInstructionAnalyzerConfiguration() {
        return new RecipeInstructionAnalyzerConfiguration(
                        Stream.of(
                                        toNext(".", ""), // REPLACE ALL with simple regex split implementation "(?<!\\bapprox|\\benv)\\."
                                        toNext("(approx.", DO_NOT_SPLIT), // Careful. Due to limitations all need to start from space or punctuation
                                        toNext(" approx.", DO_NOT_SPLIT), // Careful. Due to limitations all need to start from space or punctuation
                                        toNext("(env.", DO_NOT_SPLIT), // Careful. Due to limitations all need to start from space or punctuation
                                        toNext(" env.", DO_NOT_SPLIT) // Careful. Due to limitations all need to start from space or punctuation
                                        )
                                .toArray(RecipeInstructionSplitter.Replacement[]::new),
                        toMapP(
                                // COMPLETE!!!
                                Pair.of(DO_ADD, toRecipeInstructionMatcher("(?i)\\bajoute[rz]|incorpore[rz]\\b")),
                                Pair.of(DO_PLACE, toRecipeInstructionMatcher("(?i)\\b(?:mettre|mettez|verse[rz]|dépose[rz]|pose[rz]|place[rz])\\b")),
                                Pair.of(DO_READD, toRecipeInstructionMatcher("(?i)\\b(?:remettre|remetez)\\b")),
                                Pair.of(DO_WEIGH, toRecipeInstructionMatcher("(?i)\\bpese[rz]\\b")),
                                Pair.of(DO_SERVE, toRecipeInstructionMatcher("(?i)\\b(?:serve[rz]|servir|déguster|dégustez)\\b")),
                                Pair.of(DO_PREHEAT, toRecipeInstructionMatcher("(?i)\\b(?:allume[rz]|préchauffe[rz])\\b")),
                                Pair.of(FOCUS_MAIN, toRecipeInstructionMatcher("(?i)dans le bol")),
                                Pair.of(FOCUS_ON_TOP, toRecipeInstructionMatcher("(?i)sur le couvercle du bol")),
                                Pair.of(FOCUS_EXTERNAL, toRecipeInstructionMatcher("(?i)" + REGEX_FOCUS_EXTERNAL_CONTAINER)),
                                Pair.of(FOCUS_UNDEFINED, toRecipeInstructionMatcher("(?i)^réserve[rz]$")),

                                //                        Pair.of(RecipeAnalysisTag.ALL_INGREDIENTS, toRecipeInstructionMatcher()),
                                Pair.of(INGREDIENTS_ALL_REMAINING, toRecipeInstructionMatcher("(?i)le reste")),
                                Pair.of(IS_USE_RESERVED, toRecipeInstructionMatcher("(?i)réservé")),
                                Pair.of(SEQUENCE_PARALLEL, toRecipeInstructionMatcher("(?i)" + REGEX_PARALLEL))),
                        compile("[ ]?(?:,|\\bet\\b)[ ]?"))
                .setInstructionPreprocessor(createCookiFrInstructionPreprocessor())
                .setRecipeInstructionTransferNexus(compile(REGEX_ITEM_TRANSFER_NEXUS));
    }

    public static RecipeLangAnalyzerConfiguration createRecipeLangAnalyzerConfiguration() {
        return new RecipeLangAnalyzerConfiguration(
                        Set.of(
                                "a",
                                "à",
                                "â",
                                "abord",
                                "afin",
                                "ah",
                                "ai",
                                "aie",
                                "ainsi",
                                "allaient",
                                "allo",
                                "allô",
                                "allons",
                                "après",
                                "assez",
                                "attendu",
                                "au",
                                "aucun",
                                "aucune",
                                "aujourd",
                                "aujourd'hui",
                                "auquel",
                                "aura",
                                "auront",
                                "aussi",
                                "autre",
                                "autres",
                                "aux",
                                "auxquelles",
                                "auxquels",
                                "avaient",
                                "avais",
                                "avait",
                                "avant",
                                "avec",
                                "avoir",
                                "ayant",
                                "b",
                                "bah",
                                "beaucoup",
                                "bien",
                                "bigre",
                                "boum",
                                "bravo",
                                "brrr",
                                "c",
                                "ça",
                                "car",
                                "ce",
                                "ceci",
                                "cela",
                                "celle",
                                "celle-ci",
                                "celle-là",
                                "celles",
                                "celles-ci",
                                "celles-là",
                                "celui",
                                "celui-ci",
                                "celui-là",
                                "cent",
                                "cependant",
                                "certain",
                                "certaine",
                                "certaines",
                                "certains",
                                "certes",
                                "ces",
                                "cet",
                                "cette",
                                "ceux",
                                "ceux-ci",
                                "ceux-là",
                                "chacun",
                                "chaque",
                                "cher",
                                "chère",
                                "chères",
                                "chers",
                                "chez",
                                "chiche",
                                "chut",
                                "ci",
                                "cinq",
                                "cinquantaine",
                                "cinquante",
                                "cinquantième",
                                "cinquième",
                                "clac",
                                "clic",
                                "combien",
                                "comme",
                                "comment",
                                "compris",
                                "concernant",
                                "contre",
                                "couic",
                                "crac",
                                "d",
                                "da",
                                "dans",
                                "de",
                                "debout",
                                "dedans",
                                "dehors",
                                "delà",
                                "depuis",
                                "derrière",
                                "des",
                                "dès",
                                "désormais",
                                "desquelles",
                                "desquels",
                                "dessous",
                                "dessus",
                                "deux",
                                "deuxième",
                                "deuxièmement",
                                "devant",
                                "devers",
                                "devra",
                                "différent",
                                "différente",
                                "différentes",
                                "différents",
                                "dire",
                                "divers",
                                "diverse",
                                "diverses",
                                "dix",
                                "dix-huit",
                                "dixième",
                                "dix-neuf",
                                "dix-sept",
                                "doit",
                                "doivent",
                                "donc",
                                "dont",
                                "douze",
                                "douzième",
                                "dring",
                                "du",
                                "duquel",
                                "durant",
                                "e",
                                "effet",
                                "eh",
                                "elle",
                                "elle-même",
                                "elles",
                                "elles-mêmes",
                                "en",
                                "encore",
                                "entre",
                                "envers",
                                "environ",
                                "es",
                                "ès",
                                "est",
                                "et",
                                "etant",
                                "étaient",
                                "étais",
                                "était",
                                "étant",
                                "etc",
                                "été",
                                "etre",
                                "être",
                                "eu",
                                "euh",
                                "eux",
                                "eux-mêmes",
                                "excepté",
                                "f",
                                "façon",
                                "fais",
                                "faisaient",
                                "faisant",
                                "fait",
                                "feront",
                                "fi",
                                "flac",
                                "floc",
                                "font",
                                "g",
                                "gens",
                                "h",
                                "ha",
                                "hé",
                                "hein",
                                "hélas",
                                "hem",
                                "hep",
                                "hi",
                                "ho",
                                "holà",
                                "hop",
                                "hormis",
                                "hors",
                                "hou",
                                "houp",
                                "hue",
                                "hui",
                                "huit",
                                "huitième",
                                "hum",
                                "hurrah",
                                "i",
                                "il",
                                "ils",
                                "importe",
                                "j",
                                "je",
                                "jusqu",
                                "jusque",
                                "k",
                                "l",
                                "la",
                                "là",
                                "laquelle",
                                "las",
                                "le",
                                "lequel",
                                "les",
                                "lès",
                                "lesquelles",
                                "lesquels",
                                "leur",
                                "leurs",
                                "longtemps",
                                "lorsque",
                                "lui",
                                "lui-même",
                                "m",
                                "ma",
                                "maint",
                                "mais",
                                "malgré",
                                "me",
                                "même",
                                "mêmes",
                                "merci",
                                "mes",
                                "mien",
                                "mienne",
                                "miennes",
                                "miens",
                                "mille",
                                "mince",
                                "moi",
                                "moi-même",
                                "moins",
                                "mon",
                                "moyennant",
                                "n",
                                "na",
                                "ne",
                                "néanmoins",
                                "neuf",
                                "neuvième",
                                "ni",
                                "nombreuses",
                                "nombreux",
                                "non",
                                "nos",
                                "notre",
                                "nôtre",
                                "nôtres",
                                "nous",
                                "nous-mêmes",
                                "nul",
                                "o",
                                "o|",
                                "ô",
                                "oh",
                                "ohé",
                                "olé",
                                "ollé",
                                "on",
                                "ont",
                                "onze",
                                "onzième",
                                "ore",
                                "ou",
                                "où",
                                "ouf",
                                "ouias",
                                "oust",
                                "ouste",
                                "outre",
                                "p",
                                "paf",
                                "pan",
                                "par",
                                "parmi",
                                "partant",
                                "particulier",
                                "particulière",
                                "particulièrement",
                                "pas",
                                "passé",
                                "pendant",
                                "personne",
                                "peu",
                                "peut",
                                "peuvent",
                                "peux",
                                "pff",
                                "pfft",
                                "pfut",
                                "pif",
                                "plein",
                                "plouf",
                                "plus",
                                "plusieurs",
                                "plutôt",
                                "pouah",
                                "pour",
                                "pourquoi",
                                "premier",
                                "première",
                                "premièrement",
                                "près",
                                "proche",
                                "psitt",
                                "puisque",
                                "q",
                                "qu",
                                "quand",
                                "quant",
                                "quanta",
                                "quant-à-soi",
                                "quarante",
                                "quatorze",
                                "quatre",
                                "quatre-vingt",
                                "quatrième",
                                "quatrièmement",
                                "que",
                                "quel",
                                "quelconque",
                                "quelle",
                                "quelles",
                                "quelque",
                                "quelques",
                                "quelqu'un",
                                "quels",
                                "qui",
                                "quiconque",
                                "quinze",
                                "quoi",
                                "quoique",
                                "r",
                                "revoici",
                                "revoilà",
                                "rien",
                                "s",
                                "sa",
                                "sacrebleu",
                                "sans",
                                "sapristi",
                                "sauf",
                                "se",
                                "seize",
                                "selon",
                                "sept",
                                "septième",
                                "sera",
                                "seront",
                                "ses",
                                "si",
                                "sien",
                                "sienne",
                                "siennes",
                                "siens",
                                "sinon",
                                "six",
                                "sixième",
                                "soi",
                                "soi-même",
                                "soit",
                                "soixante",
                                "son",
                                "sont",
                                "sous",
                                "stop",
                                "suis",
                                "suivant",
                                "sur",
                                "surtout",
                                "t",
                                "ta",
                                "tac",
                                "tant",
                                "te",
                                "té",
                                "tel",
                                "telle",
                                "tellement",
                                "telles",
                                "tels",
                                "tenant",
                                "tes",
                                "tic",
                                "tien",
                                "tienne",
                                "tiennes",
                                "tiens",
                                "toc",
                                "toi",
                                "toi-même",
                                "ton",
                                "touchant",
                                "toujours",
                                "tous",
                                "tout",
                                "toute",
                                "toutes",
                                "treize",
                                "trente",
                                "très",
                                "trois",
                                "troisième",
                                "troisièmement",
                                "trop",
                                "tsoin",
                                "tsouin",
                                "tu",
                                "u",
                                "un",
                                "une",
                                "unes",
                                "uns",
                                "v",
                                "va",
                                "vais",
                                "vas",
                                "vé",
                                "vers",
                                "via",
                                "vif",
                                "vifs",
                                "vingt",
                                "vivat",
                                "vive",
                                "vives",
                                "vlan",
                                "voici",
                                "voilà",
                                "vont",
                                "vos",
                                "votre",
                                "vôtre",
                                "vôtres",
                                "vous",
                                "vous-mêmes",
                                "vu",
                                "w",
                                "x",
                                "y",
                                "z",
                                "zut",
                                "alors",
                                "aucuns",
                                "bon",
                                "devrait",
                                "dos",
                                "droite",
                                "début",
                                "essai",
                                "faites",
                                "fois",
                                "force",
                                "haut",
                                "ici",
                                "juste",
                                "maintenant",
                                "mine",
                                "mot",
                                "nommés",
                                "nouveaux",
                                "parce",
                                "parole",
                                "personnes",
                                "pièce",
                                "plupart",
                                "seulement",
                                "soyez",
                                "sujet",
                                "tandis",
                                "valeur",
                                "voie",
                                "voient",
                                "état",
                                "étions",
                                "d'",
                                "l'"))
                .setLanguageNormalizeReplacements(
                        Map.of(
                                "d'", "de ",
                                "l'", "l' ",
                                "œ", "oe"))
                .setTokenSplitRegex(" |\\-")
                .setTokenCleanPattern(
                        compile("[\\(\\)\\,\\.\\/\\+\\_]") // In FR should NOT remove '-'
                        )
                .setTokenFormatFunction(
                        new Function<>() {
                            private final Pattern cleanupPattern = compile("(?i)" + String.join("|", "s$", "^l'"));

                            @Override
                            public String apply(String s) {
                                if (s.length() <= RecipeLangAnalyzerConfiguration.TOKEN_LENGTH_MIN) {
                                    return s;
                                } else {
                                    return removeAll(s, cleanupPattern);
                                }
                            }
                        });
    }

    public static RecipeMagnitudeAnalyzerConfiguration createRecipeMagnitudeAnalyzerConfiguration() {
        return new RecipeMagnitudeAnalyzerConfiguration(
                toMap(
                        "un", ONE,
                        "deux", TWO,
                        "trois", THREE,
                        "quatre", FOUR,
                        "cinq", FIVE,
                        "six", SIX,
                        "sept", SEVEN,
                        "huit", EIGHT,
                        "neuf", NINE,
                        "dix", TEN,
                        "quelques", null));
    }

    public static RecipeMeasurementAnalyzerConfiguration createRecipeMeasurementAnalyzerConfiguration() {
        return new RecipeMeasurementAnalyzerConfiguration(UNIT_PIECE_FR, UNIT_GRAM)
                .setMeasurementUnitSeparatorTokens(Set.of("de", "d'")) // 300 g [de] pommes, 10 g [d']eau
                .setMeasurementUnitDefinitions(
                        // Add missing!
                        UNIT_PIECE_FR,
                        new MeasurementUnitDefinition("brins", UNIT_PIECE_FR, false),
                        new MeasurementUnitDefinition("branche", UNIT_PIECE_FR, false, "branches"),
                        new MeasurementUnitDefinition("botte", UNIT_PIECE_FR, false),
                        //
                        new MeasurementUnitDefinition("du", UNIT_PINCH, false),
                        new MeasurementUnitDefinition("peu", UNIT_PINCH, false),
                        new MeasurementUnitDefinition("pincée", UNIT_PINCH, true, "pincées", "pointe de couteau"), // trick for single word
                        new MeasurementUnitDefinition("goutte", UNIT_PINCH, false, "gouttes"),
                        //
                        new MeasurementUnitDefinition("CàC", UNIT_TSP, true, "c. à café", "c. à thé"), // Important, Single word
                        new MeasurementUnitDefinition("CàS", UNIT_TBSP, true, "c. à soupe"), // Important, Single word
                        //
                        UNIT_GRAM,
                        UNIT_KG,
                        UNIT_LITER,
                        UNIT_MILILITER);
    }

    public static RecipeToolAnalyzerConfiguration createRecipeToolAnalyzerConfiguration() {
        return new RecipeToolAnalyzerConfiguration(
                RECIPE_TOOL_MATCHERS,
                toMapP(
                        // These 3 will disssapear in favour of explicit tool match
                        //                        Pair.of(TOOL_MAIN, toRecipeInstructionMatcher("(?i)(?:le|du) bol")),
                        //                        Pair.of(TOOL_INTERNAL, toRecipeInstructionMatcher(REGEX_TOOL_INTERNAL)), //(?:varoma|plateau vapeur|panier cuisson|gobelet doseur)
                        //                        Pair.of(TOOL_EXTERNAL, toRecipeInstructionMatcher(REGEX_TOOL_EXTERNAL)),

                        Pair.of(OVEN_OPERATION, toRecipeInstructionMatcher("(?i)(?:enfourne[rz]|gratine[rz])|(?:allumer|allumez|préchauffer|préchauffez) le four|cuire.*à.*°C")),
                        Pair.of(REFRIGERATE, toRecipeInstructionMatcher("(?i)(?:réserve[rz]|laisse[rz]).*au (?:frais|réfrigérateur|réfrigé)|réfrigérer|réfrigérez")),
                        Pair.of(FREEZE, toRecipeInstructionMatcher("(?i)au congélateur")),

                        //                        Pair.of(RecipeAnalysisTag.SIMMERING_BASKET, toRecipeInstructionMatcher("(?i)using.*simmering basket.*(drain|strain)")),
                        Pair.of(SIMMERING_BASKET_PUT_INSIDE, toRecipeInstructionMatcher("(?i)(?:insére[rz]|réinsére[rz]) le panier cuisson")),
                        Pair.of(SIMMERING_BASKET_REMOVE, toRecipeInstructionMatcher("(?i)retire[rz] le panier cuisson")),
                        Pair.of(SIMMERING_BASKET_AS_LID_PUT, toRecipeInstructionMatcher("(?i)remplaçant.*gobelet doseur.*panier cuisson|panier.*cuisson.*la place.*gobelet doseur")), // Needs context analysis to place before tm step
                        //                        Pair.of(RecipeAnalysisTag.SIMMERING_BASKET_AS_LID_REMOVE, defineMe()),
                        //                        Pair.of(RecipeAnalysisTag.SIMMERING_BASKET_REMOVE_MEASURING_CUP_PUT, defineMe()),

                        //                        Pair.of(RecipeAnalysisTag.MIXING_BOWL, toRecipeInstructionMatcher("(?i)(?:remove).*mixing.*bowl lid")),
                        Pair.of(MIXING_BOWL_PUT, toRecipeInstructionMatcher("(?i)(repositionne[rz]|remettre|remettez) le bol")), // Repositionner le bol sur son socle
                        Pair.of(MIXING_BOWL_REMOVE, toRecipeInstructionMatcher("(?i)retire[rz] le bol")), // Repositionner le bol sur son socle
                        Pair.of(MIXING_BOWL_EMPTY, toRecipeInstructionMatcher("(?i)vide[rz].*le bol")),
                        Pair.of(MIXING_BOWL_CLEAN, toRecipeInstructionMatcher("(?i)(?:rince[rz]|nettoye[rz]).*le bol")),
                        Pair.of(TRANSFER_FROM_BOWL, toRecipeInstructionMatcher("(?i)transvase[rz].*" + REGEX_FOCUS_EXTERNAL_CONTAINER)),
                        //                        Pair.of(RecipeAnalysisTag.MEASURING_CUP, defineMe()),
                        Pair.of(MEASURING_CUP_NOTUSE, toRecipeInstructionMatcher("(?i)" + REGEX_SANS_GOBELET_DOSEUR)),
                        Pair.of(MEASURING_CUP_PUT, toRecipeInstructionMatcher("(?i)insére[rz].*gobelet doseur")),
                        Pair.of(MEASURING_CUP_HOLD_VIBRATION, toRecipeInstructionMatcher("(?i)maintenir.*gobelet doseur")),

                        //                        Pair.of(RecipeAnalysisTag.WHISK, toRecipeInstructionMatcher()),
                        Pair.of(WHISK_PUT, toRecipeInstructionMatcher("(?i)insére[rz].*fouet")),
                        Pair.of(WHISK_REMOVE, toRecipeInstructionMatcher("(?i)(?:retire[rz]|Ôter|ôter).*fouet")),

                        //                        Pair.of(RecipeAnalysisTag.SPATULA, defineMe()),
                        Pair.of(SPATULA_MIX, toRecipeInstructionMatcher("(?i)mélange[rz].*aide.*spatule")),
                        Pair.of(SPATULA_SCRAP_SIDE, toRecipeInstructionMatcher("(?i)racle[rz].*(?:parois|fond) du bol.*aide.*spatule")),
                        //                        Pair.of(RecipeAnalysisTag.SPATULA_MIX_WELL, toRecipeInstructionMatcher("(?i)mix.*well with spatula")),

                        Pair.of(STEAMER_PUT, toRecipeInstructionMatcher("(?i)(?:mettre en place|mettez en place|installe[rz]|replace[rz]|remettre).*(?:varoma|steamer)|mettre.*(?:varoma|steamer) en place")),
                        Pair.of(STEAMER_REMOVE, toRecipeInstructionMatcher("(?i)(?:retire[rz]|Ôter|ôter).*(?:varoma|steamer)")), // Retirer l'ensemble Varoma
                        Pair.of(STEAMER_CLOSE, toRecipeInstructionMatcher("(?i)ferme[rz].*(?:varoma|steamer)")),
                        Pair.of(STEAMER_DISH_PUT, toRecipeInstructionMatcher("(?i)insére[rz].*plateau vapeur")), // insérer le plateau vapeur dans le (?:varoma|steamer)
                        Pair.of(STEAMER_DISH_REMOVE, toRecipeInstructionMatcher("(?i)(?:retire[rz]|Ôter|ôter).*plateau vapeur"))
                        //                        Pair.of(RecipeAnalysisTag.STEAMER_TRAY_PUT, toRecipeInstructionMatcher("(?i)insert.*(?:varoma|steamer).*??")),
                        //                        Pair.of(RecipeAnalysisTag.STEAMER_TRAY_REMOVE, toRecipeInstructionMatcher("(?i)remove.*(varoma|steamer) ??")),

                        //                        Pair.of(RecipeAnalysisTag.OTHER_OPERATION, defineMe()),
                        //                        Pair.of(RecipeAnalysisTag.USEFUL_ITEMS, defineMe()),
                        //                                Pair.of(KITCHEN_EQUIPMENT, toRecipeInstructionMatcher())

                        ));
    }
}
