package com.recipeton.shared.analysis.service;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.configuration.RecipeAnalyzerConfiguration;
import com.recipeton.shared.analysis.configuration.RecipeCustomizationConfiguration;
import com.recipeton.shared.analysis.domain.RecipeDifficultyDefinition;
import com.recipeton.shared.analysis.domain.RecipePriceDefinition;
import com.recipeton.shared.domain.RecipeDefinition;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;
import java.util.stream.Collectors;

import static com.recipeton.shared.util.StreamUtil.toStream;

@Slf4j
@RequiredArgsConstructor
public class RecipeAnalyzer {

    private final RecipeMeasurementAnalyzer recipeMeasurementAnalyzer;
    private final RecipeIngredientAnalyzer recipeIngredientAnalyzer;
    private final RecipeInstructionAnalyzer recipeInstructionAnalyzer;
    private final RecipeCategoryAnalyzer recipeCategoryAnalyzer;
    private final RecipeLangAnalyzer recipeLangAnalyzer;
    private final RecipeToolAnalyzer recipeToolAnalyzer;

    private final RecipeAnalyzerConfiguration configuration;

    public String analyzeLocale(RecipeDefinition recipeDefinition) {
        return getRecipeCustomizationConfiguration().getRecipeLocaleLabelFunction().apply(recipeDefinition);
    }

    public RecipeDifficultyDefinition analyzeRecipeDifficulty(RecipeDefinition recipeDefinition) {
        return getRecipeCustomizationConfiguration().getRecipeDifficultDefinitionPredicates().entrySet().stream()
                .filter(e -> e.getValue().test(recipeDefinition.getDifficulty()))
                .map(Map.Entry::getKey)
                .findFirst()
                .orElse(RecipeDifficultyDefinition.UNKNOWN);
    }

    public RecipePriceDefinition analyzeRecipePrice(RecipeDefinition recipeDefinition) {
        String keywords = toStream(recipeDefinition.getKeywords()).collect(Collectors.joining(","));
        return getRecipeCustomizationConfiguration().getRecipePriceDefinitionPredicates().entrySet().stream().filter(e -> e.getValue().test(keywords)).map(Map.Entry::getKey).findFirst().orElse(RecipePriceDefinition.NO_INFORMATION);
    }

    public String analyzeTitle(RecipeDefinition recipeDefinition) {
        return getRecipeCustomizationConfiguration().getRecipeTitleFormatFunction().apply(recipeDefinition.getName());
    }

    public RecipeAnalyzerConfiguration getConfiguration() {
        return configuration;
    }

    public RecipeCategoryAnalyzer getRecipeCategoryAnalyzer() {
        return recipeCategoryAnalyzer;
    }

    public RecipeCustomizationConfiguration getRecipeCustomizationConfiguration() {
        return configuration.getRecipeCustomizationConfiguration();
    }

    public RecipeIngredientAnalyzer getRecipeIngredientAnalyzer() {
        return recipeIngredientAnalyzer;
    }

    public RecipeInstructionAnalyzer getRecipeInstructionAnalyzer() {
        return recipeInstructionAnalyzer;
    }

    public RecipeLangAnalyzer getRecipeLangAnalyzer() {
        return recipeLangAnalyzer;
    }

    public RecipeMeasurementAnalyzer getRecipeMeasurementAnalyzer() {
        return recipeMeasurementAnalyzer;
    }

    public RecipeToolAnalyzer getRecipeToolAnalyzer() {
        return recipeToolAnalyzer;
    }

    public boolean isHandlerOf(RecipeDefinition recipeDefinition) {
        return configuration.getHandlerOfPredicate().test(recipeDefinition);
    }
}
