package com.recipeton.shared.analysis.service.filter;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.domain.RecipeInstructionAnalysis;

import static com.recipeton.shared.analysis.domain.RecipeInstructionTag.*;
import static com.recipeton.shared.analysis.domain.RecipeToolComposedActionTag.*;

public class FocusByToolAnalysisFilter extends TermAddingRecipeInstructionAnalysisFilter {

    public FocusByToolAnalysisFilter() {
        super("isFocusByTool?");
    }

    @Override
    public RecipeInstructionAnalysis apply(RecipeInstructionAnalysis recipeInstructionAnalysis) {
        if (recipeInstructionAnalysis.isTagsAnyPresent(MIXING_BOWL_PUT, WHISK_PUT, MIXING_BOWL_CLEAN, MIXING_BOWL_EMPTY, SIMMERING_BASKET_PUT_INSIDE, DO_COMMAND_MAIN)) {
            addTermIfNotPresent(recipeInstructionAnalysis, FOCUS_MAIN);
            return recipeInstructionAnalysis;
        }

        if (recipeInstructionAnalysis.isTagsAnyPresent(STEAMER_PUT)) {
            addTermIfNotPresent(recipeInstructionAnalysis, FOCUS_ON_TOP);
            return recipeInstructionAnalysis;
        }

        if (recipeInstructionAnalysis.isTagsAnyPresent(REFRIGERATE, FREEZE)) {
            addTermIfNotPresent(recipeInstructionAnalysis, FOCUS_EXTERNAL);
            return recipeInstructionAnalysis;
        }

        // Improve when new detailed utensil detection is added. Each utensil should be matched for detection.

        return recipeInstructionAnalysis;
    }
}
