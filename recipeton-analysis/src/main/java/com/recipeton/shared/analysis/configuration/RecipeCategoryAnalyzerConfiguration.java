package com.recipeton.shared.analysis.configuration;

/*-
 * #%L
 * recipeton-analysis
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.recipeton.shared.analysis.domain.RecipeCategoryDefinition.createRecipeCategoryUidByPrincipal;
import static com.recipeton.shared.util.StreamUtil.toStream;
import static com.recipeton.shared.util.TextUtil.*;

@Data
@RequiredArgsConstructor
@Accessors(chain = true)
@ToString(onlyExplicitlyIncluded = true)
public class RecipeCategoryAnalyzerConfiguration {

    public static final Function<String, String> FUNCTION_REMOVE_PARENTHESES_AND_CAPITALIZE_FIRST = s -> capitalizeFirst(removeAll(s, BETWEEN_PARENTHESES_PATTERN)).trim();

    public static final int CATEGORY_MATCH_RATIO_MIN = 80;
    public static final int CATEGORY_MATCH_RATION_MIN_SPECIFIC = 98;
    public static final int CATEGORY_SHORT_SEARCH_SUBSTRING_LENGTH_MAX = 4;
    public static final int CATEGORY_SEARCH_COMPARISON_MAX_LENGTH_DIFF = 3;

    /** Prefefined recipe categories and list of possible aliases mark UID */
    private final Map<String, List<String>> recipeCategoryNamesByUid = new HashMap<>();

    private final int recipeCategoryMatchRatioMin;
    private final int recipeCategoryMatchRatioMinSpecific;
    private final int recipeCategoryShortSearchSubstringLengthMax;
    private final int recipeCategorySearchComparisonMaxLengthDiff;

    /** Preprocess recipe categories. Should return null for names to be ignored */
    private Function<String, String> recipeCategoryNamePreprocessor = FUNCTION_REMOVE_PARENTHESES_AND_CAPITALIZE_FIRST;

    public RecipeCategoryAnalyzerConfiguration() {
        this.recipeCategoryMatchRatioMin = CATEGORY_MATCH_RATIO_MIN;
        this.recipeCategoryMatchRatioMinSpecific = CATEGORY_MATCH_RATION_MIN_SPECIFIC;
        this.recipeCategoryShortSearchSubstringLengthMax = CATEGORY_SHORT_SEARCH_SUBSTRING_LENGTH_MAX;
        this.recipeCategorySearchComparisonMaxLengthDiff = CATEGORY_SEARCH_COMPARISON_MAX_LENGTH_DIFF;
    }

    public RecipeCategoryAnalyzerConfiguration addRecipeCategoryByPrincipal(String uid, String title, String... titleVariants) {
        String principalUid = createRecipeCategoryUidByPrincipal(uid);
        if (recipeCategoryNamesByUid.containsKey(principalUid)) {
            throw new IllegalArgumentException("Recipe category with uid=" + principalUid + " already registered");
        }
        recipeCategoryNamesByUid.put(principalUid, Stream.concat(Stream.of(title), toStream(titleVariants)).collect(Collectors.toList()));
        return this;
    }
}
