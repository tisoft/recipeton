package com.recipeton.shared.analysis.configuration;

/*-
 * #%L
 * recipeton-analysis
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.domain.MeasurementUnitDefinition;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.Collections;
import java.util.Optional;
import java.util.Set;

@Data
@Accessors(chain = true)
@ToString(onlyExplicitlyIncluded = true)
public class RecipeMeasurementAnalyzerConfiguration {

    public static final int DEFAULT_ON_UNIT_UNDEFINED_WEIGHTED = 10;

    private final MeasurementUnitDefinition unitPieceDefault;
    private final MeasurementUnitDefinition unitWeightedDefault;

    private Set<MeasurementUnitDefinition> measurementUnitDefinitions;

    private Set<String> measurementUnitSeparatorTokens = Collections.emptySet();

    private double onUnitUndefinedSetAsWeightedDefaultWhenMagnitudeGreaterThan = DEFAULT_ON_UNIT_UNDEFINED_WEIGHTED;

    public Optional<MeasurementUnitDefinition> findMeasurementUnitDefinition(String text) {
        return measurementUnitDefinitions.stream().filter(m -> m.isNotation(text)).findFirst();
    }

    public RecipeMeasurementAnalyzerConfiguration setMeasurementUnitDefinitions(MeasurementUnitDefinition... measurementUnitDefinitions) {
        this.measurementUnitDefinitions = Set.of(measurementUnitDefinitions);
        return this;
    }
}
