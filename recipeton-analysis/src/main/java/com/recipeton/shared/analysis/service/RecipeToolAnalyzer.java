package com.recipeton.shared.analysis.service;

/*-
 * #%L
 * recipeton-analysis
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.configuration.RecipeToolAnalyzerConfiguration;
import com.recipeton.shared.analysis.domain.RecipeInstructionMatcher;
import com.recipeton.shared.analysis.domain.RecipeToolAnalysis;
import com.recipeton.shared.analysis.domain.RecipeToolComposedActionTag;
import com.recipeton.shared.analysis.domain.RecipeToolDefinition;
import org.apache.commons.lang3.StringUtils;

import java.util.Map;

public class RecipeToolAnalyzer {

    private final RecipeLangAnalyzer recipeLangAnalyzer;
    private final RecipeToolAnalyzerConfiguration configuration;

    public RecipeToolAnalyzer(RecipeLangAnalyzer recipeLangAnalyzer, RecipeToolAnalyzerConfiguration configuration) {
        this.recipeLangAnalyzer = recipeLangAnalyzer;
        this.configuration = configuration;
    }

    public RecipeToolAnalysis analyzeRecipeTool(String text) {
        String textFormatted = StringUtils.trimToNull(text);
        if (textFormatted == null) {
            return null;
        }

        return new RecipeToolAnalysis().setUid(recipeLangAnalyzer.toUid(textFormatted)).setText(textFormatted);
    }

    public Map<RecipeToolComposedActionTag, RecipeInstructionMatcher> getRecipeToolComposedActionTagMatchers() {
        return configuration.getRecipeToolComposedActionTagMatchers();
    }

    public Map<RecipeToolDefinition, RecipeInstructionMatcher> getRecipeToolDefinitionMatchers() {
        return configuration.getRecipeToolDefinitionMatchers();
    }

    public boolean isToolPresent(String text) {
        return configuration.getToolsPattern().matcher(text).find();
    }
}
