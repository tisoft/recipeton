package com.recipeton.shared.analysis.domain;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Stream.of;

@Data
@ToString(onlyExplicitlyIncluded = true)
@Accessors(chain = true)
public class RecipeInstructionAnalysis implements RecipeTagged {

    @ToString.Include private final RecipeInstructionTokenization tokenization;
    @ToString.Include private final Collection<RecipeTerm> terms = new HashSet<>(); // Careful with hashCodeEquals...
    private final Collection<RecipeTerm> discardedTerms = new HashSet<>();

    private RecipeInstructionAnalysis prev;
    private RecipeInstructionAnalysis next;

    public void addTerm(RecipeTerm recipeTerm) {
        terms.add(recipeTerm);
    }

    public void discardTerm(RecipeIngredientTerm term) {
        boolean found = terms.remove(term);
        if (!found) {
            throw new IllegalArgumentException("Trying to remove not found term. Recheck equals/hashcode?? term=" + term);
        }
        discardedTerms.add(term);
    }

    public RecipeInstructionTag findFirstMatchInPrev(int depth, RecipeInstructionTag... tags) {
        RecipeInstructionAnalysis prev = getPrev();
        int count = 0;
        while (prev != null && count < depth) {
            RecipeInstructionTag tag = of(tags).filter(prev::isTagPresent).findFirst().orElse(null);
            if (tag != null) {
                return tag;
            }
            prev = prev.getPrev();
            count++;
        }

        return null;
    }

    public List<RecipeCommandTerm> getCommandTerms() {
        return streamTerms().filter(t -> t instanceof RecipeCommandTerm).map(t -> (RecipeCommandTerm) t).collect(Collectors.toList());
    }

    public List<RecipeIngredientTerm> getRecipeIngredientTerms() {
        return streamRecipeIngredientTerms().collect(Collectors.toList());
    }

    public List<RecipeToolTerm> getRecipeToolTerms() {
        return streamRecipeToolTerms().collect(Collectors.toList());
    }

    @Override
    public Collection<RecipeTag> getTags() {
        return streamTermsByTagged().flatMap(t -> t.getTags().stream()).collect(Collectors.toSet());
    }

    public String getText() {
        return getTokenization().getText();
    }

    @Override
    public boolean isTagPresent(RecipeTag tag) {
        return streamTermsByTagged().anyMatch(r -> r.isTagPresent(tag));
    }

    public boolean isTagPresentInNext(RecipeTag tag) {
        return Optional.ofNullable(getNext()).map(r -> r.isTagPresent(tag)).orElse(false);
    }

    public boolean isTagPresentInPrev(RecipeTag tag) {
        return Optional.ofNullable(getPrev()).map(r -> r.isTagPresent(tag)).orElse(false);
    }

    public boolean isTagsAllPresent(RecipeTag... tags) {
        return of(tags).allMatch(this::isTagPresent);
    }

    public boolean isTagsAnyPresent(RecipeTag... tags) {
        return streamTermsByTagged().anyMatch(t -> of(tags).anyMatch(t::isTagPresent));
    }

    public boolean isTagsNonePresent(RecipeTag... tags) {
        return of(tags).noneMatch(this::isTagPresent);
    }

    public void restoreTerm(RecipeIngredientTerm term) {
        boolean found = discardedTerms.remove(term);
        if (!found) {
            throw new IllegalArgumentException("Trying to remove not found term. Recheck equals/hashcode?? term=" + term);
        }
        terms.add(term);
    }

    public Stream<RecipeIngredientTerm> streamDiscardedRecipeIngredientTerms() {
        return discardedTerms.stream().filter(t -> t instanceof RecipeIngredientTerm).map(t -> (RecipeIngredientTerm) t);
    }

    public Stream<RecipeIngredientTerm> streamRecipeIngredientTerms() {
        return streamTerms().filter(t -> t instanceof RecipeIngredientTerm).map(t -> (RecipeIngredientTerm) t);
    }

    public Stream<RecipeInstructionTerm> streamRecipeInstructionTerms() {
        return streamTerms().filter(t -> t instanceof RecipeInstructionTerm).map(t -> (RecipeInstructionTerm) t);
    }

    public Stream<RecipeToolComposedActionTerm> streamRecipeToolComposedActionTerms() {
        return streamTerms().filter(t -> t instanceof RecipeToolComposedActionTerm).map(t -> (RecipeToolComposedActionTerm) t);
    }

    public Stream<RecipeTerm> streamTerms() {
        return terms.stream();
    }

    public Stream<RecipeToolTerm> streamRecipeToolTerms() {
        return streamTerms().filter(t -> t instanceof RecipeToolTerm).map(t -> (RecipeToolTerm) t);
    }

    private Stream<RecipeTagged> streamTermsByTagged() {
        return streamTerms().filter(t -> t instanceof RecipeTagged).map(t -> (RecipeTagged) t);
    }
}
