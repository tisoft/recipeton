package com.recipeton.shared.analysis.service.filter;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.analysis.configuration.RecipeInstructionAnalyzerConfiguration;
import com.recipeton.shared.analysis.domain.RecipeInstructionAnalysis;

import java.util.regex.Matcher;

import static com.recipeton.shared.analysis.domain.RecipeInstructionTag.DO_PLACE;
import static com.recipeton.shared.analysis.domain.RecipeInstructionTag.IS_ITEM_FROM_TO_TRANSFER;
import static com.recipeton.shared.analysis.domain.RecipeToolComposedActionTag.TRANSFER_FROM_BOWL;

public class ItemTransferAnalysisFilter extends TermAddingRecipeInstructionAnalysisFilter {

    private final RecipeInstructionAnalyzerConfiguration recipeInstructionAnalyzerConfiguration;

    public ItemTransferAnalysisFilter(RecipeInstructionAnalyzerConfiguration recipeInstructionAnalyzerConfiguration) {
        super("isTransfer?");
        this.recipeInstructionAnalyzerConfiguration = recipeInstructionAnalyzerConfiguration;
    }

    // TRY TO QUALIFY TRANSFER SOURCE AND TRANSFER TARGET
    // separate action and sides of nexus
    @Override
    public RecipeInstructionAnalysis apply(RecipeInstructionAnalysis recipeInstructionAnalysis) {
        if (recipeInstructionAnalysis.isTagPresent(TRANSFER_FROM_BOWL)) {
            addTermIfNotPresent(recipeInstructionAnalysis, IS_ITEM_FROM_TO_TRANSFER);
            return recipeInstructionAnalysis;
        }

        if (!recipeInstructionAnalysis.isTagsAnyPresent(DO_PLACE) || recipeInstructionAnalyzerConfiguration.getRecipeInstructionTransferNexus() == null) {
            return recipeInstructionAnalysis;
        }

        Matcher matcher = recipeInstructionAnalyzerConfiguration.getRecipeInstructionTransferNexus().matcher(recipeInstructionAnalysis.getText());
        if (matcher.find()) {
            return addTermIfNotPresent(recipeInstructionAnalysis, IS_ITEM_FROM_TO_TRANSFER);
        }

        return recipeInstructionAnalysis;
    }
}
