package com.recipeton.shared.analysis.domain;

/*-
 * #%L
 * recipeton-analysis
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import lombok.Data;
import lombok.ToString;

@Data
@ToString(onlyExplicitlyIncluded = true)
public class RecipeMeasurementUnitAnalysis {
    @ToString.Include private final String text;
    @ToString.Include private final MeasurementUnitDefinition unitDefinition;
    private final boolean contextual;

    public String getUnitText(boolean normalized) {
        return unitDefinition == null ? text : normalized ? unitDefinition.getName() : text;
    }

    public boolean isWeighted() {
        return unitDefinition != null && unitDefinition.isWeighed();
    }
}
