package com.recipeton.shared.analysis.configuration;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.recipeton.shared.domain.RecipeDefinition;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.function.Predicate;

@Data
@Accessors(chain = true)
@ToString(onlyExplicitlyIncluded = true)
public class RecipeAnalyzerConfiguration {

    public static final String REGEX_COMMAND = "(?<cmd>\\[(?:.*?)\\])";
    public static final String REGEX_SENTENCE_BEGIN = "(?<sep>^|(?<=\\. ))";
    public static final String REGEX_SENTENCE_END = "(?:\\.|$)";

    private final String id;
    private final Predicate<RecipeDefinition> handlerOfPredicate;

    private final RecipeLangAnalyzerConfiguration recipeLangAnalyzerConfiguration;
    private final RecipeCategoryAnalyzerConfiguration recipeCategoryAnalyzerConfiguration;
    private final RecipeCommandAnalyzerConfiguration recipeCommandAnalyzerConfiguration;
    private final RecipeMagnitudeAnalyzerConfiguration recipeMagnitudeAnalyzerConfiguration;
    private final RecipeMeasurementAnalyzerConfiguration recipeMeasurementAnalyzerConfiguration;
    private final RecipeInstructionAnalyzerConfiguration recipeInstructionAnalyzerConfiguration;
    private final RecipeIngredientAnalyzerConfiguration recipeIngredientAnalyzerConfiguration;
    private final RecipeToolAnalyzerConfiguration recipeToolAnalyzerConfiguration;
    private final RecipeCustomizationConfiguration recipeCustomizationConfiguration;
}
