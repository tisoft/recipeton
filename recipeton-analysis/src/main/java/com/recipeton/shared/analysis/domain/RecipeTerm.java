package com.recipeton.shared.analysis.domain;

/*-
 * #%L
 * recipeton-shared
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

public interface RecipeTerm {

    static int overlap(int offset1, int length1, int offset2, int length2) {
        return Math.max(0, Math.min(offset1 + length1, offset2 + length2) - Math.max(offset1, offset2));
    }

    default int getDistance(RecipeTerm recipeTerm) {
        if (getOverlap(recipeTerm) > 0) {
            return 0;
        }

        int end = getOffset() + getLength();
        if (end <= recipeTerm.getOffset()) {
            return end - recipeTerm.getOffset();
        }

        return recipeTerm.getOffset() + recipeTerm.getLength() - getOffset();
    }

    int getLength();

    int getOffset();

    default int getOverlap(RecipeTerm recipeTerm) {
        return overlap(getOffset(), getLength(), recipeTerm.getOffset(), recipeTerm.getLength());
    }

    boolean isConclusive();

    boolean isContextualMatch();
}
